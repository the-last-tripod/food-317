create database if not exist food_server_db;

use food_server_db;

drop table if exists user;

CREATE TABLE user (
id INT NOT NULL AUTO_INCREMENT,
username VARCHAR(50),
password VARCHAR(50),
name VARCHAR(50),
weight FLOAT,
height FLOAT,
gender ENUM('male', 'female'),
age INT,
PRIMARY KEY (id)
);