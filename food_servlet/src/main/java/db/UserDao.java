package db;


import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: yang
 * Date: 2023-05-14
 * Time: 15:47
 */
public class UserDao {
    // 判断用户名是否存在
    public static boolean exists(String username) throws SQLException {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            conn = getConnection();
            String sql = "SELECT * FROM user WHERE username=?";
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, username);
            rs = stmt.executeQuery();
            if (rs.next()) {
                return true;
            }
            return false;
        } finally {
            closeResultSet(rs);
            closeStatement(stmt);
            closeConnection(conn);
        }
    }

    // 添加用户
    public static void addUser(String username, String password) throws SQLException {
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = getConnection();
            String sql = "INSERT INTO user(username, password) VALUES (?, ?)";
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, username);
            stmt.setString(2, password);
            stmt.executeUpdate();
        } finally {
            closeStatement(stmt);
            closeConnection(conn);
        }
    }

    //检测用户名和密码
    public static boolean checkLogin(String username, String pwd)throws SQLException {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            conn = getConnection();
            String sql = "SELECT * FROM user WHERE username=?";
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, username);
            rs = stmt.executeQuery();
            if (rs.next()) {
                String realPwd = rs.getString("password");
                if(realPwd.equals(pwd))return true;
                else return false;
            }
            return false;
        } finally {
            closeResultSet(rs);
            closeStatement(stmt);
            closeConnection(conn);
        }
    }

    // 获取数据库连接
    private static Connection getConnection() {
        DataSource dataSource = new MysqlDataSource();
        ((MysqlDataSource)dataSource).setURL("jdbc:mysql://127.0.0.1:3306/food_server_db?characterEncoding=UTF-8&useSSL=false");
        ((MysqlDataSource)dataSource).setUser("root");
        ((MysqlDataSource)dataSource).setPassword("");
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
        } catch (SQLException e) {
            System.out.println("数据库连接失败");
            e.printStackTrace();
        }
        return connection;
    }

    // 关闭数据库连接
    private static void closeConnection(Connection conn) throws SQLException {
        if(conn != null)conn.close();
    }

    // 关闭 PreparedStatement 对象
    private static void closeStatement(PreparedStatement stmt) throws SQLException {
        if (stmt != null)stmt.close();
    }

    // 关闭 ResultSet 对象
    private static void closeResultSet(ResultSet rs) throws SQLException {
        if (rs != null)rs.close();
    }
}
