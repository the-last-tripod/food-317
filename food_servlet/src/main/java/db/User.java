package db;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: yang
 * Date: 2023-05-14
 * Time: 15:46
 */
public class User {
    private int age;
    private String name;
    private String gender;
    private double height;
    private double weight;
    private int sport;
    private String userName;
    private String password;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getSport() {
        return sport;
    }

    public void setSport(int sport) {
        this.sport = sport;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getAge() {
        return age;
    }

    public String getName() {
        return name;
    }

    public String getGender() {
        return gender;
    }

    public double getHeight() {
        return height;
    }

    public double getWeight() {
        return weight;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public User(int age, String name, String gender, double height, double weight,int sport) {
        this.age = age;
        this.name = name;
        this.gender = gender;
        this.height = height;
        this.weight = weight;
        this.sport = sport;
    }

    public User(String userName, String pwd){
        this.userName = userName;
        this.password = pwd;
    }

    public User() {
    }
}