/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: yang
 * Date: 2023-03-25
 * Time: 22:14
 */

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class PythonRunner {
    private String rootPath;
    private String os;

    PythonRunner(){
        os = System.getProperty("os.name");
        // 设置路径
        if (os.startsWith("Windows")) {
            rootPath = "D:\\yang\\project\\food-AI\\";
        } else {
            rootPath = "/root/ysl/project/food-AI/";
        }
    }

    public ArrayList<String> start_food_suggest(String name, int age, String gender, double weight,double height,int sport){
        ArrayList<String> al = new ArrayList<>();
        ProcessBuilder pb = null;
        try {
            //设置python虚拟环境
            if (os.startsWith("Windows")) {
                String pythonPath = "D:\\yang\\Anaconda\\envs\\paddle_env\\python.exe"; // 虚拟环境中 Python 解释器的路径
                pb = new ProcessBuilder(pythonPath, rootPath + "py_project\\suggest-test\\FoodSuggest.py",
                        "--name="+name, "--gender="+gender, "--weight="+Double.toString(weight), "--height="+Double.toString(height), "--age="+Integer.toString(age));
            } else {
                String virtualEnv = "/root/anaconda3/envs/paddle_env"; // 虚拟环境名称
                Map<String, String> envMap = new HashMap<>(System.getenv());
                String path = envMap.get("PATH");
                path += ":" + virtualEnv + "/bin";
                envMap.put("PATH", path);

                // 获取 Python 解释器路径
                String pythonPath = virtualEnv + "/bin/python";
                String scriptPath = rootPath + "py_project/suggest-test/FoodSuggest.py";
                // 创建 ProcessBuilder 对象
                pb = new ProcessBuilder(pythonPath, scriptPath,
                        "--name=" + name, "--gender=" + gender, "--weight=" + Double.toString(weight),
                        "--height=" + Double.toString(height), "--age=" + Integer.toString(age), "--sport=" + Integer.toString(sport));
            }

            // 将标准输出和标准错误输出合并
            pb.redirectErrorStream(true);
            // 启动子进程
            Process process = pb.start();

            // 读取子进程的输出
            InputStream inputStream = process.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            while ((line = reader.readLine()) != null && line.compareTo("food_suggest start:") != 0) {
                System.out.println(line);
                continue;
            }

            while ((line = reader.readLine()) != null) {
                System.out.println(line);
                al.add(line);
            }

            reader.close();
            // 等待子进程结束
            int exitCode = process.waitFor();
            return al;
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return al;
    }
    public ArrayList<String> start_ocr() {
        ArrayList<String> al = new ArrayList<>();
        ProcessBuilder pb = null;
        try {
            if (os.startsWith("Windows")) {
                String pythonPath = "D:\\yang\\Anaconda\\envs\\paddle_env\\python.exe"; // 虚拟环境中 Python 解释器的路径
                pb = new ProcessBuilder(pythonPath, rootPath + "py_project\\ocr-test\\nutrition_ocr.py");
            }
            else{
                // 设置环境变量
                String virtualEnv = "/root/anaconda3/envs/paddle_env"; // 虚拟环境名称
                Map<String, String> envMap = new HashMap<>(System.getenv());
                String path = envMap.get("PATH");
                path += ":" + virtualEnv + "/bin";
                envMap.put("PATH", path);
                // 获取 Python 解释器路径
                String pythonPath = virtualEnv + "/bin/python";
                String scriptPath = rootPath + "py_project/ocr-test/nutrition_ocr.py";
                // 创建 ProcessBuilder 对象
                pb = new ProcessBuilder(pythonPath, scriptPath);
            }

            // 将标准输出和标准错误输出合并
            pb.redirectErrorStream(true);
            // 启动子进程
            Process process = pb.start();

            // 读取子进程的输出
            InputStream inputStream = process.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            while ((line = reader.readLine()) != null && line.compareTo("OCR start:") != 0) {
                System.out.println(line);
                continue;
            }

            while ((line = reader.readLine()) != null) {
                System.out.println(line);
                al.add(line);
            }

            reader.close();
            // 等待子进程结束
            int exitCode = process.waitFor();
            return al;
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return al;
    }

    public ArrayList<String> start_yolo() {
        ArrayList<String> al = new ArrayList<>();
        ProcessBuilder pb = null;
        try {
            if (os.startsWith("Windows")) {
                // 设置环境变量
                String pythonPath = "D:\\yang\\Anaconda\\envs\\deeplearningPytorch\\python.exe"; // 虚拟环境中 Python 解释器的路径
                String scriptPath = rootPath + "py_project\\yolo-test\\yolov5\\detect.py";

                // 创建 ProcessBuilder 对象
                pb = new ProcessBuilder(pythonPath, scriptPath,
                        "--source=D:\\yang\\project\\yolo-test\\yolov5\\data\\images\\food.jpg",
                        "--weights=D:\\yang\\project\\yolo-test\\yolov5\\weights\\best.pt", "--conf=0.4", "--save-crop");
            }
            else{
                // 设置环境变量
                String virtualEnv = "/root/anaconda3/envs/yolo_env"; // 虚拟环境名称
                Map<String, String> envMap = new HashMap<>(System.getenv());
                String path = envMap.get("PATH");
                path += ":" + virtualEnv + "/bin";
                envMap.put("PATH", path);
                // 获取 Python 解释器路径
                String pythonPath = virtualEnv + "/bin/python";
                String scriptPath = rootPath + "py_project/yolo-test/yolov5/detect.py";

                // 创建 ProcessBuilder 对象
                pb = new ProcessBuilder(pythonPath, scriptPath,
                        "--source=" + rootPath + "py_project/yolo-test/yolov5/data/images/food.jpg",
                        "--weights=" + rootPath + "py_project/yolo-test/yolov5/weights/best.pt", "--conf=0.4", "--save-crop");
            }


            // 设置环境变量，强制指定 Python 的字符编码为 UTF-8
            pb.environment().put("PYTHONIOENCODING", "utf-8");
            // 将标准输出和标准错误输出合并
            pb.redirectErrorStream(true);
            // 启动子进程
            Process process = pb.start();

            // 读取子进程的输出
            InputStream inputStream = process.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            String line;

            while ((line = reader.readLine()) != null && line.compareTo("YOLO start:") != 0) {
                System.out.println(line);
                continue;
            }

            while ((line = reader.readLine()) != null) {
                System.out.println(line);
                al.add(line);
            }

            reader.close();
            // 等待子进程结束
            int exitCode = process.waitFor();
//            System.out.println(al);

            return al;
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }

        return al;
    }

    public static void main(String[] args) {
        PythonRunner pythonRunner = new PythonRunner();
        pythonRunner.start_yolo();
    }

}
