import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import javax.sql.DataSource;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import db.User;
import db.UserDao;
import jdk.nashorn.internal.parser.JSONParser;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: yang
 * Date: 2023-05-04
 * Time: 19:59
 */


class Food {
    private String name;
    private double mass;

    public Food(String name, double mass) {
        this.name = name;
        this.mass = mass;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMass(double mass) {
        this.mass = mass;
    }

    public String getName() {
        return name;
    }

    public double getMass() {
        return mass;
    }
}

class oneDayDish{
    public List<Food> breakfast;
    public List<Food> lunch;
    public List<Food> dinner;

    public oneDayDish() {
        this.breakfast = new ArrayList<>();
        this.lunch = new ArrayList<>();
        this.dinner = new ArrayList<>();
    }
}



@MultipartConfig
@WebServlet("/AI")
public class FoodAIServlet extends HttpServlet {
    ObjectMapper objectMapper = new ObjectMapper();


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        System.out.printf("接收到客户端请求!\n");
        req.setCharacterEncoding("utf8");
        String flag = req.getParameter("flag");
        switch (flag){
            case "1":
                yolo_process(req,resp);//不传递参数,在方法中直接调用类成员
                break;
            case "2":
                ocr_process(req,resp);
                break;
            case "3":
                suggest_process(req,resp);
                break;
            case "register":
                register_process(req,resp);//不传递参数,在方法中直接调用类成员
                break;
            case "login":
                login_process(req,resp);//不传递参数,在方法中直接调用类成员
                break;
            default:
                break;
        }

    }

    private void login_process(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        System.out.printf("接受到了的登录请求 \n");
        // 接收客户端的用户名和密码
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        System.out.println("用户名:"+username);
        System.out.println("密码:"+password);

        try {
            if (!UserDao.checkLogin(username, password)) {
                // 如果密码错误，返回一个错误响应
                resp.sendError(HttpServletResponse.SC_BAD_REQUEST, "用户名不存在或密码错误");
                System.out.println("用户名不存在或密码错误");
            } else {
                // 返回一个成功响应
                resp.setStatus(HttpServletResponse.SC_OK);
                System.out.println("登录成功");
            }
        } catch (SQLException e) {
            System.out.println("数据库异常");
            e.printStackTrace();
        }
    }

    private void register_process(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        System.out.printf("接受到了的注册请求 \n");
        // 接收客户端的用户名和密码
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        System.out.println("用户名:"+username);
        System.out.println("密码:"+password);
        // 判断用户名是否存在
        try {
            if (UserDao.exists(username)) {
                // 如果用户名存在，返回一个错误响应
                resp.sendError(HttpServletResponse.SC_BAD_REQUEST, "用户名已存在");
                System.out.println("用户名已存在");
            } else {
                // 如果用户名不存在，将用户信息保存到数据库中
                UserDao.addUser(username, password);
                // 返回一个成功响应
                resp.setStatus(HttpServletResponse.SC_OK);
                System.out.println("注册成功");
            }
        } catch (SQLException e) {
            System.out.println("数据库异常");
            e.printStackTrace();
        }
    }


    private void suggest_process(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        System.out.printf("接受到了的食品推荐请求 \n");
        // 接收客户端的姓名、年龄、身高、体重
        InputStream inputStream = req.getInputStream();
        String requestBody = null;
        try {
            byte[] buffer = new byte[1024];
            int len = 0;
            StringBuilder sb = new StringBuilder();
            while ((len = inputStream.read(buffer)) != -1) {
                sb.append(new String(buffer, 0, len));
            }
            requestBody = sb.toString();
            System.out.println(requestBody);
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        User user = objectMapper.readValue(requestBody, User.class);
        PythonRunner pythonRunner = new PythonRunner();
        ArrayList<String> al = pythonRunner.start_food_suggest(user.getName(),user.getAge(),user.getGender(),user.getWeight(), user.getHeight(), user.getSport());
        String response = al.get(0);//获取python推荐返回的json列表
        resp.setContentType("application/json; charset=utf8");
        resp.getWriter().write(response);
    }

    private void ocr_process(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.printf("接受到了营养表OCR请求 \n");
        //读取图片
        Part part = null;

        part = req.getPart("OCRImage");
        if (part == null){
            System.out.println("OCR图片呢?救一下啊");
        }

        System.out.println(part.getSubmittedFileName());
        System.out.println(part.getContentType());
        System.out.println(part.getSize());
        String os = System.getProperty("os.name");
        if (os.startsWith("Windows")) {
            part.write("D:\\yang\\project\\ocr-test\\img.jpg");
        }
        else {
            part.write("/root/ysl/project/food-AI/py_project/ocr-test/img.jpg");
        }

        System.out.printf("接受到了OCR请求图片 \n");

        PythonRunner pythonRunner = new PythonRunner();
        //运行ocr识别
        ArrayList<String> al = pythonRunner.start_ocr();
        //返回检测到的json数据
        String response = al.get(0);
        System.out.println(response);
        //返回响应
        resp.setContentType("application/json; charset=utf8");
        resp.getWriter().write(response);
    }

    private void yolo_process(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        System.out.printf("接受到了目标检测请求 \n");
        //读取图片
        Part part = req.getPart("YOLOImage");
        System.out.println(part.getSubmittedFileName());
        System.out.println(part.getContentType());
        System.out.println(part.getSize());
        String os = System.getProperty("os.name");
        if (os.startsWith("Windows")) {
            part.write("D:\\yang\\project\\yolo-test\\yolov5\\data\\images\\food.jpg");
        }
        else {
            part.write("/root/ysl/project/food-AI/py_project/yolo-test/yolov5/data/images/food.jpg");
        }

        System.out.printf("接受到了目标检测请求图片 \n");

        PythonRunner pythonRunner = new PythonRunner();
        //运行ocr识别
        ArrayList<String> al = pythonRunner.start_yolo();
        //返回检测到的json数据
        String response = al.get(0);
        System.out.println(response);


        //返回响应
        resp.setContentType("application/json; charset=utf8");
        resp.getWriter().write(response);
    }
}



