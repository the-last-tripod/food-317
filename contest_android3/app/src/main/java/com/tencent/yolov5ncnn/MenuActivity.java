package com.tencent.yolov5ncnn;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tencent.yolov5ncnn.adapter.MenuAdapter;
import com.tencent.yolov5ncnn.dao.FoodDao;

public class MenuActivity extends AppCompatActivity {

    private RecyclerView menu_recyclerview;
    private FoodDao mFoodDao = MyApplication.getInstance().getMyDB().FoodDao();
    private MenuAdapter menuAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        menu_recyclerview = findViewById(R.id.menu_recyclerview);
        menu_recyclerview.setLayoutManager(new LinearLayoutManager(this));

        menu_recyclerview.setAdapter(menuAdapter);

    }
}