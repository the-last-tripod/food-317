package com.tencent.yolov5ncnn.enity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity
public class FoodIntake {
    @PrimaryKey(autoGenerate = true)
    public int id;

    @ColumnInfo
    public String date;

    @ColumnInfo
    public String food_name; //吃的食物


    public FoodIntake() {

    }
}
