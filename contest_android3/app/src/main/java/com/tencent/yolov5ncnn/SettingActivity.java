
package com.tencent.yolov5ncnn;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Dao;

import com.tencent.yolov5ncnn.dao.UserDao;
import com.tencent.yolov5ncnn.enity.User;


public class SettingActivity extends AppCompatActivity {

    private ImageView person_logo;
    private TextView person_name;
    private TextView person_account;
    private EditText person_gender;
    private EditText person_age;
    private EditText person_height;
    private EditText person_weight;
    private EditText person_identity;
    //保存按钮
    private Button btn_save;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        // 绑定组件
        person_logo = findViewById(R.id.person_logo);
        person_name = findViewById(R.id.person_name);
        person_account = findViewById(R.id.person_account);
        person_gender = findViewById(R.id.person_gender);
        person_age = findViewById(R.id.person_age);
        person_height = findViewById(R.id.person_height);
        person_weight = findViewById(R.id.person_weight);
        person_identity = findViewById(R.id.person_identity);
        btn_save = findViewById(R.id.btn_save);

        // 读取数据库
        UserDao userDao = MyApplication.getInstance().getMyDB().UserDao();
        User user = userDao.queryAll();

        // 设置用户信息到界面
        person_name.setText(user.getName());
        //默认账号
        //person_account.setText(user.getAccount());
        person_gender.setText(user.getGender());
        person_age.setText(String.valueOf(user.getAge()));
        person_height.setText(String.valueOf(user.getHeight()));
        person_weight.setText(String.valueOf(user.getWeight()));
        //person_identity.setText(user.getIdentity());

        person_gender.setEnabled(true);
        person_age.setEnabled(true);
        person_height.setEnabled(true);
        person_weight.setEnabled(true);
        person_identity.setEnabled(true);

        // 保存按钮点击事件
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 获取用户编辑后的信息
                String gender = person_gender.getText().toString();
                int age = Integer.parseInt(person_age.getText().toString());
                int height = Integer.parseInt(person_height.getText().toString());
                int weight = Integer.parseInt(person_weight.getText().toString());
                // String identity = person_identity.getText().toString();

                // 更新用户信息到数据库
                user.setGender(gender);
                user.setAge(age);
                user.setHeight(height);
                user.setWeight(weight);
                userDao.update(user);
            }
        });

        // 点击person_gender弹出选择男女选项的对话框
        person_gender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showGenderDialog();
            }
        });
    }

    private void showGenderDialog() {
        final String[] genders = {"男", "女"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("选择性别");
        builder.setItems(genders, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String selectedGender = genders[which];
                person_gender.setText(selectedGender);
            }
        });
        builder.show();
    }
}
