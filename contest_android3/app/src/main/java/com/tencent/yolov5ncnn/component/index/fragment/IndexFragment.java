package com.tencent.yolov5ncnn.component.index.fragment;

import android.content.Context;


import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;

import android.text.InputType;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.button.MaterialButton;
import com.tencent.yolov5ncnn.component.identification.activity.ChooseWayActivity;
import com.tencent.yolov5ncnn.chat.ChatMainActivity;
import com.tencent.yolov5ncnn.component.foodchoice.activity.FoodChoiceActivity;
import com.tencent.yolov5ncnn.AnalysisActivity;
import com.tencent.yolov5ncnn.MyApplication;
import com.tencent.yolov5ncnn.R;


import com.tencent.yolov5ncnn.component.index.model.ResetStepsWorker;
import com.tencent.yolov5ncnn.component.recommend.activity.RecommendActivity;
import com.tencent.yolov5ncnn.dao.FoodDao;
import com.tencent.yolov5ncnn.dao.UserDao;
import com.tencent.yolov5ncnn.enity.NutrIntake;
import com.tencent.yolov5ncnn.enity.User;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.concurrent.TimeUnit;



public class IndexFragment extends Fragment implements View.OnClickListener{
    // 声明你的视图组件
    private TextView txtFoodIntake, txtSport, txtFoodNeed, txtFoodRecommend, txtCarb, txtProtein, txtFat;
    private MaterialButton mbtnFoodRecord, mbtnNutritionalAnalysis, mbtnDietAdvice;

    private SensorManager sensorManager;
    private Sensor stepSensor;
    //跳转到chatGPT
    private Button btnChat;

    //跳转到拍照识别
    private ImageView ivCamera;

    //步数
    private TextView step_num;
    //喝水量
    private TextView water_num;
    //体重
    private TextView weight_num;
    //身高
    private TextView height_num;
    //BMI
    private TextView BMI_num;
    //best_weight
    private TextView bmi_weight;

    private FoodDao mFoodDao = MyApplication.getInstance().getMyDB().FoodDao();
    //读取数据库
    UserDao userDao = MyApplication.getInstance().getMyDB().UserDao();
    User user =userDao.queryAll();


    @Nullable
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // 使用LayoutInflater找到布局
        View view = inflater.inflate(R.layout.fragment_index, container, false);

        // 找到视图组件
        txtFoodIntake = view.findViewById(R.id.txt_food_intake_index);
        txtSport = view.findViewById(R.id.txt_sport_index);
        txtFoodNeed = view.findViewById(R.id.txt_food_need_index);
        txtFoodRecommend = view.findViewById(R.id.txt_food_recommend_index);
        txtCarb = view.findViewById(R.id.txt_carb_index);
        txtProtein = view.findViewById(R.id.txt_protein_index);
        txtFat = view.findViewById(R.id.txt_fat_index);
        mbtnFoodRecord = view.findViewById(R.id.mbtn_food_record_index);
        mbtnNutritionalAnalysis = view.findViewById(R.id.mbtn_nutritional_analysis_index);
        mbtnDietAdvice = view.findViewById(R.id.mbtn_diet_advice_index);
        btnChat = view.findViewById(R.id.btn_chat);
        ivCamera = view.findViewById(R.id.img_camera);

        step_num = view.findViewById(R.id.step_num);
        water_num = view.findViewById(R.id.water_num);

        // 获取 SensorManager 实例
        sensorManager = (SensorManager) getActivity().getSystemService(Context.SENSOR_SERVICE);

        // 获取计步传感器
        stepSensor = sensorManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);

        // 获取显示步数的 TextView
        step_num = view.findViewById(R.id.step_num);

        // 设置每日的清零任务
        PeriodicWorkRequest resetStepsRequest = new PeriodicWorkRequest.Builder(ResetStepsWorker.class, 24, TimeUnit.HOURS)
                .setInitialDelay(getTimeUntilMidnight(), TimeUnit.MILLISECONDS)
                .build();

        WorkManager.getInstance(getActivity().getApplicationContext()).enqueue(resetStepsRequest);

        //体重模块
        weight_num = view.findViewById(R.id.weight_num);
        height_num = view.findViewById(R.id.height_num);
        BMI_num = view.findViewById(R.id.BMI_num);
        bmi_weight = view.findViewById(R.id.bmi_weight);

        //设置监听
        weight_num.setOnClickListener(this);
        height_num.setOnClickListener(this);
        water_num.setOnClickListener(this);

        // 调用设置数据函数
        setData();

        // 调用处理点击事件函数
        handleClickEvents();

        //修改BMI参数
        UpdateBMI();

        // 返回视图
        return view;
    }




    /**
     * 设置数据
     */
    private void setData() {
        // 为TextView设置值为"0"
        User user = MyApplication.getInstance().getMyDB().UserDao().queryAll();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        Calendar calendar = Calendar.getInstance();
        String date = dateFormat.format(calendar.getTime());
        NutrIntake nutrIntake = MyApplication.getInstance().getMyDB().NutrIntakeDao().queryByDate(date);

        String proteinIntake;
        String carbIntake;
        String fatIntake;
        int intakeEnergy = 0;
        if(nutrIntake == null){
            proteinIntake = "0";
            carbIntake = "0";
            fatIntake = "0";
        }
        else{
            proteinIntake = String.valueOf((int)nutrIntake.protein);
            carbIntake = String.valueOf((int)nutrIntake.carb);
            fatIntake = String.valueOf((int)nutrIntake.fat);
            intakeEnergy = (int)nutrIntake.energy/4;

        }
        int recommendEnery = user.needEnergy/4;
        int diff = Math.max(0,recommendEnery-intakeEnergy);

        String proteinNeed = String.valueOf(user.needProteinMax);
        String carbNeed = String.valueOf(user.needCarbMax);
        String fatNeed = String.valueOf(user.needFatMax);


        txtFoodIntake.setText(String.valueOf(intakeEnergy));
        txtSport.setText("0");
        txtFoodNeed.setText(String.valueOf(diff));
        txtFoodRecommend.setText(String.valueOf(recommendEnery));
        txtCarb.setText(carbIntake + '/' + carbNeed + " g");
        txtProtein.setText(proteinIntake + '/' + proteinNeed + " g");
        txtFat.setText(fatIntake + '/' + fatNeed + " g");
    }

    /**
     * 处理点击事件
     */
    private void handleClickEvents() {
        ivCamera.setOnClickListener(v -> {
            startActivity(new Intent(getActivity(), ChooseWayActivity.class));

        });

        // 为MaterialButton设置点击事件
        mbtnFoodRecord.setOnClickListener(v -> {
            startActivity(new Intent(getActivity(), FoodChoiceActivity.class));

        });

        mbtnNutritionalAnalysis.setOnClickListener(v -> {
            startActivity(new Intent(getActivity(), AnalysisActivity.class));
        });

        mbtnDietAdvice.setOnClickListener(v -> {
            startActivity(new Intent(getActivity(), RecommendActivity.class));
        });

        btnChat.setOnClickListener(v -> {
            startActivity(new Intent(getActivity(), ChatMainActivity.class));
        });
    }

    @Override
    public void onClick(View v) {
        //处理点击事件
        switch (v.getId()){
            case R.id.water_num:
                //点击增加饮水量
                water_num.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                        builder.setTitle("输入饮水量（单位：毫升）");


                        final EditText input = new EditText(getContext());
                        input.setInputType(InputType.TYPE_CLASS_NUMBER);
                        builder.setView(input);


                        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                int waterNum = Integer.parseInt(input.getText().toString());
                                int originalWaterNum = Integer.parseInt(water_num.getText().toString());
                                int totalWaterNum = waterNum + originalWaterNum;
                                water_num.setText(String.valueOf(totalWaterNum));

                            }
                        });
                        builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });

                        builder.show();
                    }
                });
                break;
            case R.id.weight_num:
                //点击改变体重
                weight_num.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // 创建一个 AlertDialog.Builder 对象
                        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                        // 设置弹出框的标题
                        builder.setTitle("请输入最新体重");
                        // 创建一个 EditText 控件
                        final EditText editText = new EditText(getContext());
                        // 将 EditText 控件添加到弹出框中
                        builder.setView(editText);
                        // 设置弹出框的确认按钮
                        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                // 获取用户输入的体重值
                                String weight = editText.getText().toString();
                                // 将体重值设置到体重控件中
                                weight_num.setText(weight);
                                //获取身高体重
                                double p_weight = Double.valueOf(weight_num.getText().toString());
                                userDao.updateWeight(p_weight);
                                //修改BMI参数
                                UpdateBMI();

                            }
                        });
                        // 设置弹出框的取消按钮
                        builder.setNegativeButton("取消", null);
                        // 显示弹出框
                        builder.show();
                    }
                });
                break;
            case R.id.height_num:
                //点击改变体重
                height_num.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // 创建一个 AlertDialog.Builder 对象
                        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                        // 设置弹出框的标题
                        builder.setTitle("请输入最新身高");
                        // 创建一个 EditText 控件
                        final EditText editText = new EditText(getContext());
                        // 将 EditText 控件添加到弹出框中
                        builder.setView(editText);
                        // 设置弹出框的确认按钮
                        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                // 获取用户输入的身高值
                                String height = editText.getText().toString();
                                // 将身高值设置到身高控件中
                                height_num.setText(height);
                                //获取身高体重
                                double p_height = Double.valueOf(height_num.getText().toString());
                                userDao.updateHeight(p_height);

                                //修改BMI参数
                                UpdateBMI();
                            }
                        });
                        // 设置弹出框的取消按钮
                        builder.setNegativeButton("取消", null);
                        // 显示弹出框
                        builder.show();
                    }
                });
                break;
        }
    }


    /**
     * 根据体重身高更新BMI
     */
    private void UpdateBMI() {

        //计算BIM值
        double bmi = Double.valueOf(weight_num.getText().toString()) / Math.pow(Double.valueOf(height_num.getText().toString()) / 100, 2); // 用 Math.pow 计算身高的平方
        String formatBMI = "%.0f"; // %.0f 控制保留一位小数并去掉小数点
        BMI_num.setText(String.format(formatBMI, bmi));


        if(bmi>=18.5&&bmi<=23.9){
            //正常
            bmi_weight.setText("正常");

        }else if(bmi>24.0&&bmi<=27.9){
            //超重
            bmi_weight.setText("超重");

        }else if(bmi>=28.0){
            //肥胖
            bmi_weight.setText("肥胖");
        }else if(bmi<18.5){
            //过轻
            bmi_weight.setText("过轻");
        }


    }
    @Override
    public void onResume() {
        super.onResume();
        sensorManager.registerListener(stepCounterListener, stepSensor, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    public void onPause() {
        super.onPause();
        sensorManager.unregisterListener(stepCounterListener);
    }
    private SensorEventListener stepCounterListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent event) {
            // 检查传感器类型是否为步数传感器
            if (event.sensor.getType() == Sensor.TYPE_STEP_COUNTER) {
                // 获取应用的 SharedPreferences 实例
                SharedPreferences sharedPreferences = getActivity().getSharedPreferences("app", Context.MODE_PRIVATE);

                // 从 SharedPreferences 中获取午夜时刻的步数，默认为0
                int stepsAtMidnight = sharedPreferences.getInt("stepsAtMidnight", 0);

                // 获取当前的总步数
                int totalSteps = (int) event.values[0];

                // 保存当前的总步数
                sharedPreferences.edit().putInt("totalSteps", totalSteps).apply();

                // 计算今天的步数
                int stepsToday = totalSteps - stepsAtMidnight;

                // 在这里你可以处理今天的步数数据，例如更新UI显示
                step_num.setText(String.valueOf(stepsToday));
            }
        }


        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }
    };
    private long getTimeUntilMidnight() {
        // 获取当前时间的 Calendar 实例
        Calendar c = Calendar.getInstance();
        long currentTime = c.getTimeInMillis();

        // 将时间设置为今天的午夜时刻
        c.add(Calendar.DAY_OF_MONTH, 1); // 将日期加一天，表示明天的日期
        c.set(Calendar.HOUR_OF_DAY, 0); // 将小时设置为0，表示午夜
        c.set(Calendar.MINUTE, 0); // 将分钟设置为0
        c.set(Calendar.SECOND, 0); // 将秒钟设置为0
        c.set(Calendar.MILLISECOND, 0); // 将毫秒设置为0

        // 获取午夜时刻的时间戳
        long midnightTonight = c.getTimeInMillis();

        // 计算距离午夜时刻还有多长时间
        long timeUntilMidnight = midnightTonight - currentTime;

        // 返回距离午夜时刻的时间间隔
        return timeUntilMidnight;
    }

}





