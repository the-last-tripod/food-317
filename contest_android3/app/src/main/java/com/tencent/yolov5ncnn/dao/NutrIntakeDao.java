package com.tencent.yolov5ncnn.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.tencent.yolov5ncnn.enity.NutrIntake;


@Dao
public interface NutrIntakeDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(NutrIntake ... nutrIntake);

    @Update
    void update(NutrIntake ... nutrIntakes);

    @Query("UPDATE nutrintake SET protein = :protein, carb = :carb, fat = :fat, " +
            "energy = :energy, salt = :salt WHERE date = :date")
    void update(String date, double protein, double carb, double fat, double energy, double salt);


    @Query("SELECT * FROM NutrIntake WHERE date = :date")
    NutrIntake queryByDate(String date);
}
