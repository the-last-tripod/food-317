package com.tencent.yolov5ncnn.component.login.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.qmuiteam.qmui.util.QMUIStatusBarHelper;
import com.tencent.yolov5ncnn.MyApplication;
import com.tencent.yolov5ncnn.R;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

public class RegisterActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_layout);
        //设置沉浸式状态栏，即设置状态栏颜色为透明
        QMUIStatusBarHelper.translucent(this);
        // 设置状态栏字体颜色为黑色
        QMUIStatusBarHelper.setStatusBarLightMode(this);
        // 绑定组件
        EditText usernameEditText = findViewById(R.id.edit_account);
        EditText passwordEditText1 = findViewById(R.id.edit_psd_1);
        EditText passwordEditText2 = findViewById(R.id.edit_psd_2);
        Button signUpButton = findViewById(R.id.btn_signup);
        TextView signInTextView = findViewById(R.id.txt_signin);
        // 设置按钮监听
        signUpButton.setOnClickListener(view -> {

            String username = usernameEditText.getText().toString();
            String password1 = passwordEditText1.getText().toString();
            String password2 = passwordEditText2.getText().toString();

            if (!isUsernameValid(username)) {
                Toast.makeText(getApplicationContext(), "账号需要为6~10位字符", Toast.LENGTH_SHORT).show();
                return;
            }

            if (!isPasswordValid(password1)) {
                Toast.makeText(getApplicationContext(), "密码需要为6~10位字符", Toast.LENGTH_SHORT).show();
                return;
            }

            if (!password1.equals(password2)) {
                Toast.makeText(getApplicationContext(), "两次密码不一致", Toast.LENGTH_SHORT).show();
                return;
            }

            // 存储账号密码到 SharedPreferences
            SharedPreferences sharedPreferences = getSharedPreferences("user_info", MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("username", username);
            editor.putString("password", password1);
            editor.apply();

            //发送用户名和密码进行注册
            new RegisterTask(username,password1).execute();
        });
        // 设置TextView点击监听
        signInTextView.setOnClickListener(new View.OnClickListener() { // 增加这个监听器
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RegisterActivity.this, LogInActivity.class);
                startActivity(intent);
            }
        });
    }

    private class RegisterTask extends AsyncTask<Void, Void, Boolean> {
        private Exception exception;
        private String userName;
        private String pwd;
        RegisterTask(String userName, String pwd){
            super();
            this.userName = userName;
            this.pwd = pwd;
        }

        protected Boolean doInBackground(Void... voids) {
            try {
                String encodedUsername = URLEncoder.encode(userName, "UTF-8");
                String encodedPassword = URLEncoder.encode(pwd, "UTF-8");
                URL url = new URL(MyApplication.getIP()+"AI?flag=register&username=" + encodedUsername + "&password=" + encodedPassword);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setRequestMethod("POST");

                // 获取响应状态码
                int statusCode = conn.getResponseCode();
                if (statusCode == HttpURLConnection.HTTP_OK) {
                    conn.disconnect();
                    return true;
                } else {
                    conn.disconnect();
                    return false;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return false;
        }

        protected void onPostExecute(Boolean result) {
            if (result){
                Toast.makeText(getApplicationContext(), "注册成功", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent();
                intent.setClass(RegisterActivity.this, LogInActivity.class);
                startActivity(intent);
            }
            else{
                Toast.makeText(getApplicationContext(), "用户名已存在", Toast.LENGTH_SHORT).show();
            }
        }
    }
    /**
     * 参数：username - 待验证的用户名
     * 返回值：布尔值，用户名是否有效
     * 功能：检查用户名是否满足6到10个字符的长度要求
     */
    private boolean isUsernameValid(String username) {
        return username.length() >= 6 && username.length() <= 10;
    }
    /**
     * 参数：password - 待验证的密码
     * 返回值：布尔值，密码是否有效
     * 功能：检查密码是否满足6到10个字符的长度要求
     */
    private boolean isPasswordValid(String password) {
        return password.length() >= 6 && password.length() <= 10;
    }
}