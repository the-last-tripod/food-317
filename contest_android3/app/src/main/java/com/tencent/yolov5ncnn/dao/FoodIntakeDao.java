package com.tencent.yolov5ncnn.dao;

import androidx.room.Dao;
import androidx.room.Database;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.tencent.yolov5ncnn.enity.Food;
import com.tencent.yolov5ncnn.enity.FoodIntake;

import java.util.List;

@Dao
public interface FoodIntakeDao {
    @Insert
    void insert(FoodIntake... foodIntakes);

    // 根据食物名字删除信息
    @Query("DELETE FROM FoodIntake where food_name = :name")
    void deleteByName(String name);

    // 根据日期删除信息
    @Query("DELETE FROM FoodIntake where date = :date")
    void deleteByDate(String date);

    //根据日期和食物名称删除信息
    @Delete
    void delete(FoodIntake ... intakes);

    //根据日期和食物名称更新信息
    @Update
    void update(FoodIntake ... intakes);

    // 根据日期加载食品摄入
    @Query("SELECT * FROM FoodIntake WHERE date = :date ORDER BY id DESC limit 1")
    List<FoodIntake> queryByDate(String date);
}
