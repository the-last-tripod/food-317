package com.tencent.yolov5ncnn.component.identification.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.tencent.yolov5ncnn.MyApplication;
import com.tencent.yolov5ncnn.R;
import com.tencent.yolov5ncnn.dao.NutrIntakeDao;
import com.tencent.yolov5ncnn.enity.NutrIntake;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.Locale;

public class OCRdisminateActivity extends AppCompatActivity {

    //ocr识别的图片
    private ImageView ocr_food_image;

    private EditText ocr_food_protein;

    private EditText ocr_food_fat;

    private EditText ocr_food_carbs;

    private EditText ocr_food_calorie;

    private EditText ocr_food_salt;

    private Button ocr_leading_in;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ocr_discriminate);

        //绑定组件
        ocr_food_image = findViewById(R.id.ocr_food_image);
        ocr_food_protein = findViewById(R.id.ocr_food_protein);
        ocr_food_fat = findViewById(R.id.ocr_food_fat);
        ocr_food_carbs = findViewById(R.id.ocr_food_carbs);
        ocr_food_calorie = findViewById(R.id.ocr_food_calorie);
        ocr_food_salt = findViewById(R.id.ocr_food_salt);
        ocr_leading_in = findViewById(R.id.ocr_leading_in);

        //接收传来的数据
        Intent intent = getIntent();
        String protein_s = intent.getStringExtra("protein");
        String fat_s = intent.getStringExtra("fat");
        String carbs_s = intent.getStringExtra("tanshui");
        String calorie_s = intent.getStringExtra("energy");
        String salt_s = intent.getStringExtra("na");
        if (protein_s == null)protein_s = "0";
        if (fat_s == null)fat_s = "0";
        if (carbs_s == null)carbs_s = "0";
        if (calorie_s == null)calorie_s = "0";
        if (salt_s == null)salt_s = "0";

        ocr_food_protein.setText(protein_s);
        ocr_food_fat.setText(fat_s);
        ocr_food_calorie.setText(calorie_s);
        ocr_food_carbs.setText(carbs_s);
        ocr_food_salt.setText(salt_s);


        ocr_leading_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //获取数据
                double protein = Double.parseDouble(ocr_food_protein.getText().toString());
                double fat = Double.parseDouble(ocr_food_fat.getText().toString());
                double carbs = Double.parseDouble(ocr_food_carbs.getText().toString());
                double calorie = Double.parseDouble(ocr_food_calorie.getText().toString());
                double salt = Double.parseDouble(ocr_food_salt.getText().toString());
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                String date = dateFormat.format(new Date());
                NutrIntakeDao nutrIntakeDao = MyApplication.getInstance().getMyDB().NutrIntakeDao();
                NutrIntake nutrIntake = nutrIntakeDao.queryByDate(date);

                //判断数据库中是否有该日期,没有则插入该日期数据
                if(nutrIntake == null){
                    Log.d("tmp",date);
                    nutrIntake = new NutrIntake(date);
                    nutrIntakeDao.insert(nutrIntake);
                }

                Log.d("ysl",String.valueOf(calorie));
                nutrIntakeDao.update(date, nutrIntake.protein + protein, nutrIntake.carb + carbs,
                        nutrIntake.fat + fat, nutrIntake.energy + calorie, nutrIntake.salt + salt);
                Toast.makeText(getApplicationContext(),"信息已经录入",Toast.LENGTH_SHORT).show();
                finish();
            }
        });

    }
}
