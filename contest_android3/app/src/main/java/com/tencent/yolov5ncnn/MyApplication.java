package com.tencent.yolov5ncnn;

import android.app.Application;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.room.Room;

import com.tencent.yolov5ncnn.dao.NutrIntakeDao;
import com.tencent.yolov5ncnn.dao.UserDao;
import com.tencent.yolov5ncnn.database.MyDatabase;
import com.tencent.yolov5ncnn.enity.Food;
import com.tencent.yolov5ncnn.enity.NutrIntake;
import com.tencent.yolov5ncnn.enity.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Random;
import java.util.Scanner;

public class MyApplication extends Application {

    private static MyApplication mApp;

    // 声明一个数据库对象
    private MyDatabase myDatabase;

    private static String serverIP = "http://81.70.250.7:8080/food_servlet/";
    private static String AIserverIP = "81.70.250.7";

    public static MyApplication getInstance() {
        return mApp;
    }
    public static String getIP() {
        return serverIP;
    }
    public static String getAIIP() {
        return AIserverIP;
    }


    //在App启动时调用
    @Override
    public void onCreate() {
        super.onCreate();
        mApp = this;

        // 构建数据库的实例
        myDatabase = Room.databaseBuilder(this, MyDatabase.class, "myDataBase")
                // 允许迁移数据库（发生数据库变更时，Room默认删除原数据库再创建新数据库。如此一来原来的记录会丢失，故而要改为迁移方式以便保存原有记录）
                .addMigrations()
                // 允许在主线程中操作数据库（Room默认不能在主线程中操作数据库）
                .allowMainThreadQueries()
                .build();

        if (getMyDB().FoodDao().queryAll().size() == 0) {
            // 如果食物数据库中没有数据，则插入数据
            init_database();
        }

        UserDao userDao = getMyDB().UserDao();
        //在没有用户时自动创建一个用户,为保证只有一个用户所以后面User表禁止使用任何insert语句!!!
        if (userDao.queryAll() == null){
            User user = new User();
            user.name = "default";
            user.age = 20;
            user.height = 185;
            user.weight = 75;
            user.setGender("男");
            user.sport = 0;

            userDao.insert(user);

        }

        NutrIntakeDao nutrIntakeDao = MyApplication.getInstance().getMyDB().NutrIntakeDao();
        if(nutrIntakeDao.queryByDate("default") == null){
            nutrIntakeDao.insert(new NutrIntake("default"));
        }

        //设置近五天摄入的营养元素
        for (int i = 4; i >= 0 ; i--) {
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DATE, -i);
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
            String date = dateFormat.format(calendar.getTime());

            NutrIntake nutrIntake = new NutrIntake(date);
            Random random = new Random();
            nutrIntake.energy = (double)random.nextInt(4000) + 6000;
            nutrIntake.protein = (double)random.nextInt(100) + 50;
            nutrIntake.carb = (double)random.nextInt(150) + 150;
            nutrIntake.fat = (double)random.nextInt(40) + 30;
            nutrIntakeDao.insert(nutrIntake);
        }

        new ConnectTask().execute();//分析用户个人营养情况


    }


    //在App终止时调用
    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    //在配置改变时调用，例如从竖屏变为横屏。
    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    // 获取数据库的实例
    public MyDatabase getMyDB() {
        return myDatabase;
    }

    //获取营养分析结果
    private class ConnectTask extends AsyncTask<Void, Void, String> {
        private Exception exception;

        protected String doInBackground(Void... voids) {
            try {
                UserDao userDao = MyApplication.getInstance().getMyDB().UserDao();
                User user = userDao.queryAll();
                String response = null;
                URL url = null;
                url = new URL(getIP()+"AI?flag=3");
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                try {
                    urlConnection.setRequestMethod("POST");
                    urlConnection.setRequestProperty("Content-Type", "application/json");
                    urlConnection.setRequestProperty("Accept", "application/json");
                    urlConnection.setDoOutput(true);
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("name", user.name);
                    jsonObject.put("gender", user.getGender());
                    jsonObject.put("age", user.age);
                    jsonObject.put("height", user.height);
                    jsonObject.put("weight", user.weight);
                    jsonObject.put("sport", user.sport);
                    String jsonReq = jsonObject.toString();

                    Log.d("ysl", jsonReq);
                    OutputStream outputStream = urlConnection.getOutputStream();
                    outputStream.write(jsonReq.getBytes());
                    outputStream.close();
                    //接收resp
                    Scanner scan = new Scanner(urlConnection.getInputStream());
                    StringBuilder sb = new StringBuilder();
                    while (scan.hasNext()){
                        String respCut = scan.next();
                        sb.append(respCut);
                    }
                    String jsonResp = sb.toString();

                    return jsonResp;
                } finally {
                    urlConnection.disconnect();
                }
            } catch (Exception e) {
                this.exception = e;
                e.printStackTrace();
                return null;
            }
        }

        protected void onPostExecute(String result) {
            if (result != null) {
                Log.d("ysl", result);
                try {
                    JSONObject json = new JSONObject(result);
                    Log.d("ysl",result);
                    JSONObject jsonObject = new JSONObject(result);
                    JSONObject suggestor = jsonObject.getJSONObject("suggestor");
                    //根据个人信息推荐的营养参数
                    double fatMax = suggestor.getDouble("fat_need_max");
                    double fatMin = suggestor.getDouble("fat_need_min");
                    double energy = suggestor.getDouble("energy");
                    double carbMax = suggestor.getDouble("carb_need_max");
                    double carbMin = suggestor.getDouble("carb_need_min");
                    double proteinMax = suggestor.getDouble("protein_need_max");
                    double proteinMin = suggestor.getDouble("protein_need_min");
                    UserDao userDao = MyApplication.getInstance().getMyDB().UserDao();
                    userDao.updateNutriInfo((int)energy,(int)proteinMax,(int)proteinMin,(int)carbMax,(int)carbMin,(int)fatMax,(int)fatMin);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.d("jsonResp", "Error: result is null");
            }
        }
    }

    private void init_database(){
        Food food0 = new Food("啤酒鸡","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_big_photo_url_9ff43034f86c2a65de503ebd9232fcec.jpg",7.13,0.99,422.77,0.06,5.65,"肉蛋奶");
        getMyDB().FoodDao().insert(food0);
        Food food1 = new Food("速成腌白菜","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_6fd9e0b72ec549edbf346f82c23c49f8.jpg",1.62,3.39,121.39,0.87,1.17,"蔬果");
        getMyDB().FoodDao().insert(food1);
        Food food2 = new Food("牛奶炖蛋","http://s.boohee.cn/house/upload_food/2016/7/11/mid_photo_url_0d5db509079a4a74aa58634750bfeac7.jpg",2.55,10.21,242.78,0.81,1.15,"肉蛋奶");
        getMyDB().FoodDao().insert(food2);
        Food food3 = new Food("豆浆","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_bdafcc805de0211152be929b17594ebe.jpg",3.0,1.2,129.76,0.0,1.6,"汤");
        getMyDB().FoodDao().insert(food3);
        Food food4 = new Food("大盘鸡","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_9746893cd0ee4a57e225113ca312ba31.jpg",10.38,5.76,464.62,0.7,5.33,"肉蛋奶");
        getMyDB().FoodDao().insert(food4);
        Food food5 = new Food("花生四果汤","http://s.boohee.cn/house/upload_food/2022/1/19/slim_1214791_1324385763mid.jpg",5.35,37.49,799.49,2.08,2.62,"汤");
        getMyDB().FoodDao().insert(food5);
        Food food6 = new Food("豆豉鲮鱼蒸鸡蛋","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_49f9b0ba7a294f99bb9a394e14c7390f.jpg",23.33,6.3,1268.3,0.03,20.54,"肉蛋奶");
        getMyDB().FoodDao().insert(food6);
        Food food7 = new Food("咖喱鸡块","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_dd5aa95baad6dde035c52af2fdee9124.jpg",10.23,6.03,560.9,0.3,7.71,"肉蛋奶");
        getMyDB().FoodDao().insert(food7);
        Food food8 = new Food("番茄土豆","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_3fb5d960dc12e86fa4bb24a4195ff1f9.jpg",1.03,5.76,209.29,0.56,2.71,"肉蛋奶");
        getMyDB().FoodDao().insert(food8);
        Food food9 = new Food("番茄酸汤肥牛煲","http://s.boohee.cn/house/upload_food/2021/6/11/big_photo_url_6bcb9f0dbeed04c5d35c739b19d31759.jpg",7.4,3.05,435.32,0.45,6.76,"肉蛋奶");
        getMyDB().FoodDao().insert(food9);
        Food food10 = new Food("四川凉拌酸辣粉丝","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_2ab83a854d1ab4cc24ba69a38b533857.jpg",0.51,19.41,468.81,0.31,3.68,"肉蛋奶");
        getMyDB().FoodDao().insert(food10);
        Food food11 = new Food("炒素三丝","http://s.boohee.cn/house/upload_food/2021/6/23/big_photo_url_4fa9a64ea63e7212ae1dd57663457d5e.jpg",5.12,45.51,916.69,0.71,2.12,"肉蛋奶");
        getMyDB().FoodDao().insert(food11);
        Food food12 = new Food("豆皮寿司","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_bbb3f3b93cab2e33ade300dedb1677d0.jpg",14.37,19.89,841.35,0.93,7.66,"肉蛋奶");
        getMyDB().FoodDao().insert(food12);
        Food food13 = new Food("肉末茄子","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_big_photo_url_b9fad6f87d77713d580b6943a59da31b.jpg",4.73,5.03,661.36,0.08,13.7,"肉蛋奶");
        getMyDB().FoodDao().insert(food13);
        Food food14 = new Food("煮豆沙馅儿","http://s.boohee.cn/house/upload_food/2022/1/18/slim_mid_photo_url_c688e4b1d46788bb9a8e431115a75d05.jpg",1.7,45.0,828.79,1.0,2.5,"肉蛋奶");
        getMyDB().FoodDao().insert(food14);
        Food food15 = new Food("酱牛肉","http://s.boohee.cn/house/upload_food/2022/1/18/slim_b_big_photo_url_e9a08aa5d171150c37a18641b4008444.jpg",31.4,3.2,1029.71,0.0,11.9,"肉蛋奶");
        getMyDB().FoodDao().insert(food15);
        Food food16 = new Food("蒸水蛋","http://s.boohee.cn/house/upload_food/2022/1/18/slim_mid_photo_url_c735aec9107229a1863557ac55e03e31.jpg",6.6,48.0,1594.79,1.0,17.4,"肉蛋奶");
        getMyDB().FoodDao().insert(food16);
        Food food17 = new Food("花生梨米糊","http://s.boohee.cn/house/upload_food/2022/1/18/slim_205484_1241345030mid.jpg",0.82,4.45,113.02,0.4,0.73,"主食");
        getMyDB().FoodDao().insert(food17);
        Food food18 = new Food("蒜香咸鱼干焖茄子","http://s.boohee.cn/house/upload_food/2021/2/23/mid_photo_url_4969a72b87865ef2cf35b72cc10945a8.jpg",1.4,6.81,117.2,0.1,0.11,"肉蛋奶");
        getMyDB().FoodDao().insert(food18);
        Food food19 = new Food("老鸭汤","http://s.boohee.cn/house/upload_food/2022/3/1/big_photo_url_564cc75a372e2fe695f5d6785b8037fc.jpg",3.95,1.52,301.38,0.4,5.73,"汤");
        getMyDB().FoodDao().insert(food19);
        Food food20 = new Food("朝阳鸡蛋豆腐","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_85d4247406b14d50921e9ce8b4ee59d8.jpg",8.82,4.39,594.38,0.02,10.14,"肉蛋奶");
        getMyDB().FoodDao().insert(food20);
        Food food21 = new Food("原味烧牛肉","http://s.boohee.cn/house/upload_food/2022/1/18/slim_mid_photo_url_9cbb8605a0e985ec128e14ad1aaaf076.jpg",27.4,1.4,535.78,0.0,1.2,"肉蛋奶");
        getMyDB().FoodDao().insert(food21);
        Food food22 = new Food("三鲜豆腐","http://s.boohee.cn/house/upload_food/2021/7/19/big_photo_url_ca4d60510bfbc76ca53244c8fd74cbca.jpg",5.58,6.62,385.09,0.12,4.89,"蔬果");
        getMyDB().FoodDao().insert(food22);
        Food food23 = new Food("溜藕片","http://s.boohee.cn/house/upload_food/2022/1/18/slim_mid_photo_url_fc40e3018b1b0e0636d34de1d6981b8b.jpg",0.9,19.1,339.05,0.0,0.0,"蔬果");
        getMyDB().FoodDao().insert(food23);
        Food food24 = new Food("腊肠手撕包菜","http://s.boohee.cn/house/upload_food/2020/6/8/big_photo_url_39411079c1e195115f768763d4e291b6.jpg",1.68,5.28,175.8,1.63,1.97,"肉蛋奶");
        getMyDB().FoodDao().insert(food24);
        Food food25 = new Food("土豆烧茄子","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_33944112924b03f5c0d37d95c27255e6.jpg",1.47,15.27,397.65,1.1,3.34,"蔬果");
        getMyDB().FoodDao().insert(food25);
        Food food26 = new Food("蒜蓉油麦菜","http://s.boohee.cn/house/upload_food/2020/6/11/big_photo_url_41e837c7a4c8e314dfca247ed3654e79.jpg",1.44,2.25,192.55,0.59,3.48,"蔬果");
        getMyDB().FoodDao().insert(food26);
        Food food27 = new Food("芦笋炒香干","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_big_photo_url_d3f08d864a230720ad21ba2aa1d34d3b.jpg",2.83,4.19,447.88,1.05,9.01,"蔬果");
        getMyDB().FoodDao().insert(food27);
        Food food28 = new Food("素鸭","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_df555d3cef32048ef454b18a9f5a864e.jpg",4.26,18.44,883.2,0.91,13.51,"蔬果");
        getMyDB().FoodDao().insert(food28);
        Food food29 = new Food("清炒上海青","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_89191abcb5cd0e0619942c4e71b8638f.jpg",1.44,3.2,129.76,1.34,1.74,"蔬果");
        getMyDB().FoodDao().insert(food29);
        Food food30 = new Food("海蜇马蹄汤","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_9afd2a199bde43baafdc4448791a64f6.jpg",2.26,4.16,121.39,1.93,0.25,"汤");
        getMyDB().FoodDao().insert(food30);
        Food food31 = new Food("肉末蒸豆腐","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_f908859c96b2adb1881610cbec85f48e.jpg",5.64,3.33,401.84,0.46,6.76,"肉蛋奶");
        getMyDB().FoodDao().insert(food31);
        Food food32 = new Food("麻酱菠菜","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_big_photo_url_3ba54138a9662b47db145cfb788fa71e.jpg",3.39,6.16,263.71,1.79,3.15,"蔬果");
        getMyDB().FoodDao().insert(food32);
        Food food33 = new Food("烧日式肉松饭团","https://t9.baidu.com/it/u=1191751551,3787006159&fm=74&app=80&size=f256,256&n=0&f=JPEG&fmt=auto?sec=1684083600&t=8b9fe1d4b7ccaea082e07273818b4203",5.58,22.57,544.15,0.43,1.78,"主食");
        getMyDB().FoodDao().insert(food33);
        Food food34 = new Food("肉丸糊辣汤","http://s.boohee.cn/house/upload_food/2021/6/25/big_photo_url_010c42b2448005ec6553c41bbccfdcae.jpg",2.82,7.07,171.62,0.86,0.33,"汤");
        getMyDB().FoodDao().insert(food34);
        Food food35 = new Food("萝卜腐竹煲","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_4639d1736746108ed7e6d5979e0467c8.jpg",3.56,1.69,142.32,0.22,1.51,"蔬果");
        getMyDB().FoodDao().insert(food35);
        Food food36 = new Food("鱼香茄子","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_618a2e548af22512daceba4168174324.jpg",3.82,8.75,326.49,0.24,3.62,"肉蛋奶");
        getMyDB().FoodDao().insert(food36);
        Food food37 = new Food("胡萝卜猪肉炒豆腐","http://s.boohee.cn/house/upload_food/2021/7/9/big_photo_url_dda3416ee62e0ada7a028c2ec5fa1e02.jpg",6.41,5.45,322.31,1.18,3.66,"肉蛋奶");
        getMyDB().FoodDao().insert(food37);
        Food food38 = new Food("提丝发糕","http://s.boohee.cn/house/upload_food/2022/1/18/slim_mid_photo_url_mid_photo_url_9be9e22b56bdeedb626981c367b5617e.jpg",6.7,56.7,2310.56,1.0,33.3,"肉蛋奶");
        getMyDB().FoodDao().insert(food38);
        Food food39 = new Food("白切鸡","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_b4552d76d5af6637c37667d2df053f95.jpg",16.06,2.15,665.54,0.26,9.01,"肉蛋奶");
        getMyDB().FoodDao().insert(food39);
        Food food40 = new Food("清蒸羊肉","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_4a4f80e1b3154e568e8efb49dff3958e.jpg",9.34,2.24,284.63,0.74,2.51,"肉蛋奶");
        getMyDB().FoodDao().insert(food40);
        Food food41 = new Food("糖醋青鱼","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_d823ff66ea7b2de97a9990abde8a2e83.jpg",13.44,3.36,506.48,0.16,5.92,"肉蛋奶");
        getMyDB().FoodDao().insert(food41);
        Food food42 = new Food("西湖牛肉羹","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_2c804936234f922edb5788dbbd9e11d7.jpg",7.05,1.84,251.15,0.02,2.64,"汤");
        getMyDB().FoodDao().insert(food42);
        Food food43 = new Food("清炒南瓜","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_big_photo_url_83a1a21c95bf61bd1176d799bac110ac.jpg",1.02,11.07,284.63,1.85,2.88,"蔬果");
        getMyDB().FoodDao().insert(food43);
        Food food44 = new Food("豆花鱼","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_e6ee8898f2cb4a228263923e6f36e562.jpg",9.36,4.06,393.47,1.34,4.83,"肉蛋奶");
        getMyDB().FoodDao().insert(food44);
        Food food45 = new Food("烤火鸡","http://s.boohee.cn/house/upload_food/2021/2/23/big_photo_url_e7417345cea75e7476052968be66305f.jpg",16.57,2.55,435.32,0.18,2.78,"肉蛋奶");
        getMyDB().FoodDao().insert(food45);
        Food food46 = new Food("腐乳肉","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_7b6a69825a4075c487abe5c0912c5846.jpg",8.02,2.44,1674.32,0.15,39.64,"肉蛋奶");
        getMyDB().FoodDao().insert(food46);
        Food food47 = new Food("发糕","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_9897ca8aeb44925e66747fa0232c50ec.jpg",1.97,16.45,389.28,0.35,2.3,"肉蛋奶");
        getMyDB().FoodDao().insert(food47);
        Food food48 = new Food("家常卤水咸牛肉","https://bkimg.cdn.bcebos.com/pic/91ef76c6a7efce1b9d16ec70cc1ae4deb48f8c54cf7c?x-bce-process=image/resize,m_lfit,w_536,limit_1",15.68,4.34,368.35,1.24,0.97,"肉蛋奶");
        getMyDB().FoodDao().insert(food48);
        Food food49 = new Food("红烧冬瓜","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_58097256af55d4376f988be5ea39e5b7.jpg",0.5,3.27,104.65,0.86,1.3,"蔬果");
        getMyDB().FoodDao().insert(food49);
        Food food50 = new Food("腊肠","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_a8f43280f8e566ec84e015526301993c.jpg",24.1,11.2,2126.39,0.0,40.7,"肉蛋奶");
        getMyDB().FoodDao().insert(food50);
        Food food51 = new Food("春卷","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_a7f395bd45cd05dc9c961e60807c9d37.jpg",6.1,34.8,1946.4,1.0,33.7,"肉蛋奶");
        getMyDB().FoodDao().insert(food51);
        Food food52 = new Food("鱼香碎滑鸡","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_d479d359c4d94860b8ec13a2367c0898.jpg",7.75,12.8,1343.64,1.29,26.87,"肉蛋奶");
        getMyDB().FoodDao().insert(food52);
        Food food53 = new Food("蒸鸡蛋羹","http://s.boohee.cn/house/upload_food/2022/1/18/slim_mid_photo_url_c735aec9107229a1863557ac55e03e31.jpg",4.0,0.0,175.8,0.0,2.8,"肉蛋奶");
        getMyDB().FoodDao().insert(food53);
        Food food54 = new Food("海鲜焗饭","http://s.boohee.cn/house/upload_food/2021/12/16/big_photo_url_f183ce4d40f0256ce88677d6db105647.jpg",9.17,15.21,565.08,0.48,4.16,"主食");
        getMyDB().FoodDao().insert(food54);
        Food food55 = new Food("蒸馒头","http://s.boohee.cn/house/upload_food/2022/1/18/slim_mid_photo_url_1945bc70b243079727cfd0670f1b753b.jpg",7.7,55.3,1146.91,0.0,2.1,"主食");
        getMyDB().FoodDao().insert(food55);
        Food food56 = new Food("香菇鸡汤","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_2fbb70461774d61002e00cfb5568701e.jpg",5.94,2.09,313.94,0.18,4.83,"汤");
        getMyDB().FoodDao().insert(food56);
        Food food57 = new Food("香橙烧猪肉条","https://bkimg.cdn.bcebos.com/pic/c9fcc3cec3fdfc039245f2875b6b9094a4c27d1e88ca?x-bce-process=image/resize,m_lfit,w_536,limit_1",25.1,34.5,1372.94,1.0,10.0,"肉蛋奶");
        getMyDB().FoodDao().insert(food57);
        Food food58 = new Food("大蹄扒海参","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_499e2eb6cc258aa2e17702e0327009c2.jpg",4.22,2.53,263.71,0.11,3.81,"肉蛋奶");
        getMyDB().FoodDao().insert(food58);
        Food food59 = new Food("木耳小白菜","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_3d8ea674fea02c9f49684f78b1b8bd2f.jpg",1.45,3.78,221.85,0.51,3.94,"蔬果");
        getMyDB().FoodDao().insert(food59);
        Food food60 = new Food("哨子面","http://s.boohee.cn/house/upload_food/2021/4/14/mid_photo_url_e37ab5bdd0ad4e7cbeed5138c5f5110a.jpg",9.0,43.1,1046.45,1.0,4.3,"主食");
        getMyDB().FoodDao().insert(food60);
        Food food61 = new Food("胡萝卜烩卷心菜","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_a0cfc81f968f9ac87eac33818cfb363f.jpg",1.29,5.0,217.66,1.09,3.35,"蔬果");
        getMyDB().FoodDao().insert(food61);
        Food food62 = new Food("豆腐拌肉松","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_big_photo_url_a4d6aac1932552e5bc6b18b2b93f7048.jpg",2.71,1.87,154.87,0.24,2.09,"肉蛋奶");
        getMyDB().FoodDao().insert(food62);
        Food food63 = new Food("盐水虾","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_3c9c2c0b7bdaf775b89da6ce19eeac51.jpg",11.37,2.2,246.96,0.04,0.5,"肉蛋奶");
        getMyDB().FoodDao().insert(food63);
        Food food64 = new Food("上海年糕","http://s.boohee.cn/house/upload_food/2020/6/5/mid_photo_url_big_photo_url_3f1f7d92f24bdaf6a769902ea64b804f.jpg",5.0,50.0,954.36,1.0,0.0,"肉蛋奶");
        getMyDB().FoodDao().insert(food64);
        Food food65 = new Food("鸡尖汤","http://s.boohee.cn/house/upload_food/2022/1/18/slim_mid_photo_url_62933fbfdf5ef8f9fbb431d6a1cfabf5.jpg",28.7,3.3,1192.95,0.0,17.6,"汤");
        getMyDB().FoodDao().insert(food65);
        Food food66 = new Food("素菜西红柿包","http://s.boohee.cn/house/upload_food/2020/8/21/m_big_photo_url_369ab051cc4c8d0ed5f8dfc2ec53deb7.jpg",8.24,37.72,636.24,1.2,3.58,"蔬果");
        getMyDB().FoodDao().insert(food66);
        Food food67 = new Food("肉茸蛋","http://s.boohee.cn/house/upload_food/2022/1/18/slim_1166151502045.jpg",11.4,4.39,606.94,0.59,9.34,"肉蛋奶");
        getMyDB().FoodDao().insert(food67);
        Food food68 = new Food("潮州鳝煲","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_b17fa10bab2e4a91847810eb16398983.jpg",7.67,4.83,473.0,0.38,7.1,"肉蛋奶");
        getMyDB().FoodDao().insert(food68);
        Food food69 = new Food("干煸四季豆","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_big_photo_url_3093ef85c9a51bb04a7048f202482cd5.jpg",3.51,5.95,267.89,2.11,3.84,"蔬果");
        getMyDB().FoodDao().insert(food69);
        Food food70 = new Food("花生牛奶","https://bkimg.cdn.bcebos.com/pic/7af40ad162d9f2d34879eb41a4ec8a136327cc30?x-bce-process=image/resize,m_lfit,w_536,limit_1",0.7,4.5,133.95,0.0,1.2,"肉蛋奶");
        getMyDB().FoodDao().insert(food70);
        Food food71 = new Food("豆腐炒青蒜","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_bb50857a245a436dbb687a68994f0271.jpg",10.19,8.04,950.18,0.27,17.37,"蔬果");
        getMyDB().FoodDao().insert(food71);
        Food food72 = new Food("梅菜芋头扣肉","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_2a49f4b51f315bcc86d92f6b64032335.jpg",8.31,9.98,1628.28,5.06,36.24,"肉蛋奶");
        getMyDB().FoodDao().insert(food72);
        Food food73 = new Food("酸辣汤","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_ad8f508dc9821bad3110ffeeb7c2fb77.jpg",1.94,3.26,167.43,0.07,2.15,"汤");
        getMyDB().FoodDao().insert(food73);
        Food food74 = new Food("香菇蛋包饭","https://bkimg.cdn.bcebos.com/pic/b3fb43166d224f4a2cb96b3009f790529822d1f4?x-bce-process=image/resize,m_lfit,w_440,limit_1",5.8,26.1,757.63,1.0,6.3,"主食");
        getMyDB().FoodDao().insert(food74);
        Food food75 = new Food("双菇炖鸡","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_c1eff31663febc405bbed642d4fa2939.jpg",1.21,2.67,108.83,1.33,1.42,"肉蛋奶");
        getMyDB().FoodDao().insert(food75);
        Food food76 = new Food("西芹炒鱼饼","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_big_photo_url_54d9ad7e16aa77dd516be5b6c1d35f78.jpg",4.37,4.08,313.94,1.66,5.0,"肉蛋奶");
        getMyDB().FoodDao().insert(food76);
        Food food77 = new Food("蒜蓉菜心","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_19a84e2e7918c70bf991e1f598ec78fb.jpg",2.79,4.97,226.03,1.62,2.93,"蔬果");
        getMyDB().FoodDao().insert(food77);
        Food food78 = new Food("油炸香菇","https://bkimg.cdn.bcebos.com/pic/aec379310a55b319114445224ca98226cffc172c?x-bce-process=image/resize,m_lfit,w_440,limit_1",10.6,70.8,1448.29,1.0,1.3,"蔬果");
        getMyDB().FoodDao().insert(food78);
        Food food79 = new Food("猪蹄瓜菇汤","http://s.boohee.cn/house/upload_food/2022/1/18/slim_user_mid_36179_1228915503.jpg",8.04,7.05,481.37,0.7,6.12,"汤");
        getMyDB().FoodDao().insert(food79);
        Food food80 = new Food("蛋黄菠菜土豆泥","http://s.boohee.cn/house/upload_food/2019/7/23/big_photo_url_de792e0af303c0a65599295111e6c526.jpg",5.09,6.06,489.74,0.31,8.11,"肉蛋奶");
        getMyDB().FoodDao().insert(food80);
        Food food81 = new Food("炒圆白菜","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_56efa8edec0015aa01de8a6929e5772a.jpg",1.28,10.19,301.38,0.77,3.13,"蔬果");
        getMyDB().FoodDao().insert(food81);
        Food food82 = new Food("胡萝卜西兰花","http://s.boohee.cn/house/upload_food/2022/1/19/slim_big_photo_url_big_photo_url_1f420f3e1fb3373501325d237c5b87a4.jpg",2.86,4.89,167.43,1.89,1.71,"蔬果");
        getMyDB().FoodDao().insert(food82);
        Food food83 = new Food("乌鸡炖牛奶","http://s.boohee.cn/house/upload_food/2022/1/19/slim_big_photo_url_b430357b06aee83af86b0f2173d660ee.jpg",17.79,2.73,422.77,0.17,2.16,"肉蛋奶");
        getMyDB().FoodDao().insert(food83);
        Food food84 = new Food("东坡肉","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_2076a574386ab6dcab9e391c37d48e20.jpg",7.48,9.49,2025.93,0.08,45.72,"肉蛋奶");
        getMyDB().FoodDao().insert(food84);
        Food food85 = new Food("葱爆羊肉","http://s.boohee.cn/house/upload_food/2021/3/10/big_photo_url_847bc1ec0eaf1ed13a508153a2723fb8.jpg",13.31,5.36,791.12,0.54,12.87,"肉蛋奶");
        getMyDB().FoodDao().insert(food85);
        Food food86 = new Food("红烧素鸡","http://s.boohee.cn/house/upload_food/2021/4/2/big_photo_url_481865b9ba07d7e9d8021f28704c3b52.jpg",13.35,6.92,786.93,1.7,12.18,"蔬果");
        getMyDB().FoodDao().insert(food86);
        Food food87 = new Food("迷你白切鸡","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_b4552d76d5af6637c37667d2df053f95.jpg",16.06,2.15,665.54,0.26,9.01,"肉蛋奶");
        getMyDB().FoodDao().insert(food87);
        Food food88 = new Food("凉粉","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_7d6541682e0267b6e9ea84c18352a3a7.jpg",0.2,8.9,159.06,0.6,0.3,"主食");
        getMyDB().FoodDao().insert(food88);
        Food food89 = new Food("豉油鸡","http://s.boohee.cn/house/upload_food/2022/1/18/slim_mid_photo_url_dfe0d161eade50b7316a4c54e98b4de2.jpg",21.3,0.0,866.46,0.0,13.6,"肉蛋奶");
        getMyDB().FoodDao().insert(food89);
        Food food90 = new Food("水煮活鱼","http://s.boohee.cn/house/upload_food/2022/1/18/slim_mid_photo_url_f7b4e05adf59ad012a2c5127b6c13650.jpg",4.5,13.2,899.95,0.0,15.0,"肉蛋奶");
        getMyDB().FoodDao().insert(food90);
        Food food91 = new Food("排骨年糕","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_76ca76c4e86b7e08d0ccdf921caea50e.jpg",9.09,27.47,925.06,0.21,8.39,"肉蛋奶");
        getMyDB().FoodDao().insert(food91);
        Food food92 = new Food("三鲜卷","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_6c65f985d6354a849d46a7ed48bb876b.jpg",13.8,3.85,665.54,0.2,9.91,"主食");
        getMyDB().FoodDao().insert(food92);
        Food food93 = new Food("黄瓜花生","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_a16aaab745e2e46017cacdb68c5a1eca.jpg",2.64,5.84,263.71,0.79,3.53,"蔬果");
        getMyDB().FoodDao().insert(food93);
        Food food94 = new Food("肠粉","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_028b58ec9cbbdc021c6c34676b661a50.jpg",1.3,21.0,460.44,1.0,2.2,"主食");
        getMyDB().FoodDao().insert(food94);
        Food food95 = new Food("五香熏鸡蛋","https://bkimg.cdn.bcebos.com/pic/d31b0ef41bd5ad6e76bd7ac981cb39dbb6fd3c48?x-bce-process=image/resize,m_lfit,w_536,limit_1",12.95,7.87,937.62,0.45,14.78,"肉蛋奶");
        getMyDB().FoodDao().insert(food95);
        Food food96 = new Food("担担面","http://s.boohee.cn/house/upload_food/2021/6/23/big_photo_url_9e3df96c0a4c227e5ac8d2bf7d0625c5.jpg",3.83,18.2,602.76,0.63,6.36,"主食");
        getMyDB().FoodDao().insert(food96);
        Food food97 = new Food("果汁鱼块","http://s.boohee.cn/house/upload_food/2022/1/18/slim_1170324667363.jpg",7.47,6.74,426.95,0.0,5.13,"肉蛋奶");
        getMyDB().FoodDao().insert(food97);
        Food food98 = new Food("烤鸡","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_5cd714e67e80aa3ff6e024e8e2e69b61.jpg",28.1,0.0,1109.24,0.0,16.9,"肉蛋奶");
        getMyDB().FoodDao().insert(food98);
        Food food99 = new Food("麻酱拌苦瓜","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_73923bb1da85363b965d4825020edb59.jpg",1.22,3.39,117.2,0.61,1.2,"蔬果");
        getMyDB().FoodDao().insert(food99);
        Food food100 = new Food("糖醋藕块","http://s.boohee.cn/house/upload_food/2022/3/1/big_photo_url_1cd3337372bdac26cd9c0a20e1cfb420.jpg",1.18,24.45,422.77,1.42,0.19,"蔬果");
        getMyDB().FoodDao().insert(food100);
        Food food101 = new Food("干锅猪肉花菜","http://s.boohee.cn/house/upload_food/2021/3/1/big_photo_url_4ccdfd9ebfcc476b4c676e7edf552270.jpg",4.9,5.35,460.44,2.06,8.23,"肉蛋奶");
        getMyDB().FoodDao().insert(food101);
        Food food102 = new Food("小米山药粥","http://s.boohee.cn/house/upload_food/2022/1/18/slim_mid_photo_url_big_photo_url_72b3c6ad6d1b2a4ec36f483d6e0af657.jpg",1.97,16.12,322.31,0.39,0.64,"主食");
        getMyDB().FoodDao().insert(food102);
        Food food103 = new Food("青椒胡萝卜山药泥","http://s.boohee.cn/house/upload_food/2022/1/19/slim_big_photo_url_30ddfb2698f5cc883cc83ba97f61b917.jpg",1.39,9.71,251.15,0.6,2.15,"蔬果");
        getMyDB().FoodDao().insert(food103);
        Food food104 = new Food("清炒虾米卷心菜","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_39411079c1e195115f768763d4e291b6.jpg",1.51,5.04,146.5,0.89,1.24,"肉蛋奶");
        getMyDB().FoodDao().insert(food104);
        Food food105 = new Food("蛋皮寿司","http://s.boohee.cn/house/upload_food/2022/1/18/slim_mid_photo_url_f48f954d2e4022ba5baf297a7e0d16dc.jpg",4.74,14.12,443.69,0.21,3.57,"肉蛋奶");
        getMyDB().FoodDao().insert(food105);
        Food food106 = new Food("油茶面","https://bkimg.cdn.bcebos.com/pic/c8ea15ce36d3d539b600425df3cdfe50352ac65c1a50?x-bce-process=image/resize,m_lfit,w_536,limit_1",6.0,75.0,1795.71,1.0,11.0,"主食");
        getMyDB().FoodDao().insert(food106);
        Food food107 = new Food("麻辣鸭胗","https://bkimg.cdn.bcebos.com/pic/0e2442a7d933c895d143de208f5864f082025aafaa1f?x-bce-process=image/resize,m_lfit,w_536,limit_1",13.8,13.5,548.34,0.0,2.3,"肉蛋奶");
        getMyDB().FoodDao().insert(food107);
        Food food108 = new Food("四川合川肉片","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_df4b46d01c97400a961f541db143626b.jpg",3.33,9.73,699.03,0.31,13.28,"肉蛋奶");
        getMyDB().FoodDao().insert(food108);
        Food food109 = new Food("粉丝娃娃菜","http://s.boohee.cn/house/upload_food/2021/3/10/big_photo_url_63be2bc3fa2cec4c3eecabf75d392fcc.jpg",1.69,29.16,506.48,3.08,0.34,"蔬果");
        getMyDB().FoodDao().insert(food109);
        Food food110 = new Food("凉拌腰花","https://bkimg.cdn.bcebos.com/pic/b64543a98226cffc9cf5f71eb8014a90f703eabb?x-bce-process=image/resize,m_lfit,w_220,limit_1",1.54,6.34,117.2,1.69,0.2,"肉蛋奶");
        getMyDB().FoodDao().insert(food110);
        Food food111 = new Food("叉烧肉","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_big_photo_url_ae58b35e03224d63f64644d7b948523a.jpg",20.9,6.0,820.42,0.0,9.8,"肉蛋奶");
        getMyDB().FoodDao().insert(food111);
        Food food112 = new Food("糖醋莲花白","http://s.boohee.cn/house/upload_food/2022/1/18/slim_533330_1274743458.jpg",3.71,2.36,154.87,0.74,1.61,"蔬果");
        getMyDB().FoodDao().insert(food112);
        Food food113 = new Food("酱牛肉","http://s.boohee.cn/house/upload_food/2022/1/18/slim_b_big_photo_url_e9a08aa5d171150c37a18641b4008444.jpg",31.4,3.2,1029.71,0.0,11.9,"肉蛋奶");
        getMyDB().FoodDao().insert(food113);
        Food food114 = new Food("牛肉面","http://s.boohee.cn/house/upload_food/2022/1/19/slim_big_photo_url_big_photo_url_30f2e241748b1bb398eaa68384a8970d.jpg",6.43,21.84,569.27,0.64,2.75,"主食");
        getMyDB().FoodDao().insert(food114);
        Food food115 = new Food("鸡蛋炒菠菜","http://s.boohee.cn/house/upload_food/2022/1/18/slim_mid_photo_url_big_photo_url_6f46d9317237ae9da2394c32f60ada06.jpg",3.3,12.48,330.68,0.42,1.91,"肉蛋奶");
        getMyDB().FoodDao().insert(food115);
        Food food116 = new Food("白灼虾","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_3c9c2c0b7bdaf775b89da6ce19eeac51.jpg",16.61,3.98,447.88,0.12,2.8,"肉蛋奶");
        getMyDB().FoodDao().insert(food116);
        Food food117 = new Food("荷包鲫鱼","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_0e3e97a5210345f5baaf911345e3a691.jpg",11.2,4.29,569.27,0.25,8.23,"肉蛋奶");
        getMyDB().FoodDao().insert(food117);
        Food food118 = new Food("褡裢火烧","http://s.boohee.cn/house/upload_food/2014/7/26/452803_1406369688.jpg",9.92,24.7,1197.14,0.96,16.15,"肉蛋奶");
        getMyDB().FoodDao().insert(food118);
        Food food119 = new Food("千层发糕","http://s.boohee.cn/house/upload_food/2021/4/2/big_photo_url_307c194df306fe44d76ea97b530fbdf4.jpg",3.98,10.54,1038.08,0.19,21.19,"肉蛋奶");
        getMyDB().FoodDao().insert(food119);
        Food food120 = new Food("蟹炒年糕","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_45357a7219c5bc835b7f09ba8a931651.jpg",9.93,15.23,724.14,0.24,8.1,"肉蛋奶");
        getMyDB().FoodDao().insert(food120);
        Food food121 = new Food("莴笋炒香菇","http://s.boohee.cn/house/upload_food/2021/5/21/big_photo_url_2c3a200611e7f4ab856719bc983ec82d.jpg",1.52,5.18,205.1,1.66,2.83,"蔬果");
        getMyDB().FoodDao().insert(food121);
        Food food122 = new Food("豆豉番瓜炒咸蛋黄","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_99353da61bb23eabcd6a24e6f151272c.jpg",3.78,5.15,598.57,0.52,12.02,"肉蛋奶");
        getMyDB().FoodDao().insert(food122);
        Food food123 = new Food("四川棒棒鸡","http://s.boohee.cn/house/upload_food/2022/1/18/slim_1162050543390_mid.jpg",3.44,19.74,682.29,0.38,8.09,"肉蛋奶");
        getMyDB().FoodDao().insert(food123);
        Food food124 = new Food("粉丝虾米杂菜煲","http://s.boohee.cn/house/upload_food/2022/1/18/slim_mid_photo_url_2ce30fc7e8f40f234d53fd7e7611a90e.jpg",2.74,58.9,1289.23,1.0,6.85,"肉蛋奶");
        getMyDB().FoodDao().insert(food124);
        Food food125 = new Food("黄瓜粉皮鸡丝","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_8416c20da1534d79979cf2f3561faff6.jpg",9.49,6.83,573.45,0.3,8.04,"肉蛋奶");
        getMyDB().FoodDao().insert(food125);
        Food food126 = new Food("盐水鸭","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_8a449ae72f1f41443dde216013916bf9.jpg",9.14,0.8,1343.64,0.26,30.94,"肉蛋奶");
        getMyDB().FoodDao().insert(food126);
        Food food127 = new Food("芥兰腊肉","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_55288fb06a124080b09d224a61e29b93.jpg",5.72,5.91,1008.78,0.98,21.44,"肉蛋奶");
        getMyDB().FoodDao().insert(food127);
        Food food128 = new Food("西芹炒百合","http://s.boohee.cn/house/upload_food/2014/3/21/4554174_1395362700.jpg",1.31,10.56,255.33,1.98,2.44,"蔬果");
        getMyDB().FoodDao().insert(food128);
        Food food129 = new Food("独咸茄","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_7d2d526a1f694b0c8b560dfc9081487b.jpg",6.29,9.51,477.18,3.53,6.43,"蔬果");
        getMyDB().FoodDao().insert(food129);
        Food food130 = new Food("玉米糊","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_5a2f36606eae3589af28260a2b630d6d.jpg",8.15,33.65,749.26,0.96,2.05,"主食");
        getMyDB().FoodDao().insert(food130);
        Food food131 = new Food("香菇菜心","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_big_photo_url_6baa301ee169949129cc94e861d33074.jpg",1.72,4.58,200.92,2.6,3.05,"蔬果");
        getMyDB().FoodDao().insert(food131);
        Food food132 = new Food("海鲜菌菇汤","https://bkimg.cdn.bcebos.com/pic/83025aafa40f4bfb017c415f0d4f78f0f7361809?x-bce-process=image/resize,m_lfit,w_536,limit_1",2.71,2.02,129.76,0.26,1.48,"汤");
        getMyDB().FoodDao().insert(food132);
        Food food133 = new Food("厚烧鸡蛋","http://s.boohee.cn/house/upload_food/2020/6/24/mid_photo_url_big_photo_url_62a05d17ee912e5c13c39ad8f1c666a4.jpg",9.0,37.4,1205.51,0.0,11.3,"肉蛋奶");
        getMyDB().FoodDao().insert(food133);
        Food food134 = new Food("土豆牛肉饼","https://bkimg.cdn.bcebos.com/pic/8c1001e93901213fbd1303a754e736d12f2e95ac?x-bce-process=image/resize,m_lfit,w_536,limit_1",5.9,22.9,740.89,0.0,6.7,"肉蛋奶");
        getMyDB().FoodDao().insert(food134);
        Food food135 = new Food("罗宋汤","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_3fb5d960dc12e86fa4bb24a4195ff1f9.jpg",2.15,8.59,246.96,1.55,2.19,"汤");
        getMyDB().FoodDao().insert(food135);
        Food food136 = new Food("炝辣白菜","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_b52b696242281ddbb8a9b62662431d3b.jpg",1.74,5.64,293.01,1.37,4.78,"蔬果");
        getMyDB().FoodDao().insert(food136);
        Food food137 = new Food("麻辣白菜","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_big_photo_url_0e2f0fc879a6c52ae783dbb4b2b360a8.jpg",2.23,7.56,293.01,2.45,4.02,"蔬果");
        getMyDB().FoodDao().insert(food137);
        Food food138 = new Food("龙趸焖节瓜","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_f9682f0121524233862f03a69d237715.jpg",9.58,4.35,322.31,1.04,2.65,"蔬果");
        getMyDB().FoodDao().insert(food138);
        Food food139 = new Food("香菇菜包","http://s.boohee.cn/house/upload_food/2020/8/21/m_big_photo_url_369ab051cc4c8d0ed5f8dfc2ec53deb7.jpg",6.0,37.7,1000.41,1.0,7.0,"蔬果");
        getMyDB().FoodDao().insert(food139);
        Food food140 = new Food("红萝卜煮蘑菇","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_7e2f1f7326c35cd333f91cf8fe89a8d4.jpg",3.19,5.41,209.29,1.61,2.13,"蔬果");
        getMyDB().FoodDao().insert(food140);
        Food food141 = new Food("桂花果香牛肉","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_305f4da2bd5447dea52c73f474859b9d.jpg",8.5,3.63,439.51,0.22,6.35,"肉蛋奶");
        getMyDB().FoodDao().insert(food141);
        Food food142 = new Food("原味土豆泥","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_7fe333684f15c830932ec7a5efc279f6.jpg",12.1,72.7,1561.3,1.0,3.7,"蔬果");
        getMyDB().FoodDao().insert(food142);
        Food food143 = new Food("老友面","https://bkimg.cdn.bcebos.com/pic/500fd9f9d72a6059252da3e2e87e239b033b5ab50cba?x-bce-process=image/resize,m_lfit,w_536,limit_1",11.0,70.0,1448.29,0.0,1.5,"主食");
        getMyDB().FoodDao().insert(food143);
        Food food144 = new Food("红豆钵仔糕","https://bkimg.cdn.bcebos.com/pic/42a98226cffc1e17c30d136b4490f603738de93e?x-bce-process=image/resize,m_lfit,w_536,limit_1",2.1,29.0,544.15,2.0,1.1,"主食");
        getMyDB().FoodDao().insert(food144);
        Food food145 = new Food("宋嫂鱼羹","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_22abe68f2aad930df4d2f6c0afe88655.jpg",6.61,1.29,242.78,0.1,2.87,"汤");
        getMyDB().FoodDao().insert(food145);
        Food food146 = new Food("溜肉段","http://s.boohee.cn/house/upload_food/2020/6/28/big_photo_url_big_photo_url_9b95216f476137c6d71a0c925e48714b.jpg",16.45,1.57,539.97,0.0,6.43,"肉蛋奶");
        getMyDB().FoodDao().insert(food146);
        Food food147 = new Food("豆腐鲫鱼汤","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_bda0186a9617cc9445dec32a012e3e5b.jpg",4.45,2.42,192.55,0.18,2.2,"汤");
        getMyDB().FoodDao().insert(food147);
        Food food148 = new Food("酒蒸鸡","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_ee9b9eb924e844dcbe88f998409fe4da.jpg",14.97,2.9,594.38,0.47,7.93,"肉蛋奶");
        getMyDB().FoodDao().insert(food148);
        Food food149 = new Food("虾仁炒牛奶","http://s.boohee.cn/house/upload_food/2022/1/19/slim_452803_1352719456.jpg",6.59,5.38,519.04,0.0,8.46,"肉蛋奶");
        getMyDB().FoodDao().insert(food149);
        Food food150 = new Food("宫保鸡丁","http://s.boohee.cn/house/upload_food/2022/5/31/slim_big_photo_url_e773f9c436f0572c6511b1a1db4e2795.jpg",5.65,13.15,443.69,0.43,3.66,"肉蛋奶");
        getMyDB().FoodDao().insert(food150);
        Food food151 = new Food("清炒菠菜","http://s.boohee.cn/house/upload_food/2020/6/18/big_photo_url_big_photo_url_2dfc3d0a2e815d34103e0fe6a02a6533.jpg",2.55,4.41,175.8,1.67,1.93,"蔬果");
        getMyDB().FoodDao().insert(food151);
        Food food152 = new Food("酒酿蛋汤","http://s.boohee.cn/house/upload_food/2019/8/26/big_photo_url_181e2ae5dd9d9da06443f450a69ce647.jpg",2.23,6.29,154.87,0.64,0.41,"汤");
        getMyDB().FoodDao().insert(food152);
        Food food153 = new Food("酸萝卜炖老鸭","http://s.boohee.cn/house/upload_food/2022/1/18/slim_mid_photo_url_8e6cea1ca2b3b8dd4b5d10426f28dfe1.jpg",1.8,6.2,192.55,1.0,1.4,"肉蛋奶");
        getMyDB().FoodDao().insert(food153);
        Food food154 = new Food("热干面","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_3a5088e7176ee72d2f8ac3a4b3a788c0.jpg",4.2,28.7,640.43,0.2,2.4,"主食");
        getMyDB().FoodDao().insert(food154);
        Food food155 = new Food("芹菜腐竹","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_088987343d8246b766a917e03e1765f6.jpg",4.88,2.54,280.45,0.68,4.39,"蔬果");
        getMyDB().FoodDao().insert(food155);
        Food food156 = new Food("西湖醋鱼","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_big_photo_url_72fb4241fc5e06ccea3f51597f976cf3.jpg",13.99,6.41,510.67,0.01,4.51,"肉蛋奶");
        getMyDB().FoodDao().insert(food156);
        Food food157 = new Food("烤整鸡","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_5cd714e67e80aa3ff6e024e8e2e69b61.jpg",11.25,1.03,410.21,0.08,5.47,"肉蛋奶");
        getMyDB().FoodDao().insert(food157);
        Food food158 = new Food("武汉的面窝","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_eb55fe04dfa9a8e037aefab3aaed836d.jpg",5.2,44.0,1218.07,1.1,10.7,"主食");
        getMyDB().FoodDao().insert(food158);
        Food food159 = new Food("黄豆骨头汤","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_big_photo_url_57905014f6369920684256c17f336be8.jpg",12.77,0.28,560.9,0.08,9.09,"汤");
        getMyDB().FoodDao().insert(food159);
        Food food160 = new Food("过桥米线","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_big_photo_url_0b1ae5782e1c4f9389769971cb8ffa73.jpg",4.0,11.8,385.09,1.0,3.8,"主食");
        getMyDB().FoodDao().insert(food160);
        Food food161 = new Food("相思红豆粥","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_big_photo_url_big_photo_url_4c6dee87a9806bf16785e5fb3cace80b.jpg",3.19,14.18,288.82,1.03,0.15,"主食");
        getMyDB().FoodDao().insert(food161);
        Food food162 = new Food("醋溜藕片","http://s.boohee.cn/house/upload_food/2022/1/18/slim_mid_photo_url_fc40e3018b1b0e0636d34de1d6981b8b.jpg",0.9,19.1,339.05,0.0,0.0,"蔬果");
        getMyDB().FoodDao().insert(food162);
        Food food163 = new Food("粘糕","https://bkimg.cdn.bcebos.com/pic/8718367adab44aed2e73cc4b1c579001a18b87d64d42?x-bce-process=image/resize,m_lfit,w_536,limit_1",3.98,84.24,1532.0,0.61,0.69,"主食");
        getMyDB().FoodDao().insert(food163);
        Food food164 = new Food("葱炖猪蹄","http://s.boohee.cn/house/upload_food/2022/1/18/slim_mid_photo_url_mid_photo_url_big_photo_url_abc36b4aa2ef689bdc650ce45acbd3ca.jpg",6.65,4.24,439.51,1.6,7.19,"肉蛋奶");
        getMyDB().FoodDao().insert(food164);
        Food food165 = new Food("虾仁蒸蛋","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_f956b2a8273f5322e9c96205a736598c.jpg",11.9,9.21,640.43,0.0,7.64,"肉蛋奶");
        getMyDB().FoodDao().insert(food165);
        Food food166 = new Food("冬瓜鸭架汤","https://bkimg.cdn.bcebos.com/pic/faedab64034f78f0afe63d6675310a55b2191cb1?x-bce-process=image/resize,m_lfit,w_536,limit_1",6.58,3.27,284.63,1.14,3.47,"汤");
        getMyDB().FoodDao().insert(food166);
        Food food167 = new Food("菠萝虾球","http://s.boohee.cn/house/upload_food/2022/1/18/slim_mid_photo_url_3a2712c13952445686925bc3706dae8c.jpg",6.9,9.0,334.86,0.0,2.0,"肉蛋奶");
        getMyDB().FoodDao().insert(food167);
        Food food168 = new Food("红烧猪蹄","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_big_photo_url_abc36b4aa2ef689bdc650ce45acbd3ca.jpg",17.04,2.98,916.69,0.2,15.47,"肉蛋奶");
        getMyDB().FoodDao().insert(food168);
        Food food169 = new Food("核桃仁鸡丁","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_4597e6f09ca140159f119bcdbf1972d4.jpg",14.48,6.97,1414.8,2.1,28.34,"肉蛋奶");
        getMyDB().FoodDao().insert(food169);
        Food food170 = new Food("日式御饭团","https://bkimg.cdn.bcebos.com/pic/a8014c086e061d950a7b3242acbd1dd162d9f2d35c3d?x-bce-process=image/resize,m_lfit,w_536,limit_1",3.7,28.8,594.38,1.0,1.1,"主食");
        getMyDB().FoodDao().insert(food170);
        Food food171 = new Food("萝卜糕","http://s.boohee.cn/house/upload_food/2022/1/18/slim_user_90529_1217658109.jpg",3.59,6.86,242.78,0.62,1.94,"主食");
        getMyDB().FoodDao().insert(food171);
        Food food172 = new Food("米饭","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_big_photo_url_f81e8de29f76e6d0c39d4142fe45764f.jpg",2.6,25.9,485.55,0.3,0.3,"主食");
        getMyDB().FoodDao().insert(food172);
        Food food173 = new Food("清蒸多宝鱼","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_big_photo_url_1173c9635e7cc6ab9c47fb5dc1da2fb0.jpg",15.55,2.13,334.86,0.13,1.02,"肉蛋奶");
        getMyDB().FoodDao().insert(food173);
        Food food174 = new Food("火腿白菜","http://s.boohee.cn/house/upload_food/2022/1/18/slim_mid_photo_url_8ef2d6d5026d28c12641d19a7e2eea44.jpg",3.86,3.63,364.16,0.76,6.55,"肉蛋奶");
        getMyDB().FoodDao().insert(food174);
        Food food175 = new Food("家常连锅","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_02c7c858f580464bb6fc09646bcea65e.jpg",3.69,1.43,209.29,0.28,3.37,"肉蛋奶");
        getMyDB().FoodDao().insert(food175);
        Food food176 = new Food("酒酿蛋","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_5abc45c37cff16bea870611952e5d87b.jpg",2.59,8.37,217.66,0.1,0.94,"肉蛋奶");
        getMyDB().FoodDao().insert(food176);
        Food food177 = new Food("白菜肉卷","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_12489847a27c37e921ff071abda0f293.jpg",7.23,3.95,845.53,0.39,17.53,"肉蛋奶");
        getMyDB().FoodDao().insert(food177);
        Food food178 = new Food("粉蒸排骨","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_082af3f9ec5d882f37d9fc5c60e84351.jpg",10.83,12.33,1138.54,0.38,19.98,"肉蛋奶");
        getMyDB().FoodDao().insert(food178);
        Food food179 = new Food("咸鱼茄子煲","http://s.boohee.cn/house/upload_food/2021/2/23/mid_photo_url_4969a72b87865ef2cf35b72cc10945a8.jpg",7.99,6.74,489.74,0.87,6.58,"肉蛋奶");
        getMyDB().FoodDao().insert(food179);
        Food food180 = new Food("鲜香肉皮冻","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_2f7c64b854acdca85ec7c4793ad2c0e9.jpg",24.79,5.84,1360.39,1.71,22.93,"肉蛋奶");
        getMyDB().FoodDao().insert(food180);
        Food food181 = new Food("日式肉松饭团","https://bkimg.cdn.bcebos.com/pic/3812b31bb051f8198618f76aebe05ded2e738bd449cc?x-bce-process=image/resize,m_lfit,w_536,limit_1",5.58,22.57,544.15,0.43,1.78,"主食");
        getMyDB().FoodDao().insert(food181);
        Food food182 = new Food("甜味熏鱼","http://s.boohee.cn/house/upload_food/2022/1/18/slim_mid_photo_url_74353aac21e5e88908ad8b74c201fe0d.jpg",4.8,67.8,2025.93,1.0,21.1,"肉蛋奶");
        getMyDB().FoodDao().insert(food182);
        Food food183 = new Food("豆芽粉丝","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_a00de3a33e9ef56e8ffd405eac3f5732.jpg",2.17,18.47,435.32,1.31,2.66,"蔬果");
        getMyDB().FoodDao().insert(food183);
        Food food184 = new Food("酱焖香菇牛腩","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_5502854e70b2ef69d69053301c03bee3.jpg",3.12,3.07,171.62,1.36,2.1,"肉蛋奶");
        getMyDB().FoodDao().insert(food184);
        Food food185 = new Food("素三鲜","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_c66e45c2804f83ee7a147772ccb3530a.jpg",1.37,7.63,246.96,0.98,2.9,"蔬果");
        getMyDB().FoodDao().insert(food185);
        Food food186 = new Food("醋溜白菜","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_b12c03f68166e9b0e163489a3ab2ed16.jpg",1.64,3.89,192.55,0.89,2.88,"蔬果");
        getMyDB().FoodDao().insert(food186);
        Food food187 = new Food("泥鳅炖豆腐","http://s.boohee.cn/house/upload_food/2022/1/18/slim_394045_1273721162.jpg",4.25,1.49,238.59,0.55,3.78,"肉蛋奶");
        getMyDB().FoodDao().insert(food187);
        Food food188 = new Food("香菇滑鸡","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_bda60ac26b7a76ed1bb07771102081ef.jpg",10.04,5.13,372.54,1.68,3.53,"肉蛋奶");
        getMyDB().FoodDao().insert(food188);
        Food food189 = new Food("红焖羊肉","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_80039f2856657db5fb0de45e2dcf86cb.jpg",11.78,4.75,1021.34,1.11,19.99,"肉蛋奶");
        getMyDB().FoodDao().insert(food189);
        Food food190 = new Food("油焖白菜","http://s.boohee.cn/house/upload_food/2022/1/19/slim_mid_photo_url_74371381215058820b9c3dbc49485b5c.jpg",13.11,3.74,406.02,0.08,3.31,"蔬果");
        getMyDB().FoodDao().insert(food190);
        Food food191 = new Food("日式肥牛饭","https://bkimg.cdn.bcebos.com/pic/dc54564e9258d109b3de52366f12dbbf6c81800ae756?x-bce-process=image/resize,m_lfit,w_536,limit_1",4.6,28.1,736.7,1.0,4.8,"主食");
        getMyDB().FoodDao().insert(food191);
        Food food192 = new Food("苦瓜镶肉","https://bkimg.cdn.bcebos.com/pic/8c1001e93901213fb80ec3e3f8af21d12f2eb9383ff1?x-bce-process=image/resize,m_lfit,w_536,limit_1",12.9,7.79,1159.47,1.09,21.96,"肉蛋奶");
        getMyDB().FoodDao().insert(food192);
        Food food193 = new Food("酸辣鱼片","http://s.boohee.cn/house/upload_food/2022/1/18/slim_1176876595015.jpg",10.1,7.75,1335.27,0.67,27.64,"肉蛋奶");
        getMyDB().FoodDao().insert(food193);
        Food food194 = new Food("绿豆番瓜羹","https://bkimg.cdn.bcebos.com/pic/f3d3572c11dfa9ec8a13f4531b9ae003918fa0ec6b16?x-bce-process=image/resize,m_lfit,w_536,limit_1",3.4,106.2,1783.15,0.0,0.2,"汤");
        getMyDB().FoodDao().insert(food194);
        Food food195 = new Food("茄汁菜包","http://s.boohee.cn/house/upload_food/2022/1/18/slim_user_76228_1213922015.jpg",4.28,4.28,255.33,0.62,3.09,"蔬果");
        getMyDB().FoodDao().insert(food195);
        Food food196 = new Food("营养鲫鱼汤","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_cc829cc59d11514ed7eabe40333a17b2.jpg",5.62,1.51,188.36,0.02,1.86,"汤");
        getMyDB().FoodDao().insert(food196);
        Food food197 = new Food("麻辣牛肉丝","https://bkimg.cdn.bcebos.com/pic/5366d0160924ab1869e307ea3bfae6cd7a890b72?x-bce-process=image/resize,m_lfit,w_536,limit_1",56.6,9.3,1452.47,0.0,9.0,"肉蛋奶");
        getMyDB().FoodDao().insert(food197);
        Food food198 = new Food("青椒土豆丝","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_cdc8d306b9afa488fe7c9c93a6477749.jpg",1.96,13.38,313.94,1.48,1.87,"蔬果");
        getMyDB().FoodDao().insert(food198);
        Food food199 = new Food("香干炒芹菜","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_big_photo_url_d3f08d864a230720ad21ba2aa1d34d3b.jpg",2.83,4.19,447.88,1.05,9.01,"蔬果");
        getMyDB().FoodDao().insert(food199);
        Food food200 = new Food("玉米胡萝卜猪骨汤","http://s.boohee.cn/house/upload_food/2021/6/30/big_photo_url_d101528d5191d667d85c6c52e3933b14.jpg",3.46,6.2,272.08,0.77,3.1,"汤");
        getMyDB().FoodDao().insert(food200);
        Food food201 = new Food("翡翠双冬蹄膀","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_e7ff4e6b84f146ac8901f3881da62fe0.jpg",1.06,3.54,129.76,0.57,1.6,"肉蛋奶");
        getMyDB().FoodDao().insert(food201);
        Food food202 = new Food("山椒醉鸡翅","https://bkimg.cdn.bcebos.com/pic/f636afc379310a55b3199004d10e54a98226cefcbaa2?x-bce-process=image/resize,m_lfit,w_536,limit_1",15.2,0.6,519.04,1.0,6.3,"肉蛋奶");
        getMyDB().FoodDao().insert(food202);
        Food food203 = new Food("鸭汤","http://s.boohee.cn/house/upload_food/2022/3/1/big_photo_url_564cc75a372e2fe695f5d6785b8037fc.jpg",3.95,1.52,301.38,0.4,5.73,"汤");
        getMyDB().FoodDao().insert(food203);
        Food food204 = new Food("湘烧豆腐","https://bkimg.cdn.bcebos.com/pic/37d3d539b6003af36a91eb043f2ac65c1038b6ae?x-bce-process=image/resize,m_lfit,w_536,limit_1",31.0,5.5,1389.69,1.0,20.5,"蔬果");
        getMyDB().FoodDao().insert(food204);
        Food food205 = new Food("中式烤火鸡","http://s.boohee.cn/house/upload_food/2021/2/23/big_photo_url_e7417345cea75e7476052968be66305f.jpg",16.57,2.55,435.32,0.18,2.78,"肉蛋奶");
        getMyDB().FoodDao().insert(food205);
        Food food206 = new Food("萝卜丝饼","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_ede75e34b827f5d8540d93f0af67d027.jpg",5.38,12.24,632.06,0.85,9.12,"蔬果");
        getMyDB().FoodDao().insert(food206);
        Food food207 = new Food("辣味肉皮冻","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_2f7c64b854acdca85ec7c4793ad2c0e9.jpg",24.79,5.84,1360.39,1.71,22.93,"肉蛋奶");
        getMyDB().FoodDao().insert(food207);
        Food food208 = new Food("米纸饼","https://bkimg.cdn.bcebos.com/pic/64380cd7912397dda1440303fdd2a5b7d0a20cf42921?x-bce-process=image/resize,m_lfit,w_536,limit_1",3.8,57.3,1699.43,0.0,17.9,"主食");
        getMyDB().FoodDao().insert(food208);
        Food food209 = new Food("韭菜猪血汤","http://s.boohee.cn/house/upload_food/2020/6/30/big_photo_url_big_photo_url_4c095a9b52d7d146aa9600057ac0a90b.jpg",6.79,2.9,246.96,0.01,2.61,"肉蛋奶");
        getMyDB().FoodDao().insert(food209);
        Food food210 = new Food("土豆泥","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_7fe333684f15c830932ec7a5efc279f6.jpg",2.51,16.36,519.04,1.0,5.64,"蔬果");
        getMyDB().FoodDao().insert(food210);
        Food food211 = new Food("香椿拌豆腐","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_dbcf40d2ac9ddbc84352bf7225330364.jpg",4.7,5.94,364.16,0.63,5.38,"蔬果");
        getMyDB().FoodDao().insert(food211);
        Food food212 = new Food("蒸蛋","http://s.boohee.cn/house/upload_food/2020/5/14/big_photo_url_aba1fd880078429e0dda4a752fc802c9.jpg",15.34,2.2,615.31,0.01,8.56,"肉蛋奶");
        getMyDB().FoodDao().insert(food212);
        Food food213 = new Food("煎灌肠","https://bkimg.cdn.bcebos.com/pic/faf2b2119313b07eca80e20bca9d862397dda14426d7?x-bce-process=image/resize,m_lfit,w_536,limit_1",0.39,31.48,644.61,0.32,2.99,"肉蛋奶");
        getMyDB().FoodDao().insert(food213);
        Food food214 = new Food("芹菜肉松","https://bkimg.cdn.bcebos.com/pic/71cf3bc79f3df8dc184b4dfccf11728b4710286d?x-bce-process=image/resize,m_lfit,w_536,limit_1",7.0,22.1,1059.01,3.6,16.8,"肉蛋奶");
        getMyDB().FoodDao().insert(food214);
        Food food215 = new Food("红烧羊肉","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_ed43c7d406e307d3c3d58d9bd47e5bc7.jpg",15.3,1.55,527.41,0.19,6.6,"肉蛋奶");
        getMyDB().FoodDao().insert(food215);
        Food food216 = new Food("黄瓜银耳汤","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_d427bb4ff5c08a1d82105a80758488e3.jpg",1.73,9.03,263.71,3.19,2.97,"汤");
        getMyDB().FoodDao().insert(food216);
        Food food217 = new Food("炒羊肉丝","http://s.boohee.cn/house/upload_food/2021/3/10/big_photo_url_847bc1ec0eaf1ed13a508153a2723fb8.jpg",14.82,4.56,598.57,0.27,7.32,"肉蛋奶");
        getMyDB().FoodDao().insert(food217);
        Food food218 = new Food("红薯饭","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo201512115111217767.jpg",3.07,30.8,573.45,0.74,0.34,"主食");
        getMyDB().FoodDao().insert(food218);
        Food food219 = new Food("卤猪蹄","http://s.boohee.cn/house/upload_food/2020/6/20/big_photo_url_big_photo_url_abc36b4aa2ef689bdc650ce45acbd3ca.jpg",28.4,0.0,992.03,0.0,15.4,"肉蛋奶");
        getMyDB().FoodDao().insert(food219);
        Food food220 = new Food("玉米发糕","http://s.boohee.cn/house/upload_food/2021/6/4/big_photo_url_big_photo_url_ad6ad738de0a5d37cf98708656fcf650.jpg",7.22,30.95,711.59,1.28,2.2,"主食");
        getMyDB().FoodDao().insert(food220);
        Food food221 = new Food("蚝油牛肉","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_big_photo_url_41a07bd6175480abfb35c14a5ef6cc5c.jpg",14.83,3.65,682.29,0.05,9.91,"肉蛋奶");
        getMyDB().FoodDao().insert(food221);
        Food food222 = new Food("花生莲藕汤","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_441cf5d9a7a64909c67e0a01aed59781.jpg",6.08,7.41,539.97,1.22,8.42,"汤");
        getMyDB().FoodDao().insert(food222);
        Food food223 = new Food("牛肉炖土豆","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_ba5b96b1f2ec90ee1084aab2a00c9a74.jpg",9.2,53.7,1929.65,1.0,23.5,"肉蛋奶");
        getMyDB().FoodDao().insert(food223);
        Food food224 = new Food("干葱豆豉鸡","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_big_photo_url_c0480103bc0cf7f52b2a1490e4675c2b.jpg",17.43,4.52,807.86,0.23,11.77,"肉蛋奶");
        getMyDB().FoodDao().insert(food224);
        Food food225 = new Food("鸡肝","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_big_photo_url_25fd2bbd1c1f89f5b8d2873c40aeca27.jpg",16.6,2.8,506.48,0.0,4.8,"肉蛋奶");
        getMyDB().FoodDao().insert(food225);
        Food food226 = new Food("凉拌馄饨皮","http://s.boohee.cn/house/upload_food/2020/12/3/big_photo_url_45270132165c34e2b5bcf6f456c00172.jpg",8.3,58.2,1134.35,1.5,0.5,"主食");
        getMyDB().FoodDao().insert(food226);
        Food food227 = new Food("纸包鸡","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_9b86c1282f024a38a0cef9e61f1f31f7.jpg",19.58,1.4,870.65,0.06,13.48,"肉蛋奶");
        getMyDB().FoodDao().insert(food227);
        Food food228 = new Food("栗子白菜","http://s.boohee.cn/house/upload_food/2022/1/18/slim_173755_1230198434.jpg",1.11,3.87,96.27,0.96,0.45,"蔬果");
        getMyDB().FoodDao().insert(food228);
        Food food229 = new Food("泡椒凤爪","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_50267b7ffa08eb4d8478722adc34d177.jpg",19.62,4.44,899.95,0.43,13.22,"肉蛋奶");
        getMyDB().FoodDao().insert(food229);
        Food food230 = new Food("茄子煲","http://s.boohee.cn/house/upload_food/2022/1/19/slim_big_photo_url_big_photo_url_c42d57ff1614c58ded6e8c14cd8760a8.jpg",3.65,6.37,581.83,0.72,11.26,"汤");
        getMyDB().FoodDao().insert(food230);
        Food food231 = new Food("绿豆糕","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_95306ef5239d6cf1fbb3ba9fc06e39d8.jpg",12.8,73.4,1469.22,1.2,1.0,"主食");
        getMyDB().FoodDao().insert(food231);
        Food food232 = new Food("木耳洋葱炒肉丝","http://s.boohee.cn/house/upload_food/2022/1/19/slim_big_photo_url_big_photo_url_cf9fafcd5edfe746be5a3778c48e6bc2.jpg",1.37,6.2,318.12,1.87,5.43,"肉蛋奶");
        getMyDB().FoodDao().insert(food232);
        Food food233 = new Food("咸蛋肉沫芥菜汤","https://bkimg.cdn.bcebos.com/pic/060828381f30e924b899289ecd4379061d950a7b5942?x-bce-process=image/resize,m_lfit,w_536,limit_1",3.39,2.91,205.1,0.44,2.72,"汤");
        getMyDB().FoodDao().insert(food233);
        Food food234 = new Food("炒藕丁","http://s.boohee.cn/house/upload_food/2022/1/19/slim_big_photo_url_big_photo_url_9c7bbbce7d9d995334d804844a805e20.jpg",1.95,12.57,368.35,2.99,4.11,"蔬果");
        getMyDB().FoodDao().insert(food234);
        Food food235 = new Food("蚝油豆腐","http://s.boohee.cn/house/upload_food/2020/6/18/big_photo_url_big_photo_url_f384a64185de236f185747eff1fc858b.jpg",5.93,3.78,351.61,0.19,5.13,"蔬果");
        getMyDB().FoodDao().insert(food235);
        Food food236 = new Food("山椒蒸鲩鱼","http://s.boohee.cn/house/upload_food/2019/8/29/big_photo_url_5263963d4fff656252ea2137abe566d4.jpg",15.0,2.67,636.24,0.01,9.07,"肉蛋奶");
        getMyDB().FoodDao().insert(food236);
        Food food237 = new Food("醉猪手","http://s.boohee.cn/house/upload_food/2022/1/18/slim_mid_photo_url_mid_photo_url_big_photo_url_abc36b4aa2ef689bdc650ce45acbd3ca.jpg",17.4,8.0,519.04,1.0,2.3,"肉蛋奶");
        getMyDB().FoodDao().insert(food237);
        Food food238 = new Food("香菇瘦肉","https://bkimg.cdn.bcebos.com/pic/e850352ac65c10385343b0eb4f598413b07eca8023cb?x-bce-process=image/resize,m_lfit,w_536,limit_1",5.99,20.32,481.37,1.05,1.22,"肉蛋奶");
        getMyDB().FoodDao().insert(food238);
        Food food239 = new Food("豆干炒白菜","https://bkimg.cdn.bcebos.com/pic/42166d224f4a20a44623f37eb7188f22720e0cf37a7d?x-bce-process=image/resize,m_lfit,w_536,limit_1",6.65,5.77,347.42,0.63,4.16,"蔬果");
        getMyDB().FoodDao().insert(food239);
        Food food240 = new Food("醉妃白肉","https://bkimg.cdn.bcebos.com/pic/5882b2b7d0a20cf4172c8f1c71094b36adaf99b0?x-bce-process=image/resize,m_lfit,w_536,limit_1",1.5,84.0,1460.84,1.0,0.0,"肉蛋奶");
        getMyDB().FoodDao().insert(food240);
        Food food241 = new Food("八宝青蟹火腿饭","https://bkimg.cdn.bcebos.com/pic/b151f8198618367a1bbd0cc728738bd4b31ce5f0?x-bce-process=image/resize,m_lfit,w_536,limit_1",14.6,1.7,334.86,0.0,1.6,"主食");
        getMyDB().FoodDao().insert(food241);
        Food food242 = new Food("芋头饭","https://bkimg.cdn.bcebos.com/pic/d4628535e5dde71101c99c84a9efce1b9d1661ad?x-bce-process=image/resize,m_lfit,w_536,limit_1",6.07,16.88,611.13,0.59,6.0,"主食");
        getMyDB().FoodDao().insert(food242);
        Food food243 = new Food("干炒牛河","http://s.boohee.cn/house/upload_food/2020/7/24/b_big_photo_url_852c9fb76aac83c25fa6159e8ccfed64.jpg",9.23,56.75,1197.14,0.11,2.65,"肉蛋奶");
        getMyDB().FoodDao().insert(food243);
        Food food244 = new Food("猪肉韭菜馄饨","https://bkimg.cdn.bcebos.com/pic/9a504fc2d56285351d9866e29eef76c6a6ef63fd?x-bce-process=image/resize,m_lfit,w_536,limit_1",7.5,25.7,987.85,0.0,11.4,"主食");
        getMyDB().FoodDao().insert(food244);
        Food food245 = new Food("水鸭润肺汤","https://bkimg.cdn.bcebos.com/pic/5fdf8db1cb13495409236f8383048558d109b2dedc8b?x-bce-process=image/resize,m_lfit,w_536,limit_1",2.6,1.7,96.27,0.0,0.6,"汤");
        getMyDB().FoodDao().insert(food245);
        Food food246 = new Food("葱爆肉","http://s.boohee.cn/house/upload_food/2021/3/10/big_photo_url_847bc1ec0eaf1ed13a508153a2723fb8.jpg",13.31,5.36,791.12,0.54,12.87,"肉蛋奶");
        getMyDB().FoodDao().insert(food246);
        Food food247 = new Food("小玉米炒木耳","https://bkimg.cdn.bcebos.com/pic/4afbfbedab64034f01828f93abc379310b551dd3?x-bce-process=image/resize,m_lfit,w_536,limit_1",12.3,3.29,464.62,1.03,5.58,"蔬果");
        getMyDB().FoodDao().insert(food247);
        Food food248 = new Food("醉排骨","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_93b099e7c6544ca28f063785eb6d75e4.jpg",13.4,12.9,1318.53,0.3,23.31,"肉蛋奶");
        getMyDB().FoodDao().insert(food248);
        Food food249 = new Food("辣椒豆酱蒸","https://bkimg.cdn.bcebos.com/pic/a08b87d6277f9e2f0708c4910d7afe24b899a901555d?x-bce-process=image/resize,m_lfit,w_536,limit_1",11.4,8.8,686.47,1.0,6.3,"蔬果");
        getMyDB().FoodDao().insert(food249);
        Food food250 = new Food("上汤青菜","https://img0.baidu.com/it/u=230961859,527267459&fm=253&app=138&size=w931&n=0&f=JPEG&fmt=auto?sec=1684083600&t=9566c96665fe43e11b677b2601aed564",1.7,3.2,75.34,1.6,0.2,"蔬果");
        getMyDB().FoodDao().insert(food250);
        Food food251 = new Food("卤煮火烧","https://bkimg.cdn.bcebos.com/pic/f11f3a292df5e0fe6cf3258f566034a85fdf7251?x-bce-process=image/resize,m_lfit,w_536,limit_1",6.3,9.09,485.55,0.43,6.15,"肉蛋奶");
        getMyDB().FoodDao().insert(food251);
        Food food252 = new Food("油面筋","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_big_photo_url_013121d5ee9758137ac447bfd73d77b6.jpg",26.9,40.4,2059.41,1.3,25.1,"蔬果");
        getMyDB().FoodDao().insert(food252);
        Food food253 = new Food("蒸茄夹","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_836f7b33ded552fc95983f3a35ffad75.jpg",6.63,11.3,1063.19,0.76,20.4,"蔬果");
        getMyDB().FoodDao().insert(food253);
        Food food254 = new Food("菌子油","https://img1.baidu.com/it/u=3005126971,189493363&fm=253&app=138&size=w931&n=0&f=JPEG&fmt=auto?sec=1684083600&t=86f3a67e2f65c17a5f56e532eb149bff",2.53,11.47,406.02,1.1,4.76,"蔬果");
        getMyDB().FoodDao().insert(food254);
        Food food255 = new Food("红焖牛腩","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_ba5b96b1f2ec90ee1084aab2a00c9a74.jpg",7.8,4.14,770.19,0.71,15.29,"肉蛋奶");
        getMyDB().FoodDao().insert(food255);
        Food food256 = new Food("凉拌牛腱肉","https://bkimg.cdn.bcebos.com/pic/55e736d12f2eb9389b501ca5c2289235e5dde711c118?x-bce-process=image/resize,m_lfit,w_536,limit_1",20.23,1.62,573.45,0.06,5.48,"肉蛋奶");
        getMyDB().FoodDao().insert(food256);
        Food food257 = new Food("麻油鸡","https://bkimg.cdn.bcebos.com/pic/314e251f95cad1c80016d9527d3e6709c93d5111?x-bce-process=image/resize,m_lfit,w_536,limit_1",21.1,64.5,1661.76,0.0,5.6,"肉蛋奶");
        getMyDB().FoodDao().insert(food257);
        Food food258 = new Food("自制咸蛋","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_big_photo_url_26c27728eb83054acbc99c5a6d8121be.jpg",7.74,7.07,636.24,2.16,10.98,"肉蛋奶");
        getMyDB().FoodDao().insert(food258);
        Food food259 = new Food("蚂蚁上树","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_796da418d2e8b386246de36ba9942872.jpg",2.52,14.82,573.45,0.52,7.63,"肉蛋奶");
        getMyDB().FoodDao().insert(food259);
        Food food260 = new Food("鱼香苦瓜","http://s.boohee.cn/house/upload_food/2022/1/18/slim_user_72047_1213750673.jpg",1.47,6.48,351.61,1.0,6.03,"蔬果");
        getMyDB().FoodDao().insert(food260);
        Food food261 = new Food("鱼香肉丝粉丝煲","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_57f9f91217da389bcb6e894949d4faaa.jpg",4.16,41.33,1515.26,0.94,20.14,"肉蛋奶");
        getMyDB().FoodDao().insert(food261);
        Food food262 = new Food("碗仔翅","http://s.boohee.cn/house/upload_food/2021/3/3/big_photo_url_4a636b90f77169f387b18d126f89e1f9.jpg",10.56,8.86,418.58,0.7,2.29,"肉蛋奶");
        getMyDB().FoodDao().insert(food262);
        Food food263 = new Food("咖喱火鸡腿","http://s.boohee.cn/house/new_food/big/dd7abaa5c2ae431db6af68bc78795903.jpg",20.0,0.0,380.91,1.0,1.2,"肉蛋奶");
        getMyDB().FoodDao().insert(food263);
        Food food264 = new Food("烤尖椒西红柿拌素茄子","https://bkimg.cdn.bcebos.com/pic/7acb0a46f21fbe096b630fa058341b338744ebf80770?x-bce-process=image/resize,m_lfit,w_536,limit_1",0.9,3.3,62.79,1.0,0.2,"蔬果");
        getMyDB().FoodDao().insert(food264);
        Food food265 = new Food("奶油扒菜芯","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_big_photo_url_6baa301ee169949129cc94e861d33074.jpg",1.15,3.75,133.95,0.08,1.6,"蔬果");
        getMyDB().FoodDao().insert(food265);
        Food food266 = new Food("糖醋鱼段","https://bkimg.cdn.bcebos.com/pic/7aec54e736d12f2ed421932c42c2d56285356851?x-bce-process=image/resize,m_lfit,w_536,limit_1",39.0,1.2,1536.19,1.0,23.0,"肉蛋奶");
        getMyDB().FoodDao().insert(food266);
        Food food267 = new Food("山椒拌白肉","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_5f0d25b6b805436aaf709d567e7b790b.jpg",14.68,3.4,506.48,0.04,5.58,"肉蛋奶");
        getMyDB().FoodDao().insert(food267);
        Food food268 = new Food("鱼腹藏羊肉","https://bkimg.cdn.bcebos.com/pic/0df431adcbef7609226850a821dda3cc7cd99ea2?x-bce-process=image/resize,m_lfit,w_536,limit_1",6.7,22.2,586.01,0.0,2.5,"肉蛋奶");
        getMyDB().FoodDao().insert(food268);
        Food food269 = new Food("香辣油浸活鱼","https://bkimg.cdn.bcebos.com/pic/b151f8198618367adab4e12d67279cd4b31c86014fbc?x-bce-process=image/resize,m_lfit,w_536,limit_1",19.5,5.8,1456.66,1.0,27.6,"肉蛋奶");
        getMyDB().FoodDao().insert(food269);
        Food food270 = new Food("油焖竹笋","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_261e0aba958a108b383e5973a60d86f3.jpg",2.51,5.4,209.29,1.33,2.37,"蔬果");
        getMyDB().FoodDao().insert(food270);
        Food food271 = new Food("豆豉小辣椒","https://bkimg.cdn.bcebos.com/pic/aa64034f78f0f7368753b6720a55b319ebc41317?x-bce-process=image/resize,m_lfit,w_536,limit_1",10.2,6.4,2532.41,1.0,60.8,"蔬果");
        getMyDB().FoodDao().insert(food271);
        Food food272 = new Food("糖醋咕噜肉","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_f8f31eb4a450d754c84f87d54dddaeb5.jpg",7.79,7.28,1059.01,0.27,21.18,"肉蛋奶");
        getMyDB().FoodDao().insert(food272);
        Food food273 = new Food("蒜香鸡","https://bkimg.cdn.bcebos.com/pic/5fdf8db1cb13495409ab41f1524e9258d0094ae9?x-bce-process=image/resize,m_lfit,w_536,limit_1",14.61,2.0,573.45,0.93,6.09,"肉蛋奶");
        getMyDB().FoodDao().insert(food273);
        Food food274 = new Food("甜豆沙烤年糕","https://bkimg.cdn.bcebos.com/pic/e4dde71190ef76c6eadcf4be9916fdfaaf51671c?x-bce-process=image/resize,m_lfit,w_536,limit_1",3.03,33.24,753.44,0.94,3.85,"主食");
        getMyDB().FoodDao().insert(food274);
        Food food275 = new Food("烤年糕","https://bkimg.cdn.bcebos.com/pic/1f178a82b9014a9011b16a28ad773912b21bee9c?x-bce-process=image/resize,m_lfit,w_536,limit_1",3.03,33.24,753.44,0.94,3.85,"主食");
        getMyDB().FoodDao().insert(food275);
        Food food276 = new Food("绿豆八宝粥","https://img1.baidu.com/it/u=3977814008,2548931513&fm=253&app=138&size=w931&n=0&f=JPEG&fmt=auto?sec=1684083600&t=37f9ac4712a0201c412f38d46d7179fd",1.8,15.0,318.12,1.0,0.6,"主食");
        getMyDB().FoodDao().insert(food276);
        Food food277 = new Food("葱油鱼","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_big_photo_url_9c6538211158693c9653bb093068822c.jpg",14.39,0.57,745.07,0.04,12.93,"肉蛋奶");
        getMyDB().FoodDao().insert(food277);
        Food food278 = new Food("清蒸冬瓜","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_6bf5e9e1ed4ac608e7efc97cd555c4f3.jpg",9.8,4.18,531.6,0.6,8.01,"蔬果");
        getMyDB().FoodDao().insert(food278);
        Food food279 = new Food("酸菜白肉粉丝","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_042fadd04bb6538f546bf57f78360800.jpg",2.32,1.66,326.49,0.01,7.2,"肉蛋奶");
        getMyDB().FoodDao().insert(food279);
        Food food280 = new Food("菠萝炒鸭片","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_cb4f11bfb993419f870684b6126a285f.jpg",8.39,10.02,770.19,3.51,13.03,"肉蛋奶");
        getMyDB().FoodDao().insert(food280);
        Food food281 = new Food("烤鸡翅","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_big_photo_url_88a35aa8a6fb11a1153ab7a549903409.jpg",16.18,7.11,757.63,0.13,9.66,"肉蛋奶");
        getMyDB().FoodDao().insert(food281);
        Food food282 = new Food("素四烩","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_e0622696cb20ad6942c0988ad74629eb.jpg",2.05,6.92,322.31,1.82,4.89,"蔬果");
        getMyDB().FoodDao().insert(food282);
        Food food283 = new Food("西红柿炒鸡蛋","http://s.boohee.cn/house/upload_food/2022/12/27/big_photo_url_f654dbf79a8faa52ba38abcbd8b16204.jpg",4.7,7.2,577.64,0.0,10.0,"肉蛋奶");
        getMyDB().FoodDao().insert(food283);
        Food food284 = new Food("大蒜烧鲶鱼","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_7c8fcf8451eb4f5aa92ec22453002c6c.jpg",10.26,9.79,782.74,0.61,11.86,"肉蛋奶");
        getMyDB().FoodDao().insert(food284);
        Food food285 = new Food("双皮奶","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_b3085340fea1c53ff23909f4503fb91e.jpg",3.79,11.0,364.16,0.0,3.13,"肉蛋奶");
        getMyDB().FoodDao().insert(food285);
        Food food286 = new Food("肉松麦片粥","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_ac417d6308cb0f60e090dd9c6f82af7c.jpg",2.16,15.16,330.68,1.5,1.68,"主食");
        getMyDB().FoodDao().insert(food286);
        Food food287 = new Food("川肉丝","https://bkimg.cdn.bcebos.com/pic/dc54564e9258d1093e9a91d8da58ccbf6c814d5a?x-bce-process=image/resize,m_lfit,w_536,limit_1",21.5,25.8,1912.91,0.0,30.0,"肉蛋奶");
        getMyDB().FoodDao().insert(food287);
        Food food288 = new Food("香蛋吉利卷","https://img2.baidu.com/it/u=2885211405,1583304686&fm=253&app=138&size=w931&n=0&f=JPEG&fmt=auto?sec=1684083600&t=254659bfe616f77fc5026a0b263473bc",15.0,3.8,678.1,0.0,9.7,"肉蛋奶");
        getMyDB().FoodDao().insert(food288);
        Food food289 = new Food("黄瓜菜豆腐瘦肉汤","https://bkimg.cdn.bcebos.com/pic/3b87e950352ac65c1038899596bba5119313b17e1c89?x-bce-process=image/resize,m_lfit,w_536,limit_1",5.64,6.21,246.96,0.83,1.41,"汤");
        getMyDB().FoodDao().insert(food289);
        Food food290 = new Food("清汤羊肉","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_6add406023ef4229894e647c163480b2.jpg",6.6,0.49,167.43,0.06,1.21,"肉蛋奶");
        getMyDB().FoodDao().insert(food290);
        Food food291 = new Food("芹菜炒豆腐皮","https://bkimg.cdn.bcebos.com/pic/242dd42a2834349bbc53dd97c6ea15ce36d3be4f?x-bce-process=image/resize,m_lfit,w_536,limit_1",13.54,4.36,586.01,0.66,8.14,"蔬果");
        getMyDB().FoodDao().insert(food291);
        Food food292 = new Food("双菇苦瓜丝","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo201151313551816701.jpg",1.7,5.43,213.48,2.24,2.99,"蔬果");
        getMyDB().FoodDao().insert(food292);
        Food food293 = new Food("烤玉米","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_71b7064767617ec57c29ca3d7226d778.jpg",3.99,22.39,569.27,3.11,4.15,"蔬果");
        getMyDB().FoodDao().insert(food293);
        Food food294 = new Food("豆豉蒸排骨","http://s.boohee.cn/house/upload_food/2022/1/18/slim_big_photo_url_7400dd5acaa4ad66454b41d2f30ffe74.jpg",14.89,1.98,1155.28,0.22,23.19,"肉蛋奶");
        getMyDB().FoodDao().insert(food294);
        Food food295 = new Food("冬笋咸肉炒面筋","https://bkimg.cdn.bcebos.com/pic/5ab5c9ea15ce36d3000636a53ef33a87e950b121?x-bce-process=image/resize,m_lfit,w_536,limit_1",4.19,4.6,263.71,0.52,3.19,"肉蛋奶");
        getMyDB().FoodDao().insert(food295);
        Food food296 = new Food("盐煎肉","http://s.boohee.cn/house/upload_food/2020/6/18/big_photo_url_big_photo_url_34f9fa707705b1d8a99cc86495fb169f.jpg",13.31,4.08,950.18,0.85,17.87,"肉蛋奶");
        getMyDB().FoodDao().insert(food296);
        Food food297 = new Food("红豆烧猪手","https://bkimg.cdn.bcebos.com/pic/359b033b5bb5c9eada97b6cbdd39b6003af3b3b6?x-bce-process=image/resize,m_lfit,w_536,limit_1",14.4,4.3,1201.32,0.0,24.0,"肉蛋奶");
        getMyDB().FoodDao().insert(food297);

    }
}
