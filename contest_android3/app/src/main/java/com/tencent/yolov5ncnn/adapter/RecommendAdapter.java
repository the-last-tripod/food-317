package com.tencent.yolov5ncnn.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.tencent.yolov5ncnn.R;
import com.tencent.yolov5ncnn.enity.Food;

import java.util.List;

public class RecommendAdapter  extends RecyclerView.Adapter<RecommendAdapter.RecommendViewHolder>{

    private Context mContext;

    private List<Food> RFoods;


    public RecommendAdapter(Context context,List<Food> data) {
        mContext = context;
        RFoods = data;
    }
    @NonNull
    @Override
    public RecommendViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recommend_item, parent, false);
        RecommendAdapter.RecommendViewHolder viewHolder = new RecommendAdapter.RecommendViewHolder(view);

        return new RecommendViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecommendViewHolder holder, int position) {
        Food data =  RFoods.get(position);
        holder.recommend_food_name.setText(data.getName());
        holder.recommend_food_energy.setText(String.valueOf(data.getEnergy()));
        holder.recommend_food_eat.setText(String.valueOf(data.mass));
        if (data.img_path != null && data.img_path != ""){
            Glide.with(holder.recommend_image.getContext())
                    .load(data.getImg_path())
                    .into(holder.recommend_image);
        }
    }

    @Override
    public int getItemCount() {
        return RFoods.size();
    }

    // 实现 ViewHolder
    public static class RecommendViewHolder extends RecyclerView.ViewHolder {

        public ImageView recommend_image;

        public TextView recommend_food_name;

        public TextView recommend_food_energy;


        public TextView recommend_food_eat;




        public RecommendViewHolder(@NonNull View itemView) {
            super(itemView);
            recommend_image = itemView.findViewById(R.id.recommend_image);
            recommend_food_name = itemView.findViewById(R.id.recommend_food_name);
            recommend_food_energy = itemView.findViewById(R.id.recommend_food_energy);
            recommend_food_eat = itemView.findViewById(R.id.recommend_food_eat);
        }
    }
}
