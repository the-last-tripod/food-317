package com.tencent.yolov5ncnn.enity;

import androidx.room.ColumnInfo;

import com.tencent.yolov5ncnn.MyApplication;
import com.tencent.yolov5ncnn.dao.FoodDao;

import java.util.ArrayList;
import java.util.List;

public class FoodChoiceInfo {
    public int id;// 序号
    @ColumnInfo
    public String name;//食物名称
    @ColumnInfo
    public String img_path;//图片路径
    @ColumnInfo
    public double protein;//蛋白质
    @ColumnInfo
    public double carb; //碳水化合物
    @ColumnInfo
    public double calorie; //热量
    @ColumnInfo
    public double fat; //脂肪
    @ColumnInfo
    public int mass;//质量
    @ColumnInfo
    public String type;//类型包含'主食','肉蛋奶','蔬果','汤'


    public FoodChoiceInfo(String name, String img_path, double protein, double carb, double calorie, double fat, String type) {
        this.name = name;
        this.img_path = img_path;
        this.protein = protein;
        this.carb = carb;
        this.calorie = calorie;
        this.fat = fat;
        this.type = type;
    }
    //从数据库中获取所需信息并返回
    public static List<FoodChoiceInfo> getDefaultFoodChoices(FoodDao foodDao) {
        List<FoodChoiceInfo> foodChoiceList = new ArrayList<FoodChoiceInfo>();
        List<Food> foodList = foodDao.queryAll();
        for (Food food : foodList) {
            foodChoiceList.add(new FoodChoiceInfo(
                    food.getName(),
                    food.getImg_path(),
                    food.getProtein(),
                    food.getCarb(),
                    food.getEnergy(),
                    food.getFat(),
                    food.type));
        }
        return foodChoiceList;
    }
    public FoodChoiceInfo() {}
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getImg_path() {
        return img_path;
    }
    public void setImg_path(String img_path) {
        this.img_path = img_path;
    }
    public double getProtein() {
        return protein;
    }
    public void setProtein(double protein) {
        this.protein = protein;
    }
    public double getCarb() {
        return carb;
    }
    public void setCarb(double carb) {
        this.carb = carb;
    }
    public double getCalorie() {
        return calorie;
    }
    public void setCalorie(double calorie) {
        this.calorie = calorie;
    }
    public double getFat() {
        return fat;
    }
    public void setFat(double fat) {
        this.fat = fat;
    }
    public void setMass(int mass){this.mass = mass; }
    public int getMass(){return mass; }
    public String getType(){return type; }

}
