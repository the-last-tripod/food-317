package com.tencent.yolov5ncnn.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.tencent.yolov5ncnn.dao.FoodDao;
import com.tencent.yolov5ncnn.dao.FoodIntakeDao;
import com.tencent.yolov5ncnn.dao.LeftItemDao;
import com.tencent.yolov5ncnn.dao.NutrIntakeDao;
import com.tencent.yolov5ncnn.dao.UserDao;
import com.tencent.yolov5ncnn.enity.Food;
import com.tencent.yolov5ncnn.enity.FoodIntake;
import com.tencent.yolov5ncnn.enity.LeftItem;
import com.tencent.yolov5ncnn.enity.NutrIntake;
import com.tencent.yolov5ncnn.enity.User;


//entities表示该数据库有哪些表，version表示数据库的版本号
//exportSchema表示是否导出数据库信息的json串，建议设为false，若设为true还需指定json文件的保存路径
@Database(entities = {User.class, Food.class, FoodIntake.class, NutrIntake.class}, version = 1, exportSchema = true)
public abstract class MyDatabase extends RoomDatabase {
    // 获取该数据库中某张表的持久化对象
    public abstract UserDao UserDao();
    public abstract FoodDao FoodDao();
    public abstract FoodIntakeDao FoodIntakeDao();
    public abstract NutrIntakeDao NutrIntakeDao();

}
