package com.tencent.yolov5ncnn.component.foodchoice.model;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.lxj.xpopup.core.BottomPopupView;
import com.lxj.xpopup.util.XPopupUtils;
import com.tencent.yolov5ncnn.MyApplication;
import com.tencent.yolov5ncnn.R;
import com.tencent.yolov5ncnn.component.foodchoice.activity.FoodChoiceActivity;
import com.tencent.yolov5ncnn.component.foodchoice.adapter.FoodCartAdapter;
import com.tencent.yolov5ncnn.component.foodchoice.adapter.FoodChoiceAdapter;
import com.tencent.yolov5ncnn.dao.FoodDao;
import com.tencent.yolov5ncnn.enity.Food;
import com.tencent.yolov5ncnn.enity.FoodChoiceInfo;

import java.util.ArrayList;
import java.util.List;

public class FoodCartPopup extends BottomPopupView {
    private RecyclerView foodCartView;// 食物列表
    List<Food> selectedFoods;// 获取食物对象
    FoodDao foodDao = MyApplication.getInstance().getMyDB().FoodDao();// 获取食物对象
    private FoodCartAdapter foodAdapter;// 获取适配器

    private FoodChoiceActivity foodChoiceActivity;
    /**
     * 构造函数
     */
    public FoodCartPopup(@NonNull Context context, List<Food> selectedFoods, FoodChoiceActivity foodChoiceActivity) {
        super(context);
        this.selectedFoods = selectedFoods;
        this.foodChoiceActivity = foodChoiceActivity;
    }
    /**
     * 获得xml文件
     */
    @Override
    protected int getImplLayoutId() {
        return R.layout.food_cart_bottom_popup;
    }
    @Override
    protected void onCreate() {
        super.onCreate();
        foodCartView = findViewById(R.id.rl_cart_food_choice);
        // 创建垂直方向的线性布局管理器
        LinearLayoutManager manager = new LinearLayoutManager(getContext(),RecyclerView.VERTICAL,false);
        // 设置循环视图的布局管理器
        foodCartView.setLayoutManager(manager);
        //获取食物列表
        List<FoodChoiceInfo> foodChoiceList = FoodChoiceInfo.getDefaultFoodChoices(foodDao);
        // 构建一个食物选择列表的适配器
        foodAdapter = new FoodCartAdapter(getContext(), selectedFoods, removedFood -> {
            // 更新购物车的数量视图
            int updatedSelectedCount = selectedFoods.size(); // 计算新的选择数量
            foodChoiceActivity.updateSelectedFoodCountView(updatedSelectedCount);
        });
        // 设置适配器
        foodCartView.setAdapter(foodAdapter);
    }

    /**
     * 设置最大高度
     */
    @Override
    protected int getMaxHeight() {
        return (int) (getWindowHeight(getContext()) * .5f);
    }
    /**
     * 弹窗的高度，用来动态设定当前弹窗的高度，受getMaxHeight()限制
     */
    protected int getPopupHeight() {
        return (int) (getWindowHeight(getContext()) * .5f);
    }
    /**
     * 获取屏幕高度的方法
     */
    protected int getWindowHeight(Context context) {
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        windowManager.getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.heightPixels;
    }
}
