package com.tencent.yolov5ncnn.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.bumptech.glide.Glide;
import com.tencent.yolov5ncnn.R;
import com.tencent.yolov5ncnn.enity.Food;

import java.util.List;

public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.MenuViewHolder> {

    Context context;
    private final List<Food> menuData;

    public MenuAdapter(Context context, List<Food> menuData) {
        this.context = context;
        this.menuData = menuData;
    }

    @NonNull
    @Override
    public MenuViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.menu_item, parent, false);
        MenuAdapter.MenuViewHolder viewHolder = new MenuAdapter.MenuViewHolder(view);

        return new MenuAdapter.MenuViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MenuViewHolder holder, int position) {
        Food food = menuData.get(position);
        holder.menu_food_name.setText(food.getName());
        holder.menu_food_protein.setText(String.valueOf((int) food.getProtein()));
        holder.menu_food_energy.setText(String.valueOf((int) food.getEnergy()));
        holder.menu_food_fat.setText(String.valueOf((int) food.getFat()));
        holder.menu_food_carbs.setText(String.valueOf((int) food.getCarb()));
        if (food.img_path != null && food.img_path != ""){
            Glide.with(holder.menu_food_image.getContext())
                    .load(food.getImg_path())
                    .into(holder.menu_food_image);
        }
    }

    @Override
    public int getItemCount() {

        if(menuData!=null){
            return menuData.size();
        }
        return 0;
    }

    public class MenuViewHolder extends RecyclerView.ViewHolder{

        private ImageView menu_food_image;
        private TextView menu_food_name;
        private TextView menu_food_energy;
        private TextView menu_food_protein;
        private TextView menu_food_carbs;
        private TextView menu_food_fat;


        public MenuViewHolder(@NonNull View itemView) {
            super(itemView);
            //找到条目空间

            menu_food_image = (ImageView) itemView.findViewById(R.id.menu_food_image);
            menu_food_name = (TextView) itemView.findViewById(R.id.menu_food_name);
            menu_food_energy = (TextView) itemView.findViewById(R.id.menu_food_energy);
            menu_food_protein = (TextView) itemView.findViewById(R.id.menu_food_protein);
            menu_food_carbs = (TextView) itemView.findViewById(R.id.menu_food_carbs);
            menu_food_fat = (TextView) itemView.findViewById(R.id.menu_food_fat);
        }


    }
}
