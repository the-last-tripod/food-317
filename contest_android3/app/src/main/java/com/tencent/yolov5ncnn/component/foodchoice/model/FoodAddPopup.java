package com.tencent.yolov5ncnn.component.foodchoice.model;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import com.lxj.xpopup.core.BottomPopupView;
import com.tencent.yolov5ncnn.R;
import com.tencent.yolov5ncnn.enity.Food;


public class FoodAddPopup extends BottomPopupView {
    private TextView txt_cancel;
    private TextView txt_add;
    private EditText et_food_name;
    private EditText et_calories;
    private EditText editCarb, editProtein, editFat;
    private TextView txtCarb, txtProtein, txtFat;
    private Food food;


    /**
     * 构造函数
     */
    public FoodAddPopup(@NonNull Context context) {
        super(context);
    }

    /**
     * 获得xml文件
     */
    @Override
    protected int getImplLayoutId() {
        return R.layout.popup_bottom_food_add;
    }

    @Override
    protected void onCreate() {
        super.onCreate();
        // 初始化Food对象
        food = new Food();


        // 初始化视图
        initView();
        // 设置文本改变监听
        setTextWatchers();
    }

    private void initView() {
        // 获取EditText和TextView对象
        editCarb = findViewById(R.id.edit_carb);
        editProtein = findViewById(R.id.edit_protein);
        editFat = findViewById(R.id.edit_fat);
        txt_cancel = findViewById(R.id.txt_cancel);
        et_food_name = findViewById(R.id.et_food_name);
        txt_add = findViewById(R.id.txt_add);
        et_calories = findViewById(R.id.et_calories);
        txtCarb = findViewById(R.id.txt_carb);
        txtProtein = findViewById(R.id.txt_protein);
        txtFat = findViewById(R.id.txt_fat);

        // 设置点击事件
        txt_cancel.setOnClickListener(v -> dismiss());

        txt_add.setOnClickListener(v -> {
            // 这里可以根据您的需求添加点击响应事件
            onAddClick();
            saveToFoodObject();
            dismiss();
        });
    }
    private void setTextWatchers() {
        editCarb.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                txtCarb.setTextColor(!s.toString().isEmpty() ?
                        ContextCompat.getColor(getContext(), R.color.black) :
                        ContextCompat.getColor(getContext(), R.color.font_grey));
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void afterTextChanged(Editable s) {}
        });

        editProtein.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                txtProtein.setTextColor(!s.toString().isEmpty() ?
                        ContextCompat.getColor(getContext(), R.color.black) :
                        ContextCompat.getColor(getContext(), R.color.font_grey));
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void afterTextChanged(Editable s) {}
        });

        editFat.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                txtFat.setTextColor(!s.toString().isEmpty() ?
                        ContextCompat.getColor(getContext(), R.color.black) :
                        ContextCompat.getColor(getContext(), R.color.font_grey));
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void afterTextChanged(Editable s) {}
        });
    }
    private void saveToFoodObject() {
        // 获取EditText的值并保存到Food对象
        String carbValue = editCarb.getText().toString().trim();
        String proteinValue = editProtein.getText().toString().trim();
        String fatValue = editFat.getText().toString().trim();

        if (!carbValue.isEmpty()) {
            food.carb = Double.parseDouble(carbValue);
        }

        if (!proteinValue.isEmpty()) {
            food.protein = Double.parseDouble(proteinValue);
        }

        if (!fatValue.isEmpty()) {
            food.fat = Double.parseDouble(fatValue);
        }
        Toast.makeText(getContext(), "自定义保存成功", Toast.LENGTH_SHORT).show();
    }

    private void onAddClick() {
        food.name = et_food_name.getText().toString().trim();
        try {
            food.energy = Double.parseDouble(et_calories.getText().toString().trim());
        } catch (NumberFormatException e) {
            // 这里处理当用户没有输入有效的数字时的异常
            Toast.makeText(getContext(), "请输入有效的热量值", Toast.LENGTH_SHORT).show();
            return;
        }
    }
    /**
     * 设置最大高度
     */
    @Override
    protected int getMaxHeight() {
        return (int) (getWindowHeight(getContext()) * .5f);
    }
    /**
     * 弹窗的高度，用来动态设定当前弹窗的高度，受getMaxHeight()限制
     */
    protected int getPopupHeight() {
        return (int) (getWindowHeight(getContext()) * .5f);
    }
    /**
     * 获取屏幕高度的方法
     */
    protected int getWindowHeight(Context context) {
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        windowManager.getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.heightPixels;
    }
}
