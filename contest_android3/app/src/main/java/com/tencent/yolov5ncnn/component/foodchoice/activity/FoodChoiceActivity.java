package com.tencent.yolov5ncnn.component.foodchoice.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.material.card.MaterialCardView;
import com.lxj.xpopup.XPopup;
import com.qmuiteam.qmui.util.QMUIStatusBarHelper;
import com.tencent.yolov5ncnn.MyApplication;
import com.tencent.yolov5ncnn.R;
import com.tencent.yolov5ncnn.component.foodchoice.adapter.FoodChoiceAdapter;
import com.tencent.yolov5ncnn.component.foodchoice.adapter.FoodChoiceTypeAdapter;
import com.tencent.yolov5ncnn.component.foodchoice.model.FoodAddPopup;
import com.tencent.yolov5ncnn.component.foodchoice.model.FoodCartPopup;
import com.tencent.yolov5ncnn.component.identification.activity.ChooseWayActivity;
import com.tencent.yolov5ncnn.dao.FoodDao;
import com.tencent.yolov5ncnn.enity.Food;
import com.tencent.yolov5ncnn.enity.FoodChoiceInfo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FoodChoiceActivity extends AppCompatActivity implements FoodChoiceAdapter.OnFoodSelectedListener {
    private ImageView cartImageView; // 购物车图标
    private ImageView cameraImageView; // 照相机图标
    private FoodDao foodDao = MyApplication.getInstance().getMyDB().FoodDao(); // 数据访问对象
    private RecyclerView foodTypeRecyclerView; // 食物类型循环视图
    private RecyclerView foodListRecyclerView; // 食物列表循环视图
    private FoodChoiceTypeAdapter typeAdapter; // 类型适配器1
    private FoodChoiceAdapter foodAdapter; // 食物适配器
    private TextView selectedFoodCountTextView; // 已选择食物数量文本
    private MaterialCardView selectedFoodCountView; // 已选择食物数量视图
    private RelativeLayout rlSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_choice);

        initViewElements(); // 初始化视图元素
        initFoodChoiceGrid(); // 初始化网格布局的循环视图
        setupStatusBar(); // 设置状态栏
    }

    /**
     * 初始化视图元素，包括点击监听器
     */
    private void initViewElements() {
        cameraImageView = findViewById(R.id.img_camera);
        cartImageView = findViewById(R.id.btn_cart);
        selectedFoodCountTextView = findViewById(R.id.tv_number_food_choice);
        selectedFoodCountView = findViewById(R.id.mcv_number_food_choice);
        rlSearch = findViewById(R.id.rl_search);
        cartImageView.setOnClickListener(v -> showSelectedFoodsDialog());
        cameraImageView.setOnClickListener(v -> {
            startActivity(new Intent(this, ChooseWayActivity.class));
        });
    }

    /**
     * 初始化食物类型和食物列表的网格布局循环视图
     */
    private void initFoodChoiceGrid() {
        foodListRecyclerView = findViewById(R.id.rv_food_choice);
        foodTypeRecyclerView = findViewById(R.id.rv_food_choice_type);

        setupRecyclerViewLayouts(); // 设置循环视图的布局管理器
        setupTypeAdapter(); // 设置食物类型适配器
        setupFoodAdapter(); // 设置食物列表适配器
    }

    /**
     * 设置循环视图的布局管理器
     */
    private void setupRecyclerViewLayouts() {
        foodListRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        foodTypeRecyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    /**
     * 设置食物类型适配器
     */
    private void setupTypeAdapter() {
        List<String> typeList = Arrays.asList("主食", "肉蛋奶", "蔬果", "汤");
        typeAdapter = new FoodChoiceTypeAdapter(typeList, this::onTypeClick);
        foodTypeRecyclerView.setAdapter(typeAdapter);
    }

    /**
     * 设置食物列表适配器
     */
    private void setupFoodAdapter() {
        List<FoodChoiceInfo> foodChoiceList = FoodChoiceInfo.getDefaultFoodChoices(foodDao);
        foodAdapter = new FoodChoiceAdapter(this, foodChoiceList, this);
        foodAdapter.setOnItemClickListener(foodAdapter);
        foodListRecyclerView.setAdapter(foodAdapter);
        foodListRecyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    /**
     * 设置状态栏
     */
    private void setupStatusBar() {
        QMUIStatusBarHelper.translucent(this);
        QMUIStatusBarHelper.setStatusBarLightMode(this);
        // 获取状态栏高度
        int statusBarHeight = QMUIStatusBarHelper.getStatusbarHeight(this) + 15 ;

        // 获取RelativeLayout当前的布局参数
        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) rlSearch.getLayoutParams();

        // 将RelativeLayout的顶部边距设置为状态栏高度
        layoutParams.setMargins(layoutParams.leftMargin, statusBarHeight , layoutParams.rightMargin, layoutParams.bottomMargin);

        // 将修改后的布局参数应用到RelativeLayout
        rlSearch.setLayoutParams(layoutParams);
    }

    /**
     * 根据食物类型过滤食物列表
     *
     * @param type 食物类型
     */
    private void onTypeClick(String type) {
        List<FoodChoiceInfo> foodChoiceList = FoodChoiceInfo.getDefaultFoodChoices(foodDao);
        List<FoodChoiceInfo> filteredFoodList = new ArrayList<>();
        for (FoodChoiceInfo food : foodChoiceList) {
            if (food.getType().equals(type)) {
                filteredFoodList.add(food);
            }
        }
        foodAdapter.updateFoodList(filteredFoodList);
    }

    /**
     * 显示已选择食物的对话框
     */
    private void showSelectedFoodsDialog() {
        List<Food> selectedFoods = foodAdapter.getSelectedFoods();
        new XPopup.Builder(this)
                .asCustom(new FoodCartPopup(this, selectedFoods, this)) // 注意这里传递了FoodChoiceActivity实例
                .show();
    }

    /**
     * 设置购物车的数量视图
     *
     * @param selectedCount 已选择食物的数量
     */
    @Override
    public void onFoodSelected(int selectedCount) {
        updateSelectedFoodCountView(selectedCount);
    }

    /**
     * 更新购物车的数量视图
     */
    public void updateSelectedFoodCountView(int selectedCount) {
        if (selectedCount == 0) {
            selectedFoodCountView.setVisibility(View.GONE);
        } else {
            selectedFoodCountView.setVisibility(View.VISIBLE);
            selectedFoodCountTextView.setText(String.valueOf(selectedCount));
        }
    }
}