package com.tencent.yolov5ncnn.dao;

import androidx.room.ColumnInfo;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.tencent.yolov5ncnn.enity.User;

@Dao
public interface UserDao {
    @Insert
    void insert(User user);

    //根据ID更新,确保ID为1!!!
    @Update
    void update(User user);

    //更新个人信息
    @Query("UPDATE User SET name = :name, gender = :gender, age = :age, height = :height, weight = :weight where id = 1")
    void updateInfo(String name, String gender, int age, double height, double weight);

    //更新个人饮食需求信息
    @Query("UPDATE User SET needProteinMax = :proteinMax, needProteinMin = :proteinMin, needCarbMax = :carbMax, needCarbMin = :carbMin, " +
            "needFatMax = :fatMax,needFatMin = :fatMin, needEnergy = :energy where id = 1")
    void updateNutriInfo(int energy, int proteinMax, int proteinMin,int carbMax, int carbMin,int fatMax,int fatMin);


    //查询用户
    @Query("SELECT * FROM User limit 1")
    User queryAll();


    //更新用户体重
    @Query("UPDATE User SET weight = :weight where id = 1")
    void updateWeight(double weight);

    //更新用户体重
    @Query("UPDATE User SET height = :height where id = 1")
    void updateHeight(double height);

    @Query("UPDATE User SET sport = :sport where id = 1")
    void updateSport(int sport);

    //更新特殊信息
    @Query("UPDATE User SET pregnant = :pregnant, bodyBuilding = :bodyBuilding, jiaJian = :jiaJian, jiaKang = :jiaKang, bianMi = :bianMi, tangNiaoBing = :tangNiaoBing," +
            "pinXue = :pinXue, gaoXueYa = :gaoXueYa, gaoXueZhi = :gaoXueZhi, gaoNiaoSuan = :gaoNiaoSuan, " +
            "jinShiZhangAi = :jinShiZhangAi, diXueTang = :diXueTang, diXueYa = :diXueYa where id = 1")
    void updateSpecial(boolean pregnant, boolean bodyBuilding, boolean jiaJian ,boolean jiaKang, boolean bianMi, boolean tangNiaoBing,
                        boolean pinXue, boolean gaoXueYa, boolean gaoXueZhi, boolean gaoNiaoSuan, boolean diXueYa, boolean diXueTang,
                        boolean jinShiZhangAi);

}
