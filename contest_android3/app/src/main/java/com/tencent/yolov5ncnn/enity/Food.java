package com.tencent.yolov5ncnn.enity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity
public class Food {
    @PrimaryKey(autoGenerate = true)
    public int id;// 序号
    @ColumnInfo
    public String name;//食物名称
    @ColumnInfo
    public String img_path;//图片路径
    @ColumnInfo
    public double protein;//蛋白质
    @ColumnInfo
    public double carb; //碳水化合物
    @ColumnInfo
    public double energy; //能量
    @ColumnInfo
    public double mineral_salt; //无机盐
    @ColumnInfo
    public double fat; //脂肪
    @ColumnInfo
    public int mass;//质量
    @ColumnInfo
    public String type;//类型包含'主食','肉蛋奶','蔬果','汤'


    @Ignore
    public Food(String name, String img_path, double protein, double carb, double energy, double mineral_salt, double fat,String type) {
        this.name = name;
        this.img_path = img_path;
        this.protein = protein;
        this.carb = carb;
        this.energy = energy;
        this.mineral_salt = mineral_salt;
        this.fat = fat;
        this.type = type;
    }

    public Food() {

    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImg_path() {
        return img_path;
    }

    public void setImg_path(String img_path) {
        this.img_path = img_path;
    }

    public double getProtein() {
        return protein;
    }

    public void setProtein(double protein) {
        this.protein = protein;
    }

    public double getCarb() {
        return carb;
    }

    public void setCarb(double carb) {
        this.carb = carb;
    }

    public double getEnergy() {
        return energy;
    }

    public void setEnergy(double energy) {
        this.energy = energy;
    }

    public double getMineral_salt() {
        return mineral_salt;
    }

    public void setMineral_salt(double mineral_salt) {
        this.mineral_salt = mineral_salt;
    }

    public double getFat() {
        return fat;
    }

    public void setFat(double fat) {
        this.fat = fat;
    }

    public void setMass(int mass){this.mass = mass;}

    public int getMass() {return this.mass;}

    @Override
    public String toString() {
        return "Food{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", img_path='" + img_path + '\'' +
                ", protein=" + protein +
                ", carb=" + carb +
                ", energy=" + energy +
                ", na=" + mineral_salt +
                ", fat=" + fat +
                '}';
    }
}
