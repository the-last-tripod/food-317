package com.tencent.yolov5ncnn.component.splash.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.qmuiteam.qmui.util.QMUIStatusBarHelper;
import com.tencent.yolov5ncnn.R;
import com.tencent.yolov5ncnn.component.login.activity.LogInActivity;

/**
* 作用：启动页面，在开屏时展示，更加美观
**/
public class SplashActivity extends AppCompatActivity {

    // 延迟时间（毫秒）
    private static final int SPLASH_DISPLAY_LENGTH = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_layout);
        //设置沉浸式状态栏，即设置状态栏颜色为透明
        QMUIStatusBarHelper.translucent(this);
        // 设置状态栏字体颜色为黑色
        QMUIStatusBarHelper.setStatusBarLightMode(this);
        // 创建一个 Handler 来处理延迟操作
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // 跳转到登录界面
                navigateToMainActivity();
            }
        }, SPLASH_DISPLAY_LENGTH);
    }

    /**
     * 参数：无
     * 返回值：无
     * 功能：跳转到登录界面
     */
    private void navigateToMainActivity() {
        Intent intent = new Intent(SplashActivity.this, LogInActivity.class);
        startActivity(intent);
        finish();
    }
}