package com.tencent.yolov5ncnn.enity;


import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class LeftItem {
    @PrimaryKey(autoGenerate = true)
    public int id;
    @ColumnInfo
    public String name;

    @ColumnInfo
    public boolean isSelected;

    public LeftItem() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
