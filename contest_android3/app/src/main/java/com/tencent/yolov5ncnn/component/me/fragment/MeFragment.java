package com.tencent.yolov5ncnn.component.me.fragment;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import com.tencent.yolov5ncnn.AnalysisActivity;
import com.tencent.yolov5ncnn.MyApplication;
import com.tencent.yolov5ncnn.R;
import com.tencent.yolov5ncnn.component.recommend.activity.RecommendActivity;
import com.tencent.yolov5ncnn.SettingActivity;
import com.tencent.yolov5ncnn.chat.ChatMainActivity;
import com.tencent.yolov5ncnn.enity.User;


public class MeFragment extends Fragment implements View.OnClickListener{
    private ImageButton settingBtn;
    private TextView nameTv, ageTv, heightTv, weightTv;
    private ImageButton statistics,base_information,diet_regimen,my_collection,ai_assistant,message_center;
    private TextView all_tools,help_feedback,contact_us,about_us;
    private boolean isPrivacy = true;
    private ImageButton ibPrivacy;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_me2, container, false);


        nameTv = view.findViewById(R.id.person_name);
        ageTv = view.findViewById(R.id.person_age);
        heightTv = view.findViewById(R.id.person_height);
        weightTv = view.findViewById(R.id.person_weight);
        settingBtn = view.findViewById(R.id.setting_btn);

        all_tools = view.findViewById(R.id.all_tools);
        help_feedback = view.findViewById(R.id.help_feedback);
        contact_us = view.findViewById(R.id.contact_us);
        about_us = view.findViewById(R.id.about_us);
        ibPrivacy = view.findViewById(R.id.privacy_image);

        //数据统计
        statistics = view.findViewById(R.id.statistics);

        //基本信息
        base_information = view.findViewById(R.id.base_information);
        //饮食方案
        diet_regimen = view.findViewById(R.id.diet_regimen);
        //我的收藏
        my_collection = view.findViewById(R.id.my_collection);
        //AI助手
        ai_assistant = view.findViewById(R.id.ai_assistant);
        //消息中心
        message_center = view.findViewById(R.id.message_center);

        statistics.setOnClickListener(this);
        base_information.setOnClickListener(this);
        diet_regimen.setOnClickListener(this);
        my_collection.setOnClickListener(this);
        ai_assistant.setOnClickListener(this);
        message_center.setOnClickListener(this);





        // 设置按钮 点击跳转到设置页面
        settingBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), SettingActivity.class);
                startActivity(intent);
            }
        });

        ibPrivacy.setOnClickListener(v -> {
            if(!isPrivacy){
                Resources res = getResources();
                String strPrivacy = res.getString(R.string.privacy);
                // 从数据库中获取用户信息并设置到TextView上
                nameTv.setText(strPrivacy);
                ageTv.setText(strPrivacy);
                heightTv.setText(strPrivacy);
                weightTv.setText(strPrivacy);
                isPrivacy = true;
            }
            else{
                // 从数据库中获取用户信息并设置到TextView上
                User user = MyApplication.getInstance().getMyDB().UserDao().queryAll();
                nameTv.setText(user.name);
                ageTv.setText(String.valueOf(user.age));
                heightTv.setText(String.valueOf(user.height));
                weightTv.setText(String.valueOf(user.weight));
                isPrivacy = false;
            }
        });

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.statistics:
                //数据统计
                startActivity(new Intent(getActivity(), AnalysisActivity.class));

                break;
            case R.id.base_information:
                //基本信息
                startActivity(new Intent(getActivity(), SettingActivity.class));

                break;
            case R.id.diet_regimen:
                //饮食方案
                startActivity(new Intent(getActivity(), RecommendActivity.class));

                break;
            case R.id.my_collection:
                //我的收藏
                startActivity(new Intent(getActivity(), AnalysisActivity.class));

                break;
            case R.id.ai_assistant:
                //AI助手
                startActivity(new Intent(getActivity(), ChatMainActivity.class));

                break;
            case R.id.message_center:
                //消息中心
                startActivity(new Intent(getActivity(), AnalysisActivity.class));

                break;

        }

    }
}
