package com.tencent.yolov5ncnn;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import android.content.res.ColorStateList;
import android.os.Bundle;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.qmuiteam.qmui.util.QMUIStatusBarHelper;

/**
 * 首页导航栏
 */
public class HomeActivity extends AppCompatActivity {
    private BottomNavigationView navView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        //设置沉浸式状态栏，即设置状态栏颜色为透明
        QMUIStatusBarHelper.translucent(this);
        // 设置状态栏字体颜色为黑色
        QMUIStatusBarHelper.setStatusBarLightMode(this);
        navView = findViewById(R.id.nav_view_index);
//        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
//                R.id.navigation_index, R.id.navigation_me)
//                .build();

        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
//        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navView, navController);
        // 在 NavigationActivity 中获取要启动的 Fragment ID
        int fragmentId = getIntent().getIntExtra("fragment_id", R.id.navigation_index);
        // 使用 NavController 导航到指定的 Fragment
        navController.navigate(fragmentId);
    }
}