package com.tencent.yolov5ncnn.enity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class User {
    @PrimaryKey(autoGenerate = true)
    public int id; // 序号
    @ColumnInfo
    public String name; // 姓名
    @ColumnInfo
    private String gender; // 性别
    @ColumnInfo
    public int age; // 年龄
    @ColumnInfo
    public double height; //身高
    @ColumnInfo
    public double weight; //体重
    @ColumnInfo
    public int sport; //等级从0~4
    @ColumnInfo
    public int needProteinMax; //需要的最大蛋白质
    @ColumnInfo
    public int needProteinMin; //需要的最大蛋白质
    @ColumnInfo
    public int needCarbMax; //需要的最多碳水
    @ColumnInfo
    public int needCarbMin; //需要的最少碳水
    @ColumnInfo
    public int needFatMax; //需要的最多脂肪
    @ColumnInfo
    public int needFatMin; //需要的最少脂肪
    @ColumnInfo
    public int needEnergy; //需要的能量

    @ColumnInfo
    public boolean jiaJian; //甲减

    @ColumnInfo
    public boolean jiaKang; //甲亢

    @ColumnInfo
    public boolean bianMi; //便秘

    @ColumnInfo
    public boolean tangNiaoBing; //糖尿病

    @ColumnInfo
    public boolean pinXue; //贫血

    @ColumnInfo
    public boolean gaoXueYa; //高血压

    @ColumnInfo
    public boolean gaoXueZhi; //高血脂

    @ColumnInfo
    public boolean gaoNiaoSuan; //高尿酸

    @ColumnInfo
    public boolean jinShiZhangAi; //进食障碍

    @ColumnInfo
    public boolean pregnant;//孕妇

    @ColumnInfo
    public boolean Bodybuilding; //健身人士

    @ColumnInfo
    public boolean diXueTang; //低血糖

    @ColumnInfo
    public boolean diXueYa; // 低血压

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", height=" + height +
                ", weight=" + weight +
                ", jiaJian=" + jiaJian +
                ", jiaKang=" + jiaKang +
                ", bianMi=" + bianMi +
                ", tangNiaoBing=" + tangNiaoBing +
                ", pinXue=" + pinXue +
                ", gaoXueYa=" + gaoXueYa +
                ", gaoXueZhi=" + gaoXueZhi +
                ", gaoNiaoSuan=" + gaoNiaoSuan +
                ", jinShiZhangAi=" + jinShiZhangAi +
                ", pregnant=" + pregnant +
                ", Bodybuilding=" + Bodybuilding +
                ", diXueTang=" + diXueTang +
                ", diXueYa=" + diXueYa +
                '}';
    }

    public boolean isPregnant() {
        return pregnant;
    }

    public void setPregnant(boolean pregnant) {
        this.pregnant = pregnant;
    }

    public boolean isBodybuilding() {
        return Bodybuilding;
    }

    public void setBodybuilding(boolean bodybuilding) {
        Bodybuilding = bodybuilding;
    }

    public boolean isDiXueTang() {
        return diXueTang;
    }

    public void setDiXueTang(boolean diXueTang) {
        this.diXueTang = diXueTang;
    }

    public boolean isDiXueYa() {
        return diXueYa;
    }

    public void setDiXueYa(boolean diXueYa) {
        this.diXueYa = diXueYa;
    }

    public boolean isJiaJian() {
        return jiaJian;
    }

    public void setJiaJian(boolean jiaJian) {
        this.jiaJian = jiaJian;
    }

    public boolean isJiaKang() {
        return jiaKang;
    }

    public void setJiaKang(boolean jiaKang) {
        this.jiaKang = jiaKang;
    }

    public boolean isBianMi() {
        return bianMi;
    }

    public void setBianMi(boolean bianMi) {
        this.bianMi = bianMi;
    }

    public boolean isTangNiaoBing() {
        return tangNiaoBing;
    }

    public void setTangNiaoBing(boolean tangNiaoBing) {
        this.tangNiaoBing = tangNiaoBing;
    }

    public boolean isPinXue() {
        return pinXue;
    }

    public void setPinXue(boolean pinXue) {
        this.pinXue = pinXue;
    }

    public boolean isGaoXueYa() {
        return gaoXueYa;
    }

    public void setGaoXueYa(boolean gaoXueYa) {
        this.gaoXueYa = gaoXueYa;
    }

    public boolean isGaoXueZhi() {
        return gaoXueZhi;
    }

    public void setGaoXueZhi(boolean gaoXueZhi) {
        this.gaoXueZhi = gaoXueZhi;
    }

    public boolean isGaoNiaoSuan() {
        return gaoNiaoSuan;
    }

    public void setGaoNiaoSuan(boolean gaoNiaoSuan) {
        this.gaoNiaoSuan = gaoNiaoSuan;
    }

    public boolean isJinShiZhangAi() {
        return jinShiZhangAi;
    }

    public void setJinShiZhangAi(boolean jinShiZhangAi) {
        this.jinShiZhangAi = jinShiZhangAi;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public User(){

    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}
