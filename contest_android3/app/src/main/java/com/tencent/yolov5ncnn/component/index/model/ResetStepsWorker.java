package com.tencent.yolov5ncnn.component.index.model;


import android.content.Context;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
import androidx.work.WorkManager;
import androidx.work.OneTimeWorkRequest;
import androidx.work.PeriodicWorkRequest;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;

public class ResetStepsWorker extends Worker {
    public ResetStepsWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    @NonNull
    @Override
    public Result doWork() {
        // 获取应用的SharedPreferences实例
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("app", Context.MODE_PRIVATE);
        // 从SharedPreferences中获取存储的总步数，默认为0
        int totalSteps = sharedPreferences.getInt("totalSteps", 0);

        // 存储午夜时的步数
        sharedPreferences.edit().putInt("stepsAtMidnight", totalSteps).apply();

        // 重置总步数为0
        sharedPreferences.edit().putInt("totalSteps", 0).apply();

        // 计算下一次执行的延迟时间
        long delay = getTimeUntilMidnight();

        // 调度下一次的重置步数任务
        scheduleResetStepsWorker(delay);

        // 返回成功的执行结果
        return Result.success();
    }


    private long getTimeUntilMidnight() {
        // 获取当前时间的 Calendar 实例
        Calendar c = Calendar.getInstance();
        long currentTime = c.getTimeInMillis();

        // 将时间设置为今天的午夜时刻
        c.add(Calendar.DAY_OF_MONTH, 1); // 将日期加一天，表示明天的日期
        c.set(Calendar.HOUR_OF_DAY, 0); // 将小时设置为0，表示午夜
        c.set(Calendar.MINUTE, 0); // 将分钟设置为0
        c.set(Calendar.SECOND, 0); // 将秒钟设置为0
        c.set(Calendar.MILLISECOND, 0); // 将毫秒设置为0

        // 获取午夜时刻的时间戳
        long midnightTonight = c.getTimeInMillis();

        // 计算距离午夜时刻还有多长时间
        long timeUntilMidnight = midnightTonight - currentTime;

        // 返回距离午夜时刻的时间间隔
        return timeUntilMidnight;
    }

    private void scheduleResetStepsWorker(long delay) {
        // 创建一个 OneTimeWorkRequest 对象，用于执行步数重置任务
        OneTimeWorkRequest workRequest = new OneTimeWorkRequest.Builder(ResetStepsWorker.class)
                .setInitialDelay(delay, TimeUnit.MILLISECONDS) // 设置任务的延迟执行时间
                .addTag("resetSteps") // 添加标签，可用于识别和取消任务
                .build();

        // 获取 WorkManager 实例，并将任务加入任务队列中
        WorkManager.getInstance(getApplicationContext()).enqueue(workRequest);
    }
}