package com.tencent.yolov5ncnn.component.login.activity;

import android.content.SharedPreferences;
import android.os.Bundle;

import com.qmuiteam.qmui.util.QMUIStatusBarHelper;
import com.tencent.yolov5ncnn.HomeActivity;
import com.tencent.yolov5ncnn.MyApplication;
import com.tencent.yolov5ncnn.R;
import android.app.Activity;
import android.content.Intent;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.material.button.MaterialButton;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

public class LogInActivity extends Activity {

    private EditText editAccount, editPsd; // 定义用户名和密码输入框
    private MaterialButton mbtnLogin; // 定义登录按钮
    private TextView loginRegister;  // 定义跳转到注册页面的TextView
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in); // 设置布局文件，替换成您的布局文件名称
        //设置沉浸式状态栏，即设置状态栏颜色为透明
        QMUIStatusBarHelper.translucent(this);
        // 设置状态栏字体颜色为黑色
        QMUIStatusBarHelper.setStatusBarLightMode(this);
        // 初始化视图
        editAccount = (EditText) findViewById(R.id.edit_account); // 用户名输入框
        editPsd = (EditText) findViewById(R.id.edit_psd); // 密码输入框
        mbtnLogin = (MaterialButton) findViewById(R.id.mbtn_login); // 登录按钮
        loginRegister = (TextView) findViewById(R.id.login_register);  // 获取TextView
        // 从 SharedPreferences 中获取存储的用户名和密码
        SharedPreferences sharedPreferences = getSharedPreferences("user_info", MODE_PRIVATE);
        String savedUsername = sharedPreferences.getString("username", "");
        String savedPassword = sharedPreferences.getString("password", "");
        // 设置获取的用户名和密码到 EditText
        editAccount.setText(savedUsername);
        editPsd.setText(savedPassword);
        // 设置按钮点击事件
        mbtnLogin.setOnClickListener(v -> handleLoginButtonClick());
        // 设置TextView的点击事件
        loginRegister.setOnClickListener(v -> navigateToRegisterActivity());
    }

    /**
     * 执行登录操作的方法
     */
    private void handleLoginButtonClick() {
        // 获取输入的用户名
        String inputUsername = editAccount.getText().toString().trim();
        // 获取输入的密码
        String inputPassword = editPsd.getText().toString().trim();
        // 检查输入的用户名和密码是否与保存的用户名和密码匹配
        new LoginTask(inputUsername, inputPassword).execute();
    }

    /**
     * 登录类，访问云端数据库
     */
    private class LoginTask extends android.os.AsyncTask<Void, Void, Boolean> {
        private String userName;
        private String pwd;

        LoginTask(String userName, String pwd) {
            super();
            this.userName = userName;
            this.pwd = pwd;
        }

        protected Boolean doInBackground(Void... voids) {

            try {
                String encodedUsername = URLEncoder.encode(userName, "UTF-8");
                String encodedPassword = URLEncoder.encode(pwd, "UTF-8");
                URL url = new URL(MyApplication.getIP()+"AI?flag=login&username=" + encodedUsername + "&password=" + encodedPassword);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setRequestMethod("POST");

                // 获取响应状态码
                int statusCode = conn.getResponseCode();
                if (statusCode == HttpURLConnection.HTTP_OK) {
                    conn.disconnect();
                    return true;
                } else {
                    conn.disconnect();
                    return false;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return false;
        }
        protected void onPostExecute(Boolean result) {
            if (result) {
                Toast.makeText(getApplicationContext(), "登录成功", Toast.LENGTH_SHORT).show();
                // 如果用户名和密码匹配，则跳转到主页面
                navigateToHomeActivity();
            } else {
                Toast.makeText(getApplicationContext(), "用户名或密码错误", Toast.LENGTH_SHORT).show();
            }
        }
    }

    /**
     * 跳转到登录界面
     */
    private void navigateToHomeActivity() {
        // 创建跳转到NavigationActivity的意图
        Intent intent = new Intent(LogInActivity.this, HomeActivity.class);
        // 启动新的Activity
        startActivity(intent);
    }

    /**
     * 跳转到注册界面
     */
    private void navigateToRegisterActivity() {
        // 创建跳转到RegisterActivity的Intent
        Intent intent = new Intent(LogInActivity.this, RegisterActivity.class);
        // 启动新的Activity
        startActivity(intent);
    }
}
