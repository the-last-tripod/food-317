package com.tencent.yolov5ncnn.enity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;


@Entity(indices = {@Index(value = {"date", "id"}, unique = true)})
public class NutrIntake {
    @PrimaryKey(autoGenerate = true)
    int id;

    @ColumnInfo
    String date;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public double getProtein() {
        return protein;
    }

    public void setProtein(double protein) {
        this.protein = protein;
    }

    public double getCarb() {
        return carb;
    }

    public void setCarb(double carb) {
        this.carb = carb;
    }

    public NutrIntake(String date) {
        this.date = date;
    }

    public double getEnergy() {
        return energy;
    }

    public void setEnergy(double energy) {
        this.energy = energy;
    }

    public double getSalt() {
        return salt;
    }

    public void setSalt(double salt) {
        this.salt = salt;
    }

    public double getFat() {
        return fat;
    }

    public void setFat(double fat) {
        this.fat = fat;
    }

    @ColumnInfo
    public double protein;//蛋白质

    @ColumnInfo
    public double carb; //碳水化合物
    @ColumnInfo
    public double energy; //能量
    @ColumnInfo
    public double salt; //无机盐
    @ColumnInfo
    public double fat; //脂肪




}
