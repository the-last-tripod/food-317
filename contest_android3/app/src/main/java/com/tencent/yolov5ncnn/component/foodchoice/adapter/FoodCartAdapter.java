package com.tencent.yolov5ncnn.component.foodchoice.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.tencent.yolov5ncnn.R;
import com.tencent.yolov5ncnn.enity.Food;
import java.util.List;

public class FoodCartAdapter extends RecyclerView.Adapter<FoodCartAdapter.ViewHolder> {

    private Context context; // 上下文对象
    private List<Food> selectedFood; // 选中的食物列表
    private OnFoodRemovedListener onFoodRemovedListener; // 食物移除监听器

    /**
     * 作用：构造方法，初始化 FoodCartAdapter
     *
     * @param context                  上下文
     * @param selectedFood             选中的食物列表
     * @param onFoodRemovedListener    食物移除监听器
     */
    public FoodCartAdapter(Context context, List<Food> selectedFood, OnFoodRemovedListener onFoodRemovedListener) {
        this.context = context;
        this.selectedFood = selectedFood;
        this.onFoodRemovedListener = onFoodRemovedListener;
    }

    /**
     * 作用：当需要新的 ViewHolder 来表示列表项时调用
     *
     * @param parent   父视图
     * @param viewType 视图类型
     * @return ViewHolder
     */
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // 从 XML 布局文件创建一个新的视图
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cart, parent, false);
        // 返回包含视图的 ViewHolder
        return new ViewHolder(view);
    }

    /**
     * 作用：用于将数据绑定到 ViewHolder
     *
     * @param holder   ViewHolder
     * @param position 列表项位置
     */
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // 获取当前列表项的数据
        Food food = selectedFood.get(position);
        holder.foodSelectedName.setText(food.getName()); // 设置食物名称

        // 设置食物质量和卡路里
        holder.foodSelectedMass.setText(String.format("%d克", food.getMass()));
        holder.foodSelectedCalorie.setText(String.format("%.1f千卡", food.getEnergy() * ((double)food.getMass() / 100.0)));

        if (food.img_path != null && !food.img_path.isEmpty()) { // 设置食物图片
            Glide.with(context)
                    .load(food.img_path) // 加载图片的 URL
                    .into(holder.foodIcon); // 将图片设置到 ImageView 控件中显示
        }

        holder.imgDeleteCart.setOnClickListener(v -> {
            Food removedFood = selectedFood.get(position);
            selectedFood.remove(position);
            notifyItemRemoved(position);
            notifyItemRangeChanged(position, selectedFood.size());
            onFoodRemovedListener.onFoodRemoved(removedFood);
        });
    }

    /**
     * 作用：返回列表项的数量
     *
     * @return 列表项数量
     */
    @Override
    public int getItemCount() {
        return selectedFood != null ? selectedFood.size() : 0;
    }

    // 提供对列表项的视图的引用
    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView foodSelectedName; // 食物名称
        ImageView foodIcon; // 食物图片
        TextView foodSelectedMass; // 食物质量
        TextView foodSelectedCalorie; // 食物卡路里
        ImageView imgDeleteCart; // 删除按钮

        public ViewHolder(View itemView) {
            super(itemView);
            foodSelectedName = itemView.findViewById(R.id.tv_selected_food_name);
            foodIcon = itemView.findViewById(R.id.img_food_cart);
            foodSelectedMass = itemView.findViewById(R.id.tv_selected_food_mass);
            foodSelectedCalorie = itemView.findViewById(R.id.tv_selected_food_calorie);
            imgDeleteCart = itemView.findViewById(R.id.img_delete_cart);
        }
    }

    /**
     * 作用：监听食物移除事件的接口
     */
    public interface OnFoodRemovedListener {
        void onFoodRemoved(Food removedFood);
    }
}
