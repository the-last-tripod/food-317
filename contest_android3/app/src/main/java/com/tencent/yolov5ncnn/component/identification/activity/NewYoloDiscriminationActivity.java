package com.tencent.yolov5ncnn.component.identification.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.lxj.xpopup.XPopup;
import com.qmuiteam.qmui.util.QMUIStatusBarHelper;
import com.tencent.yolov5ncnn.MyApplication;
import com.tencent.yolov5ncnn.R;
import com.tencent.yolov5ncnn.component.foodchoice.model.FoodAddPopup;
import com.tencent.yolov5ncnn.component.identification.adapter.YoloAdapter;
import com.tencent.yolov5ncnn.dao.FoodDao;
import com.tencent.yolov5ncnn.dao.NutrIntakeDao;
import com.tencent.yolov5ncnn.enity.Food;
import com.tencent.yolov5ncnn.enity.NutrIntake;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;

public class NewYoloDiscriminationActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private YoloAdapter adapter;
    private Button btn_add;
    private Button btn_load;
    private FoodDao mFoodDao = MyApplication.getInstance().getMyDB().FoodDao();

    private List<Food> mFoodList;
    private ArrayList<Integer> numList;
    private ArrayList<String> mFoodName;

    //获取此次推荐的食用量以及使用以后可以获得的营养成分量 目前只是随机推荐
    private void getFoodEaten(){
        //循环检索mFoodName中的每一个名称，将匹配的食物实体添加到mFood列表中
        for (int i = 0; i < mFoodName.size(); i++) {
            Random rand = new Random();
            int num = rand.nextInt(6) + 5;
            numList.add(num);
            Food food = mFoodDao.queryByName(mFoodName.get(i));
            mFoodList.add(food);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_yolo_discrimination);
        mFoodName = getIntent().getStringArrayListExtra("myArrayList");
        mFoodList = new ArrayList<>();
        numList = new ArrayList<>();
        // 设置沉浸式状态栏，即将状态栏颜色设置为透明
        QMUIStatusBarHelper.translucent(this);
        // 设置状态栏字体颜色为黑色
        QMUIStatusBarHelper.setStatusBarLightMode(this);

        // 获取状态栏高度
        int statusBarHeight = QMUIStatusBarHelper.getStatusbarHeight(this);

        // 通过ID找到RelativeLayout
        RelativeLayout relativeLayout = findViewById(R.id.rl_top);

        // 获取RelativeLayout当前的布局参数
        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) relativeLayout.getLayoutParams();

        // 将RelativeLayout的顶部边距设置为状态栏高度
        layoutParams.setMargins(layoutParams.leftMargin, statusBarHeight, layoutParams.rightMargin, layoutParams.bottomMargin);

        // 将修改后的布局参数应用到RelativeLayout
        relativeLayout.setLayoutParams(layoutParams);

        // 初始化RecyclerView
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        btn_add = findViewById(R.id.btn_add);
        btn_load = findViewById(R.id.btn_load);

        getFoodEaten();
        // 创建并设置适配器
        adapter = new YoloAdapter(mFoodList, numList);
        recyclerView.setAdapter(adapter);


        //添加菜品
        btn_add.setOnClickListener(v -> {
            new XPopup.Builder(this)
                    .asCustom(new FoodAddPopup(this)) // 注意这里传递了FoodChoiceActivity实例
                    .show();
        });

        //录入数据
        btn_load.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //获取数据

                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                String date = dateFormat.format(new Date());
                NutrIntakeDao nutrIntakeDao = MyApplication.getInstance().getMyDB().NutrIntakeDao();
                NutrIntake nutrIntake = nutrIntakeDao.queryByDate(date);

                //判断数据库中是否有该日期,没有则插入该日期数据
                if(nutrIntake == null){
                    Log.d("tmp",date);
                    nutrIntake = new NutrIntake(date);
                    nutrIntakeDao.insert(nutrIntake);
                }

                double protein = 0;
                double fat = 0;
                double carbs = 0;
                double calorie = 0;
                double salt = 0;
                //循环检索mFoodName中的每一个名称，将匹配的食物实体添加到mFood列表中
                for (int i = 0; i < mFoodList.size(); i++) {
                    int num = numList.get(i);
                    Food food = mFoodList.get(i);
                    protein += food.protein / 100 * num * 30;
                    carbs += food.carb/ 100 * num * 30;
                    fat += food.fat/ 100 * num * 30;
                    calorie += food.energy/ 100 * num * 30;
                    salt += food.mineral_salt/ 100 * num * 30;
                }

                nutrIntakeDao.update(date, nutrIntake.protein + protein, nutrIntake.carb + carbs,
                        nutrIntake.fat + fat, nutrIntake.energy + calorie, nutrIntake.salt + salt);
                Toast.makeText(getApplicationContext(),"信息已经录入",Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }
}

