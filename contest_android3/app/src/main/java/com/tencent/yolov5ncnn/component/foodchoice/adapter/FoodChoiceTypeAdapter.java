package com.tencent.yolov5ncnn.component.foodchoice.adapter;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;
import com.tencent.yolov5ncnn.R;
import java.util.List;

public class FoodChoiceTypeAdapter extends RecyclerView.Adapter<ViewHolder> {
    private List<String> typeList; // 类型列表
    private OnTypeClickListener onTypeClickListener; // 类型点击监听器
    private int selectedPosition = 0; // 选中的位置

    /**
     * 作用：构造函数，初始化 FoodChoiceTypeAdapter
     *
     * @param typeList            类型列表
     * @param onTypeClickListener 类型点击监听器
     */
    public FoodChoiceTypeAdapter(List<String> typeList, OnTypeClickListener onTypeClickListener) {
        this.typeList = typeList;
        this.onTypeClickListener = onTypeClickListener;
    }

    /**
     * 作用：创建整个布局的持有者，指定列表项的布局文件
     *
     * @param parent   父视图
     * @param viewType 视图类型
     * @return ViewHolder
     */
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_type, parent, false);
        return new TypeViewHolder(view);
    }

    /**
     * 作用：绑定列表项的视图持有者，操纵列表项的控件
     *
     * @param vh       ViewHolder
     * @param position 列表项位置
     */
    @Override
    public void onBindViewHolder(@NonNull ViewHolder vh, int position) {
        TypeViewHolder holder = (TypeViewHolder) vh;
        holder.typeTextView.setText(typeList.get(position));

        if (position == selectedPosition) {
            holder.itemView.setBackgroundResource(R.drawable.background_item_foodtype);
            holder.typeTextView.setTextColor(Color.BLACK);
        } else {
            holder.itemView.setBackgroundColor(Color.TRANSPARENT);
            holder.typeTextView.setTextColor(Color.GRAY);
        }

        holder.itemView.setOnClickListener(v -> {
            int previousSelectedPosition = selectedPosition;
            selectedPosition = holder.getAdapterPosition();
            // 更新先前和当前选中的项目
            notifyItemChanged(previousSelectedPosition);
            notifyItemChanged(selectedPosition);
            onTypeClickListener.onTypeClick(typeList.get(position));
        });
    }

    /**
     * 作用：获取列表项的数量
     *
     * @return int
     */
    @Override
    public int getItemCount() {
        return typeList.size();
    }

    public static class TypeViewHolder extends RecyclerView.ViewHolder {
        TextView typeTextView; // 类型文本视图

        public TypeViewHolder(@NonNull View itemView) {
            super(itemView);
            typeTextView = itemView.findViewById(R.id.txt_type);
        }
    }

    /**
     * 作用：监听类型点击事件的接口
     */
    public interface OnTypeClickListener {
        void onTypeClick(String type);
    }
}