package com.tencent.yolov5ncnn.component.foodchoice.model;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.lxj.xpopup.core.BottomPopupView;
import com.tencent.yolov5ncnn.R;
import com.tencent.yolov5ncnn.component.foodchoice.adapter.FoodCartAdapter;
import com.tencent.yolov5ncnn.enity.Food;
import com.tencent.yolov5ncnn.enity.FoodChoiceInfo;

import java.util.List;

public class FoodChoicePopup extends BottomPopupView {
    private Food mFood;// 选中食物

    private Runnable onButtonCompleteClick;// 回调函数
    /**
     * 构造函数
     * @param context
     */
    public FoodChoicePopup(@NonNull Context context, Food mFood, Runnable onButtonCompleteClick) {
        super(context);
        this.mFood = mFood;
        this.onButtonCompleteClick = onButtonCompleteClick;
    }

    /**
     * 获取布局
     * @return
     */
    @Override
    protected int getImplLayoutId() {
        return R.layout.food_choice_bottom_popup;
    }
    @Override
    protected void onCreate() {
        super.onCreate();

        ImageView imgFoodChoice = findViewById(R.id.img_food_choice);
        TextView tvSelectedFoodChoice = findViewById(R.id.tv_selected_food_choice);
        TextView tvSelectedFoodCalorie = findViewById(R.id.tv_selected_food_calorie);
        TextView calorieFoodChoice = findViewById(R.id.calorie_food_choice);
        TextView proteinFoodChoice = findViewById(R.id.protein_food_choice);
        TextView fatFoodChoice = findViewById(R.id.fat_food_choice);
        TextView cabFoodChoice = findViewById(R.id.cab_food_choice);
        TextInputEditText editText = findViewById(R.id.edit_text);
        MaterialButton buttonComplete = findViewById(R.id.button_complete);


        if (mFood.img_path != null && mFood.img_path != "")// 设置食物图片
            Glide.with(getContext())
                    .load(mFood.img_path) // 加载图片的 URL
                    .into(imgFoodChoice); // 将图片设置到 ImageView 控件中显示

        // 设置食物名称
        tvSelectedFoodChoice.setText(mFood.name);

        // 设置食物热量
        tvSelectedFoodCalorie.setText(String.valueOf(mFood.energy));

        // 设置食物热量
        calorieFoodChoice.setText(String.format("%.1f千卡", mFood.energy));

        // 设置食物蛋白质
        proteinFoodChoice.setText(String.format("%.1f克", mFood.protein));

        // 设置食物脂肪
        fatFoodChoice.setText(String.format("%.1f克", mFood.fat));

        // 设置食物碳水化合物
        cabFoodChoice.setText(String.format("%.1f克", mFood.carb));

        // 设置按钮点击后获取并设置质量
        buttonComplete.setOnClickListener(v -> {
            String massStr = editText.getText().toString();
            if (!massStr.isEmpty()) {
                int mass = Integer.parseInt(massStr);
                mFood.setMass(mass);
                // 将添加操作移至 FoodChoicePopup 类中
                onButtonCompleteClick.run();
                dismiss();
            }
        });
    }
    /**
     * 设置最大高度
     */
    @Override
    protected int getMaxHeight() {
        return (int) (getWindowHeight(getContext()) * .5f);
    }
    /**
     * 弹窗的高度，用来动态设定当前弹窗的高度，受getMaxHeight()限制
     */
    protected int getPopupHeight() {
        return (int) (getWindowHeight(getContext()) * .35f);
    }
    /**
     * 获取屏幕高度的方法
     */
    protected int getWindowHeight(Context context) {
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        windowManager.getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.heightPixels;
    }
}
