package com.tencent.yolov5ncnn.component.recommend.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.tencent.yolov5ncnn.R;
import com.tencent.yolov5ncnn.enity.Food;

import java.util.ArrayList;

public class FoodRecommendAdapter extends RecyclerView.Adapter<FoodRecommendAdapter.ViewHolder> {
    private Context mContext;
    private ArrayList<Food> mFoodList;

    public FoodRecommendAdapter(Context context, ArrayList<Food> foodList) {
        this.mContext = context;
        this.mFoodList = foodList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_food_recommend, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Food food = mFoodList.get(position);
        holder.foodName.setText(food.getName());
        holder.foodIntake.setText(String.format("%d 克", food.getMass()));
        // 加载图片，这里假设你的Food类有一个获取图片资源id的方法getImgResId
        if (food.img_path != null && food.img_path != ""){
            Glide.with(holder.foodImage.getContext())
                    .load(food.getImg_path())
                    .into(holder.foodImage);
        }
//        Glide.with(mContext).load(food.getImg_path()).into(holder.foodImage);
        holder.foodEnergy.setText(String.format("%.2f 千卡", food.getEnergy()));
    }

    @Override
    public int getItemCount() {
        return mFoodList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView foodName;
        TextView foodIntake;
        TextView foodEnergy;
        ImageView foodImage; // 增加图片的声明

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            foodName = itemView.findViewById(R.id.txt_food_name_recommend);
            foodIntake = itemView.findViewById(R.id.txt_food_intake_recommend);
            foodEnergy = itemView.findViewById(R.id.txt_food_engry_recommend);
            foodImage = itemView.findViewById(R.id.img_food_recommend); // 初始化图片
        }
    }
}
