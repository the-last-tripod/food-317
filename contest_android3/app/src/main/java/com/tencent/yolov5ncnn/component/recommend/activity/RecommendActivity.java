package com.tencent.yolov5ncnn.component.recommend.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.material.button.MaterialButton;
import com.lxj.xpopup.XPopup;
import com.qmuiteam.qmui.util.QMUIStatusBarHelper;
import com.tencent.yolov5ncnn.MyApplication;
import com.tencent.yolov5ncnn.R;
import com.tencent.yolov5ncnn.component.recommend.adapter.FoodRecommendAdapter;
import com.tencent.yolov5ncnn.dao.FoodDao;
import com.tencent.yolov5ncnn.dao.UserDao;
import com.tencent.yolov5ncnn.enity.Food;
import com.tencent.yolov5ncnn.enity.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Scanner;

public class RecommendActivity extends AppCompatActivity {
    private ImageView imageBack;
    private RecyclerView foodRecommendView;
    private EditText Intensity;
    private MaterialButton recommendButton;
    private ArrayList<Food> breakfastFoodList = new ArrayList<>();
    private ArrayList<Food> lunchFoodList = new ArrayList<>();
    private ArrayList<Food> dinnerFoodList = new ArrayList<>();
    private FoodDao mFoodDao = MyApplication.getInstance().getMyDB().FoodDao();
    private RelativeLayout breakfastLayout, lunchLayout, dinnerLayout;
    private TextView breakfastText, lunchText, dinnerText;
    private RelativeLayout selectedRelativeLayout;
    private TextView selectedTextView;
    private ImageView helpImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recommend);
        initStatus();
        UserDao userDao = MyApplication.getInstance().getMyDB().UserDao();
        User user = userDao.queryAll();
        findViews();
        setViewTags();
        setOnClickListeners();
        setLayoutManagers();
        selectedRelativeLayout = breakfastLayout;
        selectedTextView = breakfastText;
    }

    private void findViews() {
        imageBack = findViewById(R.id.img_back);
        foodRecommendView = findViewById(R.id.rv_food_recommend);
        Intensity = findViewById(R.id.edit_intensity);
        recommendButton = findViewById(R.id.mbtn_recommend);
        breakfastLayout = findViewById(R.id.rl_breakfast);
        lunchLayout = findViewById(R.id.rl_lunch);
        dinnerLayout = findViewById(R.id.rl_dinner);
        breakfastText = findViewById(R.id.txt_6_1);
        lunchText = findViewById(R.id.txt_7_1);
        dinnerText = findViewById(R.id.txt_8_1);
        helpImage = findViewById(R.id.help);
    }

    private void setViewTags() {
        breakfastLayout.setTag("breakfast");
        lunchLayout.setTag("lunch");
        dinnerLayout.setTag("dinner");
    }

    private void setOnClickListeners() {
        imageBack.setOnClickListener(v -> finish());
        recommendButton.setOnClickListener(v -> updateRecommendation());
        breakfastLayout.setOnClickListener(v -> setSelected(breakfastLayout, breakfastText));
        lunchLayout.setOnClickListener(v -> setSelected(lunchLayout, lunchText));
        dinnerLayout.setOnClickListener(v -> setSelected(dinnerLayout, dinnerText));
        helpImage.setOnClickListener(v -> showHelpPopup());
    }

    private void setLayoutManagers() {
        foodRecommendView.setLayoutManager(new LinearLayoutManager(RecommendActivity.this, LinearLayoutManager.HORIZONTAL, false));
    }

    private void updateRecommendation() {
        breakfastFoodList.clear();
        lunchFoodList.clear();
        dinnerFoodList.clear();
        new ConnectTask().execute();
    }

    private void showHelpPopup() {
        new XPopup.Builder(RecommendActivity.this)
                .asConfirm("运动系数说明",
                        "不进行或很少进行运动的人群：1\n" +
                                "一周进行1-3次低强度运动的人群：2\n" +
                                "一周进行3-5次中等强度运动的人群：3\n" +
                                "一周进行6-7次强度较大运动的人群：4\n" +
                                "从事非常高强度运动或活动的人群：5",
                        null, "了解", null, null, true, 0)
                .show();
    }

    // 设置选中的视图的背景并重置之前选中的视图
    private void setSelected(RelativeLayout rl, TextView txt) {
        if (selectedRelativeLayout != null && selectedTextView != null) {
            resetSelected(selectedRelativeLayout, selectedTextView);
        }

        Drawable[] layers = new Drawable[2];
        layers[0] = ContextCompat.getDrawable(this, R.drawable.background_rounded_gradient_grey);
        layers[1] = ContextCompat.getDrawable(this, R.drawable.background_rounded_gradient_green);
        TransitionDrawable transitionDrawable = new TransitionDrawable(layers);

        rl.setBackground(transitionDrawable);
        transitionDrawable.startTransition(300);

        animateTextColor(txt, R.color.black, R.color.white);

        selectedRelativeLayout = rl;
        selectedTextView = txt;
    }

    // 重置未选中视图的背景
    private void resetSelected(RelativeLayout rl, TextView txt) {
        Drawable[] layers = new Drawable[2];
        layers[0] = ContextCompat.getDrawable(this, R.drawable.background_rounded_gradient_green);
        layers[1] = ContextCompat.getDrawable(this, R.drawable.background_rounded_gradient_grey);
        TransitionDrawable transitionDrawable = new TransitionDrawable(layers);

        // 启用交叉淡入淡出效果
        transitionDrawable.setCrossFadeEnabled(true);

        rl.setBackground(transitionDrawable);
        transitionDrawable.startTransition(300);

        animateTextColor(txt, R.color.white, R.color.black);
    }

    // 动画改变文本颜色
    private void animateTextColor(TextView textView, int colorFrom, int colorTo) {
        int textColorFrom = getResources().getColor(colorFrom);
        int textColorTo = getResources().getColor(colorTo);
        ObjectAnimator textColorAnimator = ObjectAnimator.ofInt(textView, "textColor", textColorFrom, textColorTo);
        textColorAnimator.setEvaluator(new ArgbEvaluator());
        textColorAnimator.setDuration(300);
        textColorAnimator.start();
    }
    private class ConnectTask extends AsyncTask<Void, Void, String> {
        private Exception exception;

        protected String doInBackground(Void... voids) {
            try {
                UserDao userDao = MyApplication.getInstance().getMyDB().UserDao();
                userDao.updateSport(Integer.parseInt(Intensity.getText().toString()) - 1);
                User user = userDao.queryAll();

                URL url = null;
                url = new URL(MyApplication.getIP()+"AI?flag=3");
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                try {
                    urlConnection.setRequestMethod("POST");
                    urlConnection.setRequestProperty("Content-Type", "application/json");
                    urlConnection.setRequestProperty("Accept", "application/json");
                    urlConnection.setDoOutput(true);
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("name", user.name);
                    jsonObject.put("gender", user.getGender());
                    jsonObject.put("age", user.age);
                    jsonObject.put("height", user.height);
                    jsonObject.put("weight", user.weight);
                    jsonObject.put("sport", user.sport);
                    String jsonReq = jsonObject.toString();

                    OutputStream outputStream = urlConnection.getOutputStream();
                    outputStream.write(jsonReq.getBytes());
                    outputStream.close();
                    //接收resp
                    Scanner scan = new Scanner(urlConnection.getInputStream());
                    StringBuilder sb = new StringBuilder();
                    while (scan.hasNext()){
                        String respCut = scan.next();
                        sb.append(respCut);
                    }
                    String jsonResp = sb.toString();

                    return jsonResp;
                } finally {
                    urlConnection.disconnect();
                }
            } catch (Exception e) {
                this.exception = e;
                e.printStackTrace();
                return null;
            }
        }

        protected void onPostExecute(String result) {
            if (result != null) {
                try {
                    JSONObject json = new JSONObject(result);
                    JSONObject jsonObject = new JSONObject(result);
                    // 获取 breakfast 数组
                    JSONArray breakfast = jsonObject.getJSONArray("breakfast");
                    for (int i = 0; i < breakfast.length(); i++) {
                        JSONObject foodJson = breakfast.getJSONObject(i);
                        String name = foodJson.getString("name");
                        double mass = foodJson.getDouble("mass");
                        Food food = mFoodDao.queryByName(name);
                        food.mass = (int)mass;
                        breakfastFoodList.add(food);
                    }

//                     获取 lunch 数组
                    JSONArray lunch = jsonObject.getJSONArray("lunch");
                    for (int i = 0; i < lunch.length(); i++) {
                        JSONObject foodJson = lunch.getJSONObject(i);
                        String name = foodJson.getString("name");
                        double mass = foodJson.getDouble("mass");
                        Food food = mFoodDao.queryByName(name);
                        food.mass = (int)mass;
                        lunchFoodList.add(food);
                    }

//                     获取 dinner 数组
                    JSONArray dinner = jsonObject.getJSONArray("dinner");
                    for (int i = 0; i < dinner.length(); i++) {
                        JSONObject foodJson = dinner.getJSONObject(i);
                        String name = foodJson.getString("name");
                        double mass = foodJson.getDouble("mass");
                        Food food = mFoodDao.queryByName(name);
                        food.mass = (int)mass;
                        dinnerFoodList.add(food);
                    }

                    JSONObject suggestor = jsonObject.getJSONObject("suggestor");
                    //根据个人信息推荐的营养参数
                    double fatMax = suggestor.getDouble("fat_need_max");
                    double fatMin = suggestor.getDouble("fat_need_min");
                    double energy = suggestor.getDouble("energy");
                    double carbMax = suggestor.getDouble("carb_need_max");
                    double carbMin = suggestor.getDouble("carb_need_min");
                    double proteinMax = suggestor.getDouble("protein_need_max");
                    double proteinMin = suggestor.getDouble("protein_need_min");
                    UserDao userDao = MyApplication.getInstance().getMyDB().UserDao();
                    userDao.updateNutriInfo((int)energy,(int)proteinMin,(int)proteinMax,(int)carbMax,(int)carbMin,(int)fatMax,(int)fatMin);

                    Log.d("ysl",breakfastFoodList.toString());

                    // 使用早餐、午餐或晚餐列表来创建适配器并设置给 ViewPager
                    FoodRecommendAdapter adapter;
                    if (selectedRelativeLayout != null) {
                        String tag = (String) selectedRelativeLayout.getTag();

                        switch (tag) {
                            case "lunch":
                                adapter = new FoodRecommendAdapter(RecommendActivity.this, lunchFoodList);
                                break;

                            case "dinner":
                                adapter = new FoodRecommendAdapter(RecommendActivity.this, dinnerFoodList);
                                break;

                            case "breakfast":
                            default:
                                adapter = new FoodRecommendAdapter(RecommendActivity.this, breakfastFoodList);
                                break;
                        }

                        foodRecommendView.setAdapter(adapter);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.d("jsonResp", "Error: result is null");
            }
        }
    }


    /**
     * 初始化状态栏与标题栏
     */
    public void initStatus(){
        // 设置沉浸式状态栏，即将状态栏颜色设置为透明
        QMUIStatusBarHelper.translucent(this);
        // 设置状态栏字体颜色为黑色
        QMUIStatusBarHelper.setStatusBarLightMode(this);

        // 获取状态栏高度
        int statusBarHeight = QMUIStatusBarHelper.getStatusbarHeight(this);

        // 通过ID找到RelativeLayout
        RelativeLayout relativeLayout = findViewById(R.id.rl_top);

        // 获取RelativeLayout当前的布局参数
        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) relativeLayout.getLayoutParams();

        // 将RelativeLayout的顶部边距设置为状态栏高度
        layoutParams.setMargins(layoutParams.leftMargin, statusBarHeight, layoutParams.rightMargin, layoutParams.bottomMargin);

        // 将修改后的布局参数应用到RelativeLayout
        relativeLayout.setLayoutParams(layoutParams);
    }
}