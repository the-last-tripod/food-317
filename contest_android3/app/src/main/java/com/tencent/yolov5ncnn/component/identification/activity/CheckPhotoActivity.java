package com.tencent.yolov5ncnn.component.identification.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.tencent.yolov5ncnn.R;


public class CheckPhotoActivity extends AppCompatActivity {
    private ImageView iv_photo;
    private ImageView iv_confirm;
    private ImageView iv_revoke;
    private Bitmap bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_photo);

        iv_photo = findViewById(R.id.iv_photo);
        iv_revoke = findViewById(R.id.iv_revoke);
        iv_confirm = findViewById(R.id.iv_confirm);

        Intent intent = getIntent();
        String path = intent.getStringExtra("path");

        if(path != null){
            bitmap = BitmapFactory.decodeFile(path);
            iv_photo.setImageBitmap(bitmap);
            Log.d("ysl",path);
        }

        iv_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent resultIntent = new Intent();
                setResult(RESULT_OK, resultIntent);
                finish();
            }
        });

        iv_revoke.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent resultIntent = new Intent();
                setResult(RESULT_CANCELED, resultIntent);
                finish();
            }
        });
    }
}