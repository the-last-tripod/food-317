package com.tencent.yolov5ncnn.component.identification.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import com.tencent.yolov5ncnn.MyApplication;
import com.tencent.yolov5ncnn.MyView.CameraXView;
import com.tencent.yolov5ncnn.MyView.LoadingView;
import com.tencent.yolov5ncnn.R;
import com.tencent.yolov5ncnn.utils.BitmapUtil;
import com.tencent.yolov5ncnn.utils.Utils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class ChooseWayActivity extends AppCompatActivity implements View.OnClickListener{

    //private ImageView image_view;
    private Button btn_yolo;
    private Button btn_ocr;
    private Button btn_identify;
    private final int NO_CHOSEN = 0;
    private final int YOLO_CHOSEN = 1;
    private final int OCR_CHOSEN = 2;
    private final int SELECT_IMAGE = 20;
    private final int CAMERA_REQUEST_CODE = 30;
    private int chosen = NO_CHOSEN;

    private Bitmap bitmap = null;


    private final static String TAG = "CameraxPhotoActivity";
    private CameraXView cxv_preview; // 声明一个增强相机视图对象
    private View v_black; // 声明一个视图对象
    private boolean selectPhoto = false;

    private ImageView iv_preview_image; // 声明一个图像视图对象

    private final Handler mHandler = new Handler(Looper.myLooper()); // 声明一个处理器对象

    private Uri photoUri;


    //点击拍照按钮
    private ImageView camera_button;
    private ImageView iv_album;

    //获取的图像
    private ImageView iv_image;

    //layout
    private RelativeLayout rl_camera;
    private RelativeLayout rl_choice;
    private LoadingView loadingView;

    //拍照得到的图片path
    private String photoPath;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_recognition);

        btn_yolo = findViewById(R.id.btn_yolo);
        btn_ocr = findViewById(R.id.btn_ocr);
        btn_identify = findViewById(R.id.btn_indenify);

        camera_button = findViewById(R.id.camera_button);
        iv_album = findViewById(R.id.album_button);

        iv_image = findViewById(R.id.iv_image);

        rl_camera = findViewById(R.id.rl_camera);
        rl_choice = findViewById(R.id.rl_choice);
        loadingView = findViewById(R.id.loading_view);
        btn_yolo.setOnClickListener(this);
        btn_ocr.setOnClickListener(this);
        iv_album.setOnClickListener(this);
        camera_button.setOnClickListener(this);
        btn_identify.setOnClickListener(this);

        initView(); // 初始化视图
        initCamera(); // 初始化相机
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_yolo:
                //用户选择yolo识别
                btn_yolo.setBackgroundColor(Color.parseColor("#006400"));
                // 设置按钮文字加粗
                btn_yolo.setTypeface(null, Typeface.BOLD);
                // 设置按钮文字颜色
                btn_yolo.setTextColor(Color.parseColor("#FF00BB76"));
                // 设置状态为选择yolo
                chosen = YOLO_CHOSEN;


                //将ocr恢复原样
                btn_ocr.setBackgroundColor(Color.TRANSPARENT);
                btn_ocr.setTextColor(Color.WHITE);
                btn_ocr.setTypeface(null,Typeface.NORMAL);

                break;
            case R.id.btn_ocr:
                //用户选择ocr识别
                btn_ocr.setBackgroundColor(Color.parseColor("#006400"));
                btn_ocr.setTextColor(Color.parseColor("#FF00BB76"));
                btn_ocr.setTypeface(null, Typeface.BOLD);
                // 设置状态为选择ocr
                chosen = OCR_CHOSEN;

                //yolo恢复原样
                btn_yolo.setBackgroundColor(Color.TRANSPARENT);
                btn_yolo.setTextColor(Color.WHITE);
                btn_yolo.setTypeface(null, Typeface.NORMAL);
                break;
            case R.id.album_button:
                //相册
                Intent i = new Intent(Intent.ACTION_PICK);
                i.setType("image/*");
                startActivityForResult(i, SELECT_IMAGE);
                break;
            case R.id.camera_button:
                dealPhoto();
                break;
            case R.id.btn_indenify:
                if(!selectPhoto){
                    Toast.makeText(getApplicationContext(), "请先输入图片", Toast.LENGTH_SHORT).show();
                    break;
                }
                if(chosen == YOLO_CHOSEN){
                    new YoloConnectTask().execute();
                }
                else if(chosen == OCR_CHOSEN){
                    new OcrConnectTask().execute();
                }
                else{
                    Toast.makeText(getApplicationContext(), "请选择识别功能", Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                break;
        }
    }

    // 初始化视图
    private void initView() {
        cxv_preview = findViewById(R.id.cxv_preview);
        v_black = findViewById(R.id.v_black);
        int adjustHeight = Utils.getScreenWidth(this) * 16 / 9;//相机预览窗口的宽高比
        Log.d(TAG, "onResume getScreenWidth="+Utils.getScreenWidth(this)+", adjustHeight="+adjustHeight);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) cxv_preview.getLayoutParams();
        params.height = adjustHeight; // 根据预览尺寸调整预览窗口的高度
        cxv_preview.setLayoutParams(params); // 设置预览视图的布局参数
    }


    // 初始化相机
    private void initCamera() {
        // 打开增强相机，并指定停止拍照监听器
        cxv_preview.openCamera(this, CameraXView.MODE_PHOTO, (result) -> {
            runOnUiThread(() -> {
                camera_button.setEnabled(true);
                photoPath = cxv_preview.getPhotoPath();
                // 创建一个 File 对象
                File imageFile = new File(photoPath);
                // 获取 FileProvider 的 authority，用于创建 content URI
                String authority = getApplicationContext().getPackageName() + ".fileprovider";
                //创建Uri便于Ocr识别
                photoUri = FileProvider.getUriForFile(getApplicationContext(), authority, imageFile);
                Intent intent = new Intent(this, CheckPhotoActivity.class);
                intent.putExtra("path", photoPath);
                startActivityForResult(intent, CAMERA_REQUEST_CODE);
            });
        });
    }

    // 处理拍照动作
    private void dealPhoto() {
        camera_button.setEnabled(false);
        v_black.setVisibility(View.VISIBLE);
        cxv_preview.takePicture(); // 拍摄照片

        mHandler.postDelayed(() -> v_black.setVisibility(View.GONE), 500);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        cxv_preview.closeCamera(); // 关闭相机
    }

    private Bitmap decodeUri(Uri selectedImage) throws FileNotFoundException
    {
        // Decode image size
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImage), null, o);

        // The new size we want to scale to
        final int REQUIRED_SIZE = 640;

        // Find the correct scale value. It should be the power of 2.
        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;
        while (true) {
            if (width_tmp / 2 < REQUIRED_SIZE
                    || height_tmp / 2 < REQUIRED_SIZE) {
                break;
            }
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        // Decode with inSampleSize
        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        Bitmap bitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImage), null, o2);

        // Rotate according to EXIF
        int rotate = 0;
        try
        {
            ExifInterface exif = new ExifInterface(getContentResolver().openInputStream(selectedImage));
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;
            }
        }
        catch (IOException e)
        {
            Log.e("MainActivity", "ExifInterface IOException");
        }

        Matrix matrix = new Matrix();
        matrix.postRotate(rotate);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case SELECT_IMAGE:
                if (resultCode == RESULT_OK && null != data) {
                    Uri selectedImage = data.getData();
                    try
                    {
                        bitmap = decodeUri(selectedImage);
                        Intent intent = new Intent(this, CheckPhotoActivity.class);
                        Log.d("ysl",selectedImage.toString());
                        bitmap = BitmapUtil.decodeUri(this, selectedImage);
                        cxv_preview.setVisibility(View.GONE);
                        iv_image.setVisibility(View.VISIBLE);
                        iv_image.setImageBitmap(bitmap);
                        selectPhoto = true;
                        photoUri = selectedImage;
                    }
                    catch (FileNotFoundException e)
                    {
                        Log.e("MainActivity", "FileNotFoundException");
                        return;
                    }
                }
                break;
            case CAMERA_REQUEST_CODE:
                if(resultCode == RESULT_OK){
                    cxv_preview.setVisibility(View.GONE);
                    bitmap = BitmapFactory.decodeFile(photoPath);
                    iv_image.setVisibility(View.VISIBLE);
                    iv_image.setImageBitmap(bitmap);
                    selectPhoto = true;
                }
                break;
            default:
                break;
        }
    }


    private class YoloConnectTask extends AsyncTask<Void, Void, String> {
        private Exception exception;

        protected String doInBackground(Void... voids) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    rl_camera.setVisibility(View.GONE);
                    rl_choice.setVisibility(View.GONE);
                    loadingView.setVisibility(View.VISIBLE);
                }
            });
            // 创建一个Socket对象并连接到目标主机和端口号
            Socket socket = null;
            Log.d("ysl","yolo开始尝试连接");

            try {
                socket = new Socket(MyApplication.getAIIP(), 9090);
            } catch (IOException e) {
                e.printStackTrace();
            }

            Log.d("ysl","yolo建立连接");

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
            byte[] imageData = baos.toByteArray();

            try(InputStream inputStream = socket.getInputStream();
                OutputStream outputStream = socket.getOutputStream();) {
                // 获取Socket的输出流，并将图像数据写入输出流中
                PrintWriter printWriter = new PrintWriter(outputStream);
                printWriter.println("1");
                printWriter.flush();//将缓冲区刷新
                Scanner scan = new Scanner(inputStream);
                String response = scan.next();
                if(response.compareTo("OK") == 0){
                    Log.d("tmp", "请求接收成功");
                    outputStream.write(imageData, 0, imageData.length);
                    outputStream.flush();
                }
                socket.shutdownOutput();

                ArrayList<String[]> al = new ArrayList<>();//label x1 y1 x2 y2备用
                ArrayList<String> res = new ArrayList<>();//只返回label

                int cnt = Integer.valueOf(scan.next());
                for (int i = 0; i < cnt; i++) {
                    String[] p = new String[5];
                    for (int j = 0; j < 5; j++) {
                        p[j] = scan.next();
                    }
                    Log.d("ysl", Arrays.toString(p));
                    al.add(p);
                    res.add(p[0]);
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        showObjects_online(al);
                    }
                });
                yoloProcess(res);//页面跳转
                socket.close();
            }catch (IOException e){
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(String result) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    loadingView.setVisibility(View.INVISIBLE);
                    rl_choice.setVisibility(View.VISIBLE);
                    rl_camera.setVisibility(View.VISIBLE);
                }
            });
        }
    }

    private class OcrConnectTask extends AsyncTask<Void, Void, String> {
        private Exception exception;
        private Uri uri = photoUri;
        public OcrConnectTask() {
            super();
        }

        protected String doInBackground(Void... voids) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    rl_camera.setVisibility(View.GONE);
                    rl_choice.setVisibility(View.GONE);
                    loadingView.setVisibility(View.VISIBLE);
                }
            });

            // 创建一个Socket对象并连接到目标主机和端口号
            Socket socket = null;
            try {
                socket = new Socket(MyApplication.getAIIP(), 9090);
                Log.d("ysl","ocr建立连接");


            } catch (IOException e) {
                e.printStackTrace();
            }

            try(InputStream inputStream = socket.getInputStream();
                OutputStream outputStream = socket.getOutputStream();) {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                InputStream is = getContentResolver().openInputStream(uri);
                byte[] buffer = new byte[1024];
                int bytesRead;
                while ((bytesRead = is.read(buffer)) != -1) {
                    baos.write(buffer, 0, bytesRead);
                }

                byte[] imageData = baos.toByteArray();
                // 获取Socket的输出流，并将图像数据写入输出流中
                PrintWriter printWriter = new PrintWriter(outputStream);
                printWriter.println("2");//请求OCR识别
                printWriter.flush();//将缓冲区刷新
                Scanner scan = new Scanner(inputStream);
                String response = scan.next();
                if(response.compareTo("OK") == 0){
                    Log.d("tmp", "请求接收成功");
                    outputStream.write(imageData, 0, imageData.length);
                    outputStream.flush();
                }
                socket.shutdownOutput();

                ArrayList<String> al = new ArrayList<>();
                int cnt = Integer.valueOf(scan.next());
                for (int i = 0; i < cnt; i++) {
                    String p = scan.next();
                    Log.d("tmp", p);
                    al.add(p);
                }
                ocrProcess(al);
                socket.close();
                finish();
            }catch (IOException e){
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(String result) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    loadingView.setVisibility(View.INVISIBLE);
                    rl_choice.setVisibility(View.VISIBLE);
                    rl_camera.setVisibility(View.VISIBLE);
                }
            });

        }
    }


    private void yoloProcess(ArrayList<String> res) {
        Intent intent = new Intent(this, NewYoloDiscriminationActivity.class);
        intent.putStringArrayListExtra("myArrayList", res);

        startActivity(intent);
    }


    private void ocrProcess(ArrayList<String> al) {//处理服务端传来的OCR数据,进入记录界面
        Intent intent = new Intent(this, OCRdisminateActivity.class);
        for (int i = 0; i < al.size(); i++) {
            String s = al.get(i);
            String[] strings = s.split(":");
            String nutrition_name = strings[0];
            String mass = strings[1];
            intent.putExtra(nutrition_name, mass);
        }
        startActivity(intent);
    }

    private void showObjects_online(ArrayList<String[]> al)
    {
        if (al.size() == 0)
        {
            iv_image.setImageBitmap(bitmap);
            return;
        }

        // draw objects on bitmap
        Bitmap rgba = bitmap.copy(Bitmap.Config.ARGB_8888, true);

        final int[] colors = new int[] {
                Color.rgb( 54,  67, 244),
                Color.rgb( 99,  30, 233),
                Color.rgb(176,  39, 156),
                Color.rgb(183,  58, 103),
                Color.rgb(181,  81,  63),
                Color.rgb(243, 150,  33),
                Color.rgb(244, 169,   3),
                Color.rgb(212, 188,   0),
                Color.rgb(136, 150,   0),
                Color.rgb( 80, 175,  76),
                Color.rgb( 74, 195, 139),
                Color.rgb( 57, 220, 205),
                Color.rgb( 59, 235, 255),
                Color.rgb(  7, 193, 255),
                Color.rgb(  0, 152, 255),
                Color.rgb( 34,  87, 255),
                Color.rgb( 72,  85, 121),
                Color.rgb(158, 158, 158),
                Color.rgb(139, 125,  96)
        };

        Canvas canvas = new Canvas(rgba);

        Paint paint = new Paint();
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(4);

        Paint textbgpaint = new Paint();
        textbgpaint.setColor(Color.WHITE);
        textbgpaint.setStyle(Paint.Style.FILL);

        Paint textpaint = new Paint();
        textpaint.setColor(Color.BLACK);
        textpaint.setTextSize(26);
        textpaint.setTextAlign(Paint.Align.LEFT);

        for (int i = 0; i < al.size(); i++)
        {
            int a = Integer.valueOf(al.get(i)[1]);
            int b = Integer.valueOf(al.get(i)[2]);
            int a2 = Integer.valueOf(al.get(i)[3]);
            int b2 = Integer.valueOf(al.get(i)[4]);
            String label = al.get(i)[0];

            paint.setColor(colors[i % 19]);

            canvas.drawRect(a, b, a2, b2, paint);

            // draw filled text inside image
            {
//                String text = label + " = " + String.format("%.1f", objects[i].prob * 100) + "%";
                String text = label;


                float text_width = textpaint.measureText(text);
                float text_height = - textpaint.ascent() + textpaint.descent();

                float x = a;
                float y = b - text_height;
                if (y < 0)
                    y = 0;
                if (x + text_width > rgba.getWidth())
                    x = rgba.getWidth() - text_width;

                canvas.drawRect(x, y, x + text_width, y + text_height, textbgpaint);

                canvas.drawText(text, x, y - textpaint.ascent(), textpaint);
            }
        }

        iv_image.setImageBitmap(rgba);
    }
}


/*
    HTTP异步传输图片,因为不知道的原因图片无法被HTTP服务器接收,由于时间紧迫,保留Socket传输,但改为异步传输，后期统一HTTP传输
 */
//    private class ConnectTask extends AsyncTask<Void, Void, String> {
//        private Exception exception;
//        private Uri uri = null;
//        public ConnectTask(Uri uri) {
//            super();
//            this.uri = uri;
//        }
//
//        protected String doInBackground(Void... voids) {
//        HttpURLConnection conn = null;
//
//        // boundary就是request头和上传文件内容的分隔符(可自定义任意一组字符串)
//        String BOUNDARY = "----------------------------909970152053021118417998";
//        // 用来标识payLoad+文件流的起始位置和终止位置(相当于一个协议,告诉你从哪开始,从哪结束)
//        String  preFix = ("\r\n" + BOUNDARY + "\r\n");
//            try {
//                URL url = new URL("http://101.42.248.111:8080/food_servlet/AI?flag=2");
//                conn = (HttpURLConnection) url.openConnection();
//                conn.setConnectTimeout(5000);
//                conn.setReadTimeout(30000);
//                conn.setDoOutput(true);
//                conn.setDoInput(true);
//                conn.setUseCaches(false);
//                // 设置请求方法
//                conn.setRequestMethod("POST");
//                // 设置header
//                conn.setRequestProperty("Accept","*/*");
//                conn.setRequestProperty("Connection", "keep-alive");
//                conn.setRequestProperty("Content-Type",
//                    "multipart/form-data; boundary=" + BOUNDARY);
//                conn.setRequestProperty("Accept-Encoding", "gzip, deflate, br");
//
//                OutputStream out = new DataOutputStream(conn.getOutputStream());
//
//                // 要上传的数据
//                StringBuffer strBuf = new StringBuffer();
//                // 标识payLoad + 文件流的起始位置
//                strBuf.append(preFix);
//                strBuf.append("Content-Disposition: form-data; name=\"OCRImage\"; filename=\"" + "IMG_20230504_084517"+".jpg" + "\"\r\n");
//                strBuf.append("Content-Type: image/jpeg"  + "\r\n\r\n");
//
//                out.write(strBuf.toString().getBytes());
//                // 获取文件流
//                ByteArrayOutputStream baos = new ByteArrayOutputStream();
//                InputStream is = getContentResolver().openInputStream(uri);
//                byte[] buffer = new byte[1024];
//                int bytesRead;
//                while ((bytesRead = is.read(buffer)) != -1) {
//                    baos.write(buffer, 0, bytesRead);
//                }
//                byte[] imageData = baos.toByteArray();
//                // 关闭文件流
//                is.close();
//                out.write(imageData, 0, imageData.length);
//
//                // 标识payLoad + 文件流的结尾位置
//                out.write(preFix.getBytes());
//                // 输出所有数据到服务器
//                out.flush();
//                // 关闭网络输出流
//                out.close();
//
//                //接收resp
//                Scanner scan = new Scanner(conn.getInputStream());
//                StringBuilder sb = new StringBuilder();
//                while (scan.hasNext()){
//                    String respCut = scan.next();
//                    sb.append(respCut);
//                }
//                String jsonResp = sb.toString();
//
//                return jsonResp;
//            } catch (IOException e) {
//                e.printStackTrace();
//            } finally {
//                if(conn != null){
//                    conn.disconnect();
//                }
//            }
//            return null;
//        }
//
//        protected void onPostExecute(String result) {
//            if (result != null) {
//                Log.d("ysl", result);
//                try {
//                    JSONObject json = new JSONObject(result);
//                    JSONObject jsonObject = new JSONObject(result);
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            } else {
//                Log.d("jsonResp", "Error: result is null");
//            }
//        }
//    }