package com.tencent.yolov5ncnn.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.tencent.yolov5ncnn.enity.Food;

import java.util.List;

@Dao
public interface FoodDao {
    @Insert
    void insert(Food... foods);

    @Query("DELETE FROM Food where name = :name")
    void deleteByName(String name);

    // 删除所有食物信息
    @Query("DELETE FROM Food")
    void deleteAll();

    @Update
    int update(Food ... foods);

    // 加载所有食物信息
    @Query("SELECT * FROM Food")
    List<Food> queryAll();

    // 根据名字加载食物
    @Query("SELECT * FROM Food WHERE name = :name ORDER BY id DESC limit 1")
    Food queryByName(String name);

    // 根据ID加载食物
    @Query("SELECT * FROM Food WHERE id = :id limit 1")
    Food queryByID(int id);

    // 根据关键词查询食物,返回包含改关键词的食物列表
    @Query("SELECT * from Food where name like '%' || :key || '%';")
    List<Food> queryByKey(String key);
}
