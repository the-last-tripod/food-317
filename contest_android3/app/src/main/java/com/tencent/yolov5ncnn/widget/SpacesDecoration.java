package com.tencent.yolov5ncnn.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.view.View;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

public class SpacesDecoration extends RecyclerView.ItemDecoration {
    private int space; // 空白间隔
    private Drawable divider;

    public SpacesDecoration(Context context, int resId,int space) {
        // 从资源文件中获取分割线的Drawable
        divider = ContextCompat.getDrawable(context, resId);
        this.space = space;
    }

    @Override
    public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
        int left = parent.getPaddingLeft();
        int right = parent.getWidth() - parent.getPaddingRight();

        int childCount = parent.getChildCount();
        for (int i = 0; i < childCount; i++) {
            if (i < childCount - 1) { // 不为最后一行
                // 计算每个item的底部坐标
                int top = parent.getChildAt(i).getBottom();
                int bottom = top + divider.getIntrinsicHeight();
                // 绘制分割线
                divider.setBounds(left, top, right, bottom);
                divider.draw(c);
            }
        }
    }
    @Override
    public void getItemOffsets(Rect outRect, View v, RecyclerView parent, RecyclerView.State state) {
        outRect.left = space; // 左边空白间隔
        outRect.right = space; // 右边空白间隔
        outRect.bottom = space; // 上方空白间隔
        outRect.top = space; // 下方空白间隔
    }
}
