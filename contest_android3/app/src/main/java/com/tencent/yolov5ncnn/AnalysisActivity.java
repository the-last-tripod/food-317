package com.tencent.yolov5ncnn;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.graphics.Color;
import android.os.Bundle;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.qmuiteam.qmui.util.QMUIStatusBarHelper;
import com.tencent.yolov5ncnn.dao.FoodDao;
import com.tencent.yolov5ncnn.dao.NutrIntakeDao;
import com.tencent.yolov5ncnn.enity.NutrIntake;
import com.tencent.yolov5ncnn.enity.User;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class AnalysisActivity extends AppCompatActivity {

    //设置近五天摄入的蛋白质
    private float[] danbaizhi=new float[5];
    //设置近五天摄入的碳水化合物
    private float[] tanshui=new float[5];
    //设置近五天摄入的脂肪
    private float[] zhifang=new float[5];
    //设置近五天摄入的热量
    private float[] energy5day=new float[5];
    //设置近五天的日期
    private String[] days =new String[5];
    //设置今日已摄入的能量
    private float energy_today;

    private PieChart nutriStandardPieChart;
    private User user;
    private NutrIntake nutrIntake;
    private TableLayout tableLayout;
    private BarChart barChartEnergy;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_analysis);
        //设置沉浸式状态栏，即设置状态栏颜色为透明
        QMUIStatusBarHelper.translucent(this);
        // 设置状态栏字体颜色为黑色
        QMUIStatusBarHelper.setStatusBarLightMode(this);
        get_info();//从数据库中读取信息
        Button button_dan = findViewById(R.id.button_dan);
        Button button_tan = findViewById(R.id.button_tan);
        Button button_zhi = findViewById(R.id.button_zhi);
        LineChart nutriStandardLineChart = findViewById(R.id.line_chart);
        user = MyApplication.getInstance().getMyDB().UserDao().queryAll();

        TextView tvScore = findViewById(R.id.score);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        Calendar calendar = Calendar.getInstance();
        String date = dateFormat.format(calendar.getTime());
        nutrIntake = MyApplication.getInstance().getMyDB().NutrIntakeDao().queryByDate(date);

        //计算用户的饮食得分
        //脂肪占比20% 蛋白质占比20% 碳水占比20% 热量占比40%
        //60分以下红色 60~80黄色 80以上绿色
        int score = 0;

        if(tanshui[4]  < user.needCarbMax && tanshui[4]  > user.needCarbMin)score += 20;
        else score += (int)(20 / (Math.abs(tanshui[4] - user.needCarbMax)));

        if(zhifang[4]  < user.needFatMax && zhifang[4]  > user.needFatMin)score += 20;
        else score += (int)(20 / (Math.abs(zhifang[4] - user.needFatMax)));

        if(danbaizhi[4]  < user.needProteinMax && danbaizhi[4]  > user.needProteinMin)score += 20;
        else score += (int)(20 / (Math.abs(danbaizhi[4] - user.needProteinMax)));

        if(Math.abs(energy_today - user.needEnergy) < 1000)score += 40;
        else score += (int)(40 / (Math.abs(energy_today - user.needEnergy)));

        tvScore.setText(String.valueOf(score));
        //大于60设置为黄色 大于80设置为绿色
        if(score>60)tvScore.setTextColor(Color.rgb(254,225,111));
        if(score>80)tvScore.setTextColor(Color.rgb(127,231,194));

        //处理表格
        //第一列数据 碳水 蛋白质 脂肪的当天摄入量
        tableLayout = findViewById(R.id.nuti_table);
        if(nutrIntake != null){
            TableRow tableRow = (TableRow) tableLayout.getChildAt(1);
            TextView textView = (TextView)tableRow.getChildAt(1);
            textView.setText(String.valueOf((int)nutrIntake.carb));

            tableRow = (TableRow) tableLayout.getChildAt(2);
            textView = (TextView)tableRow.getChildAt(1);
            textView.setText(String.valueOf((int)nutrIntake.protein));

            tableRow = (TableRow) tableLayout.getChildAt(3);
            textView = (TextView)tableRow.getChildAt(1);
            textView.setText(String.valueOf((int)nutrIntake.fat));
        }


        //第二列数据 碳水 蛋白质 脂肪的推荐范围 min~max
        TableRow tableRow = (TableRow) tableLayout.getChildAt(1);
        TextView textView = (TextView)tableRow.getChildAt(2);
        textView.setText( String.valueOf(user.needCarbMin) + "~"+ String.valueOf(user.needCarbMax));

        tableRow = (TableRow) tableLayout.getChildAt(2);
        textView = (TextView)tableRow.getChildAt(2);
        textView.setText( String.valueOf(user.needProteinMin) + "~"+ String.valueOf(user.needProteinMax));


        tableRow = (TableRow) tableLayout.getChildAt(3);
        textView = (TextView)tableRow.getChildAt(2);
        textView.setText( String.valueOf(user.needFatMin) + "~"+ String.valueOf(user.needFatMax));


        button_dan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // 创建一个数据集并添加数据
                List<Entry> entries = new ArrayList<>();
                entries.add(new Entry(1, danbaizhi[0]));
                entries.add(new Entry(2, danbaizhi[1]));
                entries.add(new Entry(3, danbaizhi[2]));
                entries.add(new Entry(4, danbaizhi[3]));
                entries.add(new Entry(5, danbaizhi[4]));
                LineDataSet dataSet = new LineDataSet(entries, "蛋白质");
                dataSet.setColor(Color.YELLOW);
                dataSet.setValueTextColor(Color.BLACK);

                // 创建第二个数据集并添加数据
                List<Entry> entries2 = new ArrayList<>();
                entries2.add(new Entry(1, user.needProteinMax));
                entries2.add(new Entry(2, user.needProteinMax));
                entries2.add(new Entry(3, user.needProteinMax));
                entries2.add(new Entry(4, user.needProteinMax));
                entries2.add(new Entry(5, user.needProteinMax));

                LineDataSet dataSet2 = new LineDataSet(entries2, "标准摄入");
                dataSet2.setColor(Color.RED);
                dataSet2.setValueTextColor(Color.BLACK);

                // 创建一个数据集合并添加数据集
                LineData lineData = new LineData(dataSet,dataSet2);
                if (nutriStandardLineChart == null){
                    Log.d("ysl","空指针???");
                }
                nutriStandardLineChart.setData(lineData);

                // 设置折线图的属性
                nutriStandardLineChart.getDescription().setEnabled(false);
                nutriStandardLineChart.setTouchEnabled(true);
                nutriStandardLineChart.setDragEnabled(true);
                nutriStandardLineChart.setScaleEnabled(true);
                nutriStandardLineChart.setPinchZoom(true);

                // 更新折线图
                nutriStandardLineChart.invalidate();
            }
        });

        button_tan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // 创建一个数据集并添加数据
                List<Entry> entries = new ArrayList<>();
                entries.add(new Entry(1, tanshui[0]));
                entries.add(new Entry(2, tanshui[1]));
                entries.add(new Entry(3, tanshui[2]));
                entries.add(new Entry(4, tanshui[3]));
                entries.add(new Entry(5, tanshui[4]));
                LineDataSet dataSet = new LineDataSet(entries, "碳水化合物");
                dataSet.setColor(Color.GRAY);
                dataSet.setValueTextColor(Color.BLACK);

                List<Entry> entries2 = new ArrayList<>();
                entries2.add(new Entry(1, user.needCarbMax));
                entries2.add(new Entry(2, user.needCarbMax));
                entries2.add(new Entry(3, user.needCarbMax));
                entries2.add(new Entry(4, user.needCarbMax));
                entries2.add(new Entry(5, user.needCarbMax));
                LineDataSet dataSet2 = new LineDataSet(entries2, "标准摄入");
                dataSet2.setColor(Color.RED);
                dataSet2.setValueTextColor(Color.BLACK);

                // 创建一个数据集合并添加数据集
                LineData lineData = new LineData(dataSet,dataSet2);

                nutriStandardLineChart.setData(lineData);

                // 设置折线图的属性
                nutriStandardLineChart.getDescription().setEnabled(false);
                nutriStandardLineChart.setTouchEnabled(true);
                nutriStandardLineChart.setDragEnabled(true);
                nutriStandardLineChart.setScaleEnabled(true);
                nutriStandardLineChart.setPinchZoom(true);



                // 更新折线图
                nutriStandardLineChart.invalidate();
            }
        });

        button_zhi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // 创建一个数据集并添加数据
                List<Entry> entries = new ArrayList<>();
                entries.add(new Entry(1, zhifang[0]));
                entries.add(new Entry(2, zhifang[1]));
                entries.add(new Entry(3, zhifang[2]));
                entries.add(new Entry(4, zhifang[3]));
                entries.add(new Entry(5, zhifang[4]));
                LineDataSet dataSet = new LineDataSet(entries, "脂肪");
                dataSet.setColor(Color.MAGENTA);
                dataSet.setValueTextColor(Color.BLACK);

                // 创建第二个数据集并添加数据
                List<Entry> entries2 = new ArrayList<>();
                entries2.add(new Entry(1, user.needFatMax));
                entries2.add(new Entry(2, user.needFatMax));
                entries2.add(new Entry(3, user.needFatMax));
                entries2.add(new Entry(4, user.needFatMax));
                entries2.add(new Entry(5, user.needFatMax));

                LineDataSet dataSet2 = new LineDataSet(entries2, "标准摄入");
                dataSet2.setColor(Color.RED);
                dataSet2.setValueTextColor(Color.BLACK);

                // 创建一个数据集合并添加数据集
                LineData lineData = new LineData(dataSet,dataSet2);
                nutriStandardLineChart.setData(lineData);

                // 设置折线图的属性
                nutriStandardLineChart.getDescription().setEnabled(false);
                nutriStandardLineChart.setTouchEnabled(true);
                nutriStandardLineChart.setDragEnabled(true);
                nutriStandardLineChart.setScaleEnabled(true);
                nutriStandardLineChart.setPinchZoom(true);

                // 更新折线图
                nutriStandardLineChart.invalidate();
            }
        });

        nutriStandardPieChart = findViewById(R.id.pie_chart);

        nutriStandardPieChart.setUsePercentValues(true);
        nutriStandardPieChart.getDescription().setEnabled(false);
        nutriStandardPieChart.setExtraOffsets(5, 10, 5, 5);

        nutriStandardPieChart.setDragDecelerationFrictionCoef(0.95f);


        nutriStandardPieChart.setCenterText("推荐摄入占比");

        nutriStandardPieChart.setExtraOffsets(20.f, 0.f, 20.f, 0.f);

        nutriStandardPieChart.setDrawHoleEnabled(true);
        nutriStandardPieChart.setHoleColor(Color.WHITE);

        nutriStandardPieChart.setTransparentCircleColor(Color.WHITE);
        nutriStandardPieChart.setTransparentCircleAlpha(110);

        nutriStandardPieChart.setHoleRadius(58f);
        nutriStandardPieChart.setTransparentCircleRadius(61f);

        nutriStandardPieChart.setDrawCenterText(true);

        nutriStandardPieChart.setRotationAngle(0);
        // enable rotation of the chart by touch
        nutriStandardPieChart.setRotationEnabled(true);
        nutriStandardPieChart.setHighlightPerTapEnabled(true);

        // chart.spin(2000, 0, 360);

        Legend l = nutriStandardPieChart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
        l.setOrientation(Legend.LegendOrientation.VERTICAL);
        l.setDrawInside(false);
        l.setEnabled(false);

        // entry label styling
        nutriStandardPieChart.setEntryLabelColor(Color.BLACK);
        nutriStandardPieChart.setEntryLabelTextSize(12f);
        setData(3, 10);



        /////////////////////////设置一周热量摄入柱状图/////////////////////////
        barChartEnergy = findViewById(R.id.bar_chart);

        barChartEnergy.getDescription().setEnabled(false);

        // if more than 60 entries are displayed in the chart, no values will be
        // drawn
        barChartEnergy.setMaxVisibleValueCount(60);

        // scaling can now only be done on x- and y-axis separately
        barChartEnergy.setPinchZoom(false);

        barChartEnergy.setDrawBarShadow(false);
        barChartEnergy.setDrawGridBackground(false);

        XAxis xAxis = barChartEnergy.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);


        barChartEnergy.getAxisLeft().setDrawGridLines(false);
        barChartEnergy.animateY(1500);

        barChartEnergy.getLegend().setEnabled(false);
        ArrayList<BarEntry> values = new ArrayList<>();

        for (int i = 0; i < 5; i++) {
            values.add(new BarEntry(i, energy5day[i]));
        }

        xAxis.setValueFormatter(new IndexAxisValueFormatter(days));
        BarDataSet set1;
        if (barChartEnergy.getData() != null &&
                barChartEnergy.getData().getDataSetCount() > 0) {
            set1 = (BarDataSet) barChartEnergy.getData().getDataSetByIndex(0);
            set1.setValues(values);
            barChartEnergy.getData().notifyDataChanged();
            barChartEnergy.notifyDataSetChanged();
        } else {
            set1 = new BarDataSet(values, "Data Set");
            set1.setColors(ColorTemplate.VORDIPLOM_COLORS);
            set1.setDrawValues(false);

            ArrayList<IBarDataSet> dataSets = new ArrayList<>();
            dataSets.add(set1);

            BarData data = new BarData(dataSets);
            barChartEnergy.setData(data);
            barChartEnergy.setFitBars(true);
        }


        barChartEnergy.invalidate();
    }

    private void setData(int count, float range) {

        List<PieEntry> entries = new ArrayList<>();
        entries.add(new PieEntry(user.needProteinMax, "蛋白质"));
        entries.add(new PieEntry(user.needCarbMax, "碳水化合物"));
        entries.add(new PieEntry(user.needFatMax, "脂肪"));

        PieDataSet dataSet = new PieDataSet(entries, "");
        dataSet.setSliceSpace(3f);
        dataSet.setSelectionShift(5f);

        // add a lot of colors

        ArrayList<Integer> colors = new ArrayList<>();

        for (int c : ColorTemplate.VORDIPLOM_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.JOYFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.COLORFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.LIBERTY_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.PASTEL_COLORS)
            colors.add(c);

        colors.add(ColorTemplate.getHoloBlue());

        dataSet.setColors(colors);
        //dataSet.setSelectionShift(0f);


        dataSet.setValueLinePart1OffsetPercentage(80.f);
        dataSet.setValueLinePart1Length(0.2f);
        dataSet.setValueLinePart2Length(0.4f);

        //dataSet.setXValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);
        dataSet.setYValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);

        PieData data = new PieData(dataSet);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(11f);
        data.setValueTextColor(Color.BLACK);
        nutriStandardPieChart.setData(data);

        // undo all highlights
        nutriStandardPieChart.highlightValues(null);

        nutriStandardPieChart.invalidate();
    }


    private void get_info() {
        User user = MyApplication.getInstance().getMyDB().UserDao().queryAll();
        FoodDao foodDao = MyApplication.getInstance().getMyDB().FoodDao();
        NutrIntakeDao nutrIntakeDao = MyApplication.getInstance().getMyDB().NutrIntakeDao();

        //设置近五天摄入的营养元素
        for (int i = 4; i >= 0 ; i--) {
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DATE, -i);
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
            String date = dateFormat.format(calendar.getTime());
            NutrIntake nutrIntake = nutrIntakeDao.queryByDate(date);
            if (nutrIntake == null){
                nutrIntake = new NutrIntake("0000-00-00");
            }
            danbaizhi[4-i] = (float)(nutrIntake.protein);
            tanshui[4-i] = (float)(nutrIntake.carb);
            zhifang[4-i] = (float)(nutrIntake.fat);
            energy5day[4-i] = (float)(nutrIntake.energy) * 0.2389f;
            days[4-i] = date.substring(5);
            //设置今日已摄入的能量
            if(i == 0){
                energy_today = (float)(nutrIntake.energy);
            }
        }
    }
}