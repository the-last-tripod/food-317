package com.tencent.yolov5ncnn.component.identification.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.tencent.yolov5ncnn.R;
import com.tencent.yolov5ncnn.enity.Food;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class YoloAdapter extends RecyclerView.Adapter<YoloAdapter.ViewHolder> {
    private List<Food> itemList;
    private List<Integer> numList;

    public YoloAdapter(List<Food> itemList, List<Integer> numList) {
        this.itemList = itemList;
        this.numList = numList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.yolo_item, parent, false);
        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Food item = itemList.get(position);

        if (item.img_path != null && item.img_path != ""){
            Glide.with(holder.iv_food.getContext())
                    .load(item.getImg_path())
                    .into(holder.iv_food);
            Log.d("ysl",item.getImg_path());

        }
        else {
            holder.iv_food.setImageResource(R.drawable.food);
        }
        holder.titleTextView.setText(item.getName());
        holder.descTextView.setText(String.valueOf((int)(item.getEnergy()/4.186)));

        holder.amountTextView.setText(String.valueOf(numList.get(position)));
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView iv_food;
        TextView titleTextView;
        TextView descTextView;
        TextView amountTextView;
        ImageView iv_del;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            iv_food = itemView.findViewById(R.id.iv_food);
            titleTextView = itemView.findViewById(R.id.titleTextView);
            descTextView = itemView.findViewById(R.id.descTextView);
            amountTextView = itemView.findViewById(R.id.tv_amount);
            iv_del = itemView.findViewById(R.id.iv_del);

            iv_del.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION) {
                        itemList.remove(position);
                        numList.remove(position);
                        notifyItemRemoved(position);
                    }
                }
            });
        }
    }
}
