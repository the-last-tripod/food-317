package com.tencent.yolov5ncnn.component.foodchoice.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;

import com.lxj.xpopup.XPopup;
import com.tencent.yolov5ncnn.MyApplication;
import com.tencent.yolov5ncnn.component.foodchoice.model.FoodCartPopup;
import com.tencent.yolov5ncnn.component.foodchoice.model.FoodChoicePopup;
import com.tencent.yolov5ncnn.dao.FoodDao;
import com.tencent.yolov5ncnn.enity.Food;
import com.tencent.yolov5ncnn.widget.RecyclerExtras;
import com.bumptech.glide.Glide;
import com.tencent.yolov5ncnn.R;
import com.tencent.yolov5ncnn.enity.FoodChoiceInfo;


import java.util.ArrayList;
import java.util.List;

public class FoodChoiceAdapter extends RecyclerView.Adapter<ViewHolder> implements RecyclerExtras.OnItemClickListener {
    private Context mContext; // 上下文对象
    private List<FoodChoiceInfo> mFoodChoiceList; // 食物选择列表
    private List<Food> mSelectedFoods; // 已选食物列表
    private RecyclerExtras.OnItemClickListener mOnItemClickListener; // 点击监听器
    private RecyclerExtras.OnItemLongClickListener mOnItemLongClickListener; // 长按监听器
    private OnFoodSelectedListener onFoodSelectedListener; // 食物选择监听器
    FoodDao foodDao = MyApplication.getInstance().getMyDB().FoodDao(); // 数据访问对象

    /**
     * 构造方法，初始化适配器
     *
     * @param context 上下文对象
     * @param foodChoiceList 食物选择列表
     * @param onFoodSelectedListener 食物选择监听器
     */
    public FoodChoiceAdapter(Context context, List<FoodChoiceInfo> foodChoiceList, OnFoodSelectedListener onFoodSelectedListener) {
        mContext = context;
        mFoodChoiceList = foodChoiceList;
        mSelectedFoods = new ArrayList<>();
        this.onFoodSelectedListener = onFoodSelectedListener;
    }

    /**
     * 更新食物列表
     *
     * @param foodList 食物列表
     */
    public void updateFoodList(List<FoodChoiceInfo> foodList) {
        mFoodChoiceList = foodList;
        notifyDataSetChanged();
    }

    /**
     * 获取列表项的个数
     *
     * @return 列表项个数
     */
    public int getItemCount() {
        return mFoodChoiceList.size();
    }

    /**
     * 创建列表项的视图持有者
     *
     * @param parent   父视图组
     * @param viewType 列表项视图类型
     * @return 视图持有者
     */
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // 根据布局文件item_food.xml生成视图对象
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_food, parent, false);
        return new ItemHolder(view);
    }

    /**
     * 绑定列表项的视图持有者
     *
     * @param vh       视图持有者
     * @param position 列表项位置
     */
    public void onBindViewHolder(ViewHolder vh, final int position) {
        ItemHolder holder = (ItemHolder) vh;
        FoodChoiceInfo foodChoice = mFoodChoiceList.get(position);
        if (foodChoice.img_path != null && !foodChoice.img_path.equals("")) {
            Glide.with(mContext)
                    .load(foodChoice.img_path) // 加载图片的 URL
                    .into(holder.iv_pic); // 将图片设置到 ImageView 控件中显示
        }
        holder.tv_name.setText(foodChoice.name);
        holder.tv_calorie.setText(String.format("%.1f 千焦/100克", foodChoice.calorie));
        if (foodChoice.calorie > 500) {
            holder.txtEnergy.setBackgroundResource(R.drawable.background_item_textview_red);
        } else {
            holder.txtEnergy.setBackgroundResource(R.drawable.background_item_textview_green);
        }
        // 列表项的点击事件
        holder.rl_item.setOnClickListener(v -> {
            if (mOnItemClickListener != null) {
                int pos = holder.getAdapterPosition();
                mOnItemClickListener.onItemClick(v, pos);
            }
        });
    }

    /**
     * 获取列表项的类型
     *
     * @param position 列表项位置
     * @return 类型
     */
    public int getItemViewType(int position) {
        return 0;
    }

    /**
     * 获取列表项的编号
     *
     * @param position 列表项位置
     * @return 编号
     */
    public long getItemId(int position) {
        return position;
    }

    /**
     * 列表项视图持有者
     */
    public static class ItemHolder extends RecyclerView.ViewHolder {
        public RelativeLayout rl_item; // 列表项布局
        public ImageView iv_pic; // 图片视图
        public TextView tv_name; // 名称文本视图
        public TextView tv_calorie; // 卡路里文本视图
        private TextView txtEnergy; // 能量视图
        public ItemHolder(View v) {
            super(v);
            rl_item = v.findViewById(R.id.rl_item);
            iv_pic = v.findViewById(R.id.item_food_image);
            tv_name = v.findViewById(R.id.item_food_name);
            tv_calorie = v.findViewById(R.id.item_food_calorie);
            txtEnergy = v.findViewById(R.id.txt_energy);
        }
    }

    /**
     * 设置点击监听器
     *
     * @param listener 点击监听器
     */
    public void setOnItemClickListener(RecyclerExtras.OnItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    /**
     * 处理列表项的点击事件
     *
     * @param view     被点击的视图
     * @param position 列表项位置
     */
    @Override
    public void onItemClick(View view, int position) {
        String foodName = mFoodChoiceList.get(position).getName(); // 获取食物名称
        Food selectedFood = foodDao.queryByName(foodName); // 获取食物实例
        new XPopup.Builder(view.getContext())
                .asCustom(new FoodChoicePopup(view.getContext(), selectedFood, () -> {
                    mSelectedFoods.add(selectedFood); // 添加食物到购物车
                    String desc = String.format("您已成功添加了第%d项，食物名称是%s", position + 1, mFoodChoiceList.get(position).getName()); // 提示文字
                    Toast.makeText(mContext, desc, Toast.LENGTH_SHORT).show(); // 显示提示信息
                    notifyItemChanged(position);
                    int selectedCount = mSelectedFoods.size();
                    onFoodSelectedListener.onFoodSelected(selectedCount);

                }))
                .show();
    }

    /**
     * 获取已选食物列表
     *
     * @return 已选食物列表
     */
    public List<Food> getSelectedFoods() {
        return mSelectedFoods;
    }

    /**
     * 接口，方便在选择后给出当前选择了多少食物
     */
    public interface OnFoodSelectedListener {
        /**
         * 食物选择回调函数
         *
         * @param selectedCount 已选食物数量
         */
        void onFoodSelected(int selectedCount);
    }

}